#!/usr/bin/env bash

docker-compose -p wyre -f /vagrant/deploy/compose/development.yml kill
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml rm -f
docker volume rm -f wyre_geth_data
docker system prune -f
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run geth init /genesis.json
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run web ./manage.py create_ethereum_account -k lAZQgo/MThOx6uCMyPwtv5Ddz7v2i3OVkOB+Dsq+9Zo= -p b0215bfa2328416f8aadaffc16374b80
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run web ./manage.py create_ethereum_account -k Kf/Xr7x1TsFZTxodSvsO8dTPaKHUroVwSF1x71GtnoA= -p bdc4cd51521145be8dacf93179dc61b0
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run web ./manage.py deploy_erc20_token -o 0x0Ed59D6E42A8DA7441ce8119eDbE5e8272aD7a4f -p b0215bfa2328416f8aadaffc16374b80 -f /usr/src/app/wyre/ethereum/smart_contracts/dist/fake_token.json -t FAKEToken -s FAKE
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run web ./manage.py approve_spender -o 0x0Ed59D6E42A8DA7441ce8119eDbE5e8272aD7a4f -p b0215bfa2328416f8aadaffc16374b80 -s 0x496203755fe97fe0198ee803bcf4d10be3ac8208 -n 9000000000
docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run web ./manage.py transfer_tokens -o 0x0Ed59D6E42A8DA7441ce8119eDbE5e8272aD7a4f -p b0215bfa2328416f8aadaffc16374b80 -r 0x496203755fe97fe0198ee803bcf4d10be3ac8208 -n 1000000000
