import argparse

import base64
import jinja2
import os


def b64encode(s):
    return base64.b64encode(s.encode()).decode('utf-8')


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    environment = jinja2.Environment(loader=jinja2.FileSystemLoader(path or './'))
    environment.filters['b64encode'] = b64encode
    return environment.get_template(filename).render(context)


parser = argparse.ArgumentParser()
parser.add_argument("input_file_name", help="Input file")
args = parser.parse_args()

context = {}
for key, value in os.environ.items():
    context[key.lower()] = value

print(render(args.input_file_name, context))
