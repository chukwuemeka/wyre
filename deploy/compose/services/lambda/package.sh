#!/bin/bash

# Create virtualenv
rm -rf /var/task/bin /var/task/lib /var/task/include
virtualenv --always-copy /var/task/ -p /var/lang/bin/python
source /var/task/bin/activate
pip install -r /in/requirements.txt

# Add files to archive
mkdir /var/lambda
cd /var/task/lib/python3.6/site-packages/
zip -r9 /var/lambda/$1 *
cd /in
zip -g /var/lambda/$1 lambda_function.py
