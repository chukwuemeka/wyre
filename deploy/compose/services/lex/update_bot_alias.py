from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import logging
from time import sleep

import boto3

logging.basicConfig(level=logging.INFO, format='%(message)s')
logger = logging.getLogger()

# Args
parser = argparse.ArgumentParser()
parser.add_argument("name", help="Name of the bot")
parser.add_argument("alias", help="Alias to update to highest bot version")
args = parser.parse_args()

# Initialize client
client = boto3.client('lex-models')

# Get Bot Versions
next_token = None
max_version_number = -1
while True:
    if next_token:
        response = client.get_bot_versions(
            name=args.name,
            nextToken=next_token,
            maxResults=1
        )
    else:
        response = client.get_bot_versions(
            name=args.name,
            maxResults=1
        )
    for bot in response.get('bots'):
        if bot.get('version') == '$LATEST':
            continue
        max_version_number = max(max_version_number, int(bot.get('version')))

    next_token = response.get('nextToken')
    if not next_token:
        break

logger.info('max_version_number = {0}'.format(max_version_number))
assert max_version_number > 0, 'Failed to find max version number'

# Wait for the new bot version to build
while True:
    response = client.get_bot(
        name=args.name,
        versionOrAlias=str(max_version_number)
    )
    assert response.get('status') != 'FAILED', 'Bot failed to build'
    if response.get('status') == 'READY':
        break
    logger.info('Waiting for bot to build...')
    sleep(1)

# Get last alias checksum
checksum = None
try:
    response = client.get_bot_alias(
        name=args.alias,
        botName=args.name,
    )
    checksum = response.get('checksum')
except Exception:
    logger.exception('Failed to get checksum lol')
    pass


# Update alias
logger.info('Updating bot alias')
logger.info('checksum = {0}'.format(checksum))
kwargs = dict(
    name=args.alias,
    botVersion=str(max_version_number),
    botName=args.name,
    checksum=checksum
)
if not checksum:
    kwargs.pop('checksum')
response = client.put_bot_alias(**kwargs)
logger.info('Shifted alias {0} to version {1}'.format(args.alias, max_version_number))
