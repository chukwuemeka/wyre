bind = 'unix:///var/run/gunicorn/wyre.sock'
# max_requests = 250
# max_requests_jitter = 25
workers = 3
worker_class = 'gevent'
timeout = 120
preload = True
