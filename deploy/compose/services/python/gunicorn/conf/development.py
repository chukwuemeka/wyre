bind = 'unix:///var/run/gunicorn/wyre.sock'
workers = 1
worker_class = 'gevent'
max_requests = 1
max_requests_jitter = 25

reload = True
