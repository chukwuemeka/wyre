#!/bin/bash

rm -rfv /etc/nginx/sites-enabled/*
cp -rfv $NGINX_CONFIG_FILE_PATH /etc/nginx/sites-enabled/site.conf
export
supervisord -n
