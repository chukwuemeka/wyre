import json
import logging

import os
import requests
from aws_xray_sdk.core import patch
from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core.models import http
from django.core import signing

patch(['requests'])

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
SERVICE_NAME = os.environ.get('SERVICE_NAME')
LEX_WEBHOOK_URL = os.environ.get('LEX_WEBHOOK_URL')
LEX_WEBHOOK_SECRET_KEY = os.environ.get('LEX_WEBHOOK_SECRET_KEY')


def handler(event, context):
    if 'currentIntent' in event:
        logger.info('Received event!\n{0}'.format(json.dumps(event, indent=2)))
        trace_id = event.get('requestAttributes').get('traceId')
        logger.info('lambda_trace_id = {0}'.format(xray_recorder.current_segment().trace_id))
        payload = {'event': signing.dumps(event, key=LEX_WEBHOOK_SECRET_KEY)}
        logger.info('payload = {0}'.format(payload))
        logger.info('LEX_WEBHOOK_URL = {0}'.format(LEX_WEBHOOK_URL))
        response = requests.post(LEX_WEBHOOK_URL, json=payload,
                                 headers={http.ALT_XRAY_HEADER: 'Self={1};Root={0}'.format(trace_id, xray_recorder.current_segment().trace_id)})
        logger.info('response.request.headers = {0}'.format(response.request.headers))
        logger.info('response.status_code = {0}'.format(response.status_code))
        if response.status_code == requests.codes.ok:
            json_response = response.json()
            logger.info('response = {0}'.format(json.dumps(json_response if json_response else {}, indent=2)))
            return json_response
        else:
            logger.info('response.content = {0}'.format(response.content))
    return None
