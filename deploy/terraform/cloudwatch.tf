resource "aws_cloudwatch_metric_alarm" "sqs_twitter_inbox_oldest_message" {
  alarm_name = "${var.app_name}-${var.environment}-sqs-twitter-inbox-oldest-message"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "60"
  statistic = "Average"
  threshold = "7.5"
  alarm_actions = [
    "${aws_sns_topic.sqs_alarms.arn}"]

  dimensions {
    QueueName = "${title(var.app_name)}${title(var.environment)}TwitterInbox.fifo"
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_emails_oldest_message" {
  alarm_name = "${var.app_name}-${var.environment}-sqs-emails-oldest-message"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "60"
  statistic = "Average"
  threshold = "300"
  alarm_actions = [
    "${aws_sns_topic.sqs_alarms.arn}"]

  dimensions {
    QueueName = "${title(var.app_name)}${title(var.environment)}Emails"
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_ethereum_blocks_oldest_message" {
  alarm_name = "${var.app_name}-${var.environment}-sqs-ethereum-blocks-oldest-message"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "60"
  statistic = "Average"
  threshold = "60"
  alarm_actions = [
    "${aws_sns_topic.sqs_alarms.arn}"]

  dimensions {
    QueueName = "${title(var.app_name)}${title(var.environment)}EthereumBlocks.fifo"
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_ethereum_logs_oldest_message" {
  alarm_name = "${var.app_name}-${var.environment}-sqs-ethereum-logs-oldest-message"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "60"
  statistic = "Average"
  threshold = "60"
  alarm_actions = [
    "${aws_sns_topic.sqs_alarms.arn}"]

  dimensions {
    QueueName = "${title(var.app_name)}${title(var.environment)}EthereumLogs.fifo"
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_ethereum_transactions_oldest_message" {
  alarm_name = "${var.app_name}-${var.environment}-sqs-ethereum-transactions-oldest-message"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "60"
  statistic = "Average"
  threshold = "60"
  alarm_actions = [
    "${aws_sns_topic.sqs_alarms.arn}"]

  dimensions {
    QueueName = "${title(var.app_name)}${title(var.environment)}EthereumTransactions.fifo"
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudfront_5xx_error_rate" {
  alarm_name = "${var.app_name}-${var.environment}-cloudfront-num-5xx-error-rate"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "5xxErrorRate"
  namespace = "AWS/Cloudfront"
  period = "60"
  statistic = "Average"
  threshold = "0.75"
  alarm_actions = [
    "${aws_sns_topic.cloudfront_alarms.arn}"]
  insufficient_data_actions = [
    "${aws_sns_topic.cloudfront_alarms.arn}"]

  dimensions {
    DistributionId = "${aws_cloudfront_distribution.default.id}"
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudfront_4xx_error_rate" {
  alarm_name = "${var.app_name}-${var.environment}-cloudfront-num-4xx-error-rate"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "4xxErrorRate"
  namespace = "AWS/Cloudfront"
  period = "60"
  statistic = "Average"
  threshold = "0.75"
  alarm_actions = [
    "${aws_sns_topic.cloudfront_alarms.arn}"]
  insufficient_data_actions = [
    "${aws_sns_topic.cloudfront_alarms.arn}"]

  dimensions {
    DistributionId = "${aws_cloudfront_distribution.default.id}"
  }
}

resource "aws_cloudwatch_metric_alarm" "database_cpu_utilization" {
  alarm_name = "${var.app_name}-${var.environment}-database-cpu-utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "15"
  metric_name = "CPUUtilization"
  namespace = "AWS/RDS"
  period = "60"
  statistic = "Average"
  threshold = "90"
  alarm_actions = [
    "${aws_sns_topic.database_alarms.arn}"]
  insufficient_data_actions = [
    "${aws_sns_topic.database_alarms.arn}"]

  dimensions {
    DBInstanceIdentifier = "${aws_db_instance.default.id}"
  }
}

resource "aws_cloudwatch_metric_alarm" "database_num_connections" {
  alarm_name = "${var.app_name}-${var.environment}-database-num-connections"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "DatabaseConnections"
  namespace = "AWS/RDS"
  period = "60"
  statistic = "Average"
  threshold = "${var.database_max_connections * 0.75}"
  alarm_actions = [
    "${aws_sns_topic.database_alarms.arn}"]
  insufficient_data_actions = [
    "${aws_sns_topic.database_alarms.arn}"]

  dimensions {
    DBInstanceIdentifier = "${aws_db_instance.default.id}"
  }
}
