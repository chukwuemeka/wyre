data "aws_acm_certificate" "default" {
  domain   = "baetoken.com"
  statuses = ["ISSUED"]
}
