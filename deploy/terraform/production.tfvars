debug = "False"
environment = "production"
s3_bucket_name = "wyre-production"
lex_bot_name = "Wyre"
lex_bot_alias = "Production"
site_domain = "baetoken.com"
txt_records = {
  "_amazonses.baetoken.com" = "cq//UZ9jXfFshae3chUJAYTdRZTwcajn7TW5ei4yCx4="
  "baetoken.com" = "amazonses:cq//UZ9jXfFshae3chUJAYTdRZTwcajn7TW5ei4yCx4="
}
cname_records = {
  "mail" = "mail1301.opentransfer.com"
  "m6ux4upavbevxqsvqznajcwe47bfblq6._domainkey.baetoken.com" = "m6ux4upavbevxqsvqznajcwe47bfblq6.dkim.amazonses.com"
  "ygwzhkv5tejab2gmupo6cbtxlderfxvv._domainkey.baetoken.com" = "ygwzhkv5tejab2gmupo6cbtxlderfxvv.dkim.amazonses.com"
  "2zonrroxqgcacsi7oircqfsdy2mw3azm._domainkey.baetoken.com" = "2zonrroxqgcacsi7oircqfsdy2mw3azm.dkim.amazonses.com"
  "www" = "baetoken.com"
  "_domainconnect" = "_domainconnect.gd.domaincontrol.com"
  "_344b9ef8bb45b17dd72659500a0823da.baetoken.com" = "_c5f01bdeff81939bf4f9889f508907d4.acm-validations.aws"
}
mx_records = {
  "baetoken.com" = "1 mail1301.hostexcellence.com"
}
ns_records = {
  "staging" = [
    "ns-1055.awsdns-03.org.",
    "ns-172.awsdns-21.com",
    "ns-1882.awsdns-43.co.uk",
    "ns-732.awsdns-27.net"
  ]
}
database_instance_class = "db.t2.small"
database_allocated_storage = "100"
database_max_connections = "254"