debug = "False"
environment = "staging"
s3_bucket_name = "wyre-staging"
lex_bot_name = "Wyre"
lex_bot_alias = "Staging"
site_domain = "staging.baetoken.com"
txt_records = {
  "_amazonses.staging.baetoken.com" = "keWQnmfkxGR9wVXePnvvNlHXC2wu7OcdaEo1UU1s5Mo="
  "staging.baetoken.com" = "amazonses:keWQnmfkxGR9wVXePnvvNlHXC2wu7OcdaEo1UU1s5Mo="
}
cname_records = {
  "kkcwwvnrx2p4dl52lscjckohfu4lakgh._domainkey.staging.baetoken.com" = "kkcwwvnrx2p4dl52lscjckohfu4lakgh.dkim.amazonses.com"
  "rtyu4io4o3r2w7zukkh5oaehpjmsyf4q._domainkey.staging.baetoken.com" = "rtyu4io4o3r2w7zukkh5oaehpjmsyf4q.dkim.amazonses.com"
  "whxzmngrgdo6qfof3cbiz6war2n2mjoh._domainkey.staging.baetoken.com" = "whxzmngrgdo6qfof3cbiz6war2n2mjoh.dkim.amazonses.com"
  "www" = "staging.baetoken.com"
}
mx_records = {
}
ns_records = {}

database_instance_class = "db.t2.small"
database_allocated_storage = "20"
database_max_connections = "63"
