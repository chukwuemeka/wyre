terraform {
  backend "s3" {
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}