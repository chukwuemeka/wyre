resource "aws_ses_domain_identity" "default" {
  domain = "${var.site_domain}"
}

resource "aws_route53_zone" "default" {
  name = "${var.site_domain}"
}

resource "aws_route53_record" "cloudfront_alias" {
  zone_id = "${aws_route53_zone.default.zone_id}"
  name = ""
  type = "A"
  alias {
    name = "${aws_cloudfront_distribution.default.domain_name}"
    zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
    evaluate_target_health = false
  }
}


resource "aws_route53_record" "txt" {
  count = "${length(var.txt_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.txt_records), count.index)}"
  type    = "TXT"
  ttl     = "600"
  records = ["${lookup(var.txt_records, element(keys(var.txt_records), count.index))}"]
}


resource "aws_route53_record" "cname" {
  count = "${length(var.cname_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.cname_records), count.index)}"
  type    = "CNAME"
  ttl     = "600"
  records = ["${lookup(var.cname_records, element(keys(var.cname_records), count.index))}"]
}

resource "aws_route53_record" "mx" {
  count = "${length(var.mx_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.mx_records), count.index)}"
  type    = "MX"
  ttl     = "600"
  records = ["${lookup(var.mx_records, element(keys(var.mx_records), count.index))}"]
}

resource "aws_route53_record" "ns" {
  count = "${length(var.ns_records)}"
  zone_id = "${aws_route53_zone.default.id}"
  name    = "${element(keys(var.ns_records), count.index)}"
  type    = "NS"
  ttl     = "600"
  records = "${var.ns_records[element(keys(var.ns_records), count.index)]}"
}


output "route53_zone_name_servers" {
  value = "${aws_route53_zone.default.name_servers}"
}
