resource "aws_cloudfront_origin_access_identity" "default" {
  comment = "${var.app_name}"
}

resource "aws_cloudfront_distribution" "default" {
  web_acl_id = "${aws_waf_web_acl.default.id}"
  enabled = true
  price_class = "PriceClass_All"
  aliases = [
    "${var.site_domain}",
    "www.${var.site_domain}"
  ]

  viewer_certificate {
//    cloudfront_default_certificate = "true"
    acm_certificate_arn = "${data.aws_acm_certificate.default.arn}"
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags {
    Name = "${var.app_name}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }

  origin {
    domain_name = "${var.aws_alb_domain}"
    origin_id = "${var.aws_alb_domain}"

    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols = [
//        "SSLv3",
        "TLSv1",
        "TLSv1.1",
        "TLSv1.2",
      ]
    }
  }

  origin {
    domain_name = "${aws_s3_bucket.default.bucket_domain_name}"
    origin_id = "${aws_s3_bucket.default.bucket_domain_name}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path}"
    }
  }

  cache_behavior {
    path_pattern = "${var.django_mediafiles_location}/*"
    allowed_methods = [
      "GET",
      "HEAD"]
    cached_methods = [
      "GET",
      "HEAD"]
    target_origin_id = "${aws_s3_bucket.default.bucket_domain_name}"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 31557600
    max_ttl = 31557600
  }

  cache_behavior {
    path_pattern = "static/*"
    allowed_methods = [
      "GET",
      "HEAD"]
    cached_methods = [
      "GET",
      "HEAD"]
    target_origin_id = "${aws_s3_bucket.default.bucket_domain_name}"
    compress = true

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 31557600
    max_ttl = 31557600
  }

  default_cache_behavior {
    allowed_methods = [
      "DELETE",
      "GET",
      "HEAD",
      "OPTIONS",
      "PATCH",
      "POST",
      "PUT"]
    cached_methods = [
      "GET",
      "HEAD"]
    target_origin_id = "${var.aws_alb_domain}"
    compress = true

    forwarded_values {
      query_string = true
      headers = [
        "Referer",
        "X-CSRFToken",
        "Host"
      ]
      cookies {
        forward = "all"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 31557600
    max_ttl = 31557600
  }
}

