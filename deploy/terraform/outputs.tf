output "AWS_CLOUDFRONT_DOMAIN" {
  value = "${aws_cloudfront_distribution.default.domain_name}"
}

output "POSTGRES_HOST" {
  value = "${aws_db_instance.default.address}"
}

output "POSTGRES_NAME" {
  value = "${aws_db_instance.default.name}"
}

output "POSTGRES_PASSWORD" {
  value = "${aws_db_instance.default.password}"
}

output "POSTGRES_PORT" {
  value = "${aws_db_instance.default.port}"
}

output "POSTGRES_USER" {
  value = "${aws_db_instance.default.username}"
}

output "POD_ROLE_ARN" {
  value = "${aws_iam_role.pod.arn}"
}

output "CLOUDFRONT_DOMAIN_NAME" {
  value = "${aws_cloudfront_distribution.default.domain_name}"
}

