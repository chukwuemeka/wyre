resource "aws_kms_key" "database" {}

resource "aws_kms_alias" "database" {
  name = "alias/${var.app_name}-${var.environment}-database-key-alias"
  target_key_id = "${aws_kms_key.database.key_id}"
}

resource "aws_security_group" "database" {
  name = "${var.app_name}-${var.environment}-database"
  description = "Firewall for database"
  vpc_id = "${var.vpc_id}"
  ingress {
    from_port = "${var.database_port}"
    to_port = "${var.database_port}"
    protocol = "tcp"
    security_groups = [
      "${var.node_security_group_id}"]
  }
  tags {
    Name = "${var.app_name}-${var.environment}-database"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}

resource "aws_db_subnet_group" "default" {
  subnet_ids = [
    "${var.private_subnet_ids}"]
  name = "${var.app_name}-${var.environment}"
}

resource "aws_db_instance" "default" {
  availability_zone    = "${var.database_availability_zone}"
  allocated_storage = "${var.database_allocated_storage}"
  storage_type = "${var.database_storage_type}"
  engine = "${var.database_engine}"
  engine_version = "${var.database_engine_version}"
  auto_minor_version_upgrade = true
  instance_class = "${var.database_instance_class}"
  name = "${var.app_name}"
  identifier = "${var.app_name}-${var.environment}"
  username = "${var.database_user}"
  password = "${var.database_password}"
  port = "${var.database_port}"
  vpc_security_group_ids = [
    "${aws_security_group.database.id}"]
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  storage_encrypted = true
  kms_key_id = "${aws_kms_key.database.arn}"
  multi_az = false
  backup_retention_period = 7

  lifecycle {
    prevent_destroy = true
  }

  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}