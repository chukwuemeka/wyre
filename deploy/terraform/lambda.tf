data "aws_iam_policy_document" "lex_hook" {
  statement {
    actions = [
      "xray:PutTraceSegments",
      "xray:PutTelemetryRecords"
    ]
    resources = [
      "*",
    ]
  }
  statement {
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.default.id}",
      "arn:aws:s3:::${aws_s3_bucket.default.id}/*",
    ]
  }
  statement {
    actions = [
      "cloudfront:ListDistributions",
      "cloudfront:CreateInvalidation"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "ses:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "sqs:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "lex:PostText",
      "lex:PostContent"
    ]
    resources = [
      "arn:aws:lex:${var.aws_region}:${data.aws_caller_identity.current.account_id}:bot:${var.lex_bot_name}:${var.lex_bot_alias}"
    ]
  }
  statement {
    actions = [
      "rds:*"
    ]
    resources = [
      "arn:aws:rds:::${aws_db_instance.default.id}"
    ]
  }
  statement {
    actions = [
      "logs:*"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "ec2:AttachNetworkInterface",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeInstances",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DetachNetworkInterface",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:ResetNetworkInterfaceAttribute"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_role_policy" "lex_hook" {
  name = "${var.app_name}-${var.environment}-LexHookExecutionPolicy"
  role = "${aws_iam_role.lex_hook.id}"
  policy = "${data.aws_iam_policy_document.lex_hook.json}"
}

data "aws_iam_policy_document" "lex_hook_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
        "lambda.amazonaws.com",
        "events.amazonaws.com",
        "apigateway.amazonaws.com",
        "lex.amazonaws.com",
        "ses.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "lex_hook" {
  name = "${var.app_name}-${var.environment}-LexHookExecutionRole"
  assume_role_policy = "${data.aws_iam_policy_document.lex_hook_assume_role.json}"
}


module "lex_hook" {
  source = "./modules/lambda"
  app_name = "${var.app_name}"
  aws_s3_bucket_id = "${aws_s3_bucket.default.id}"
  environment = "${var.environment}"
  environment_variables = {
    SERVICE_NAME = "lambda.${var.site_domain}"
    LEX_WEBHOOK_URL = "https://${var.site_domain}/${var.django_lex_webhook_path}"
    LEX_WEBHOOK_SECRET_KEY = "${var.django_lex_webhook_secret_key}"
  }
  function_name = "${var.app_name}-${var.environment}-lex-hook"
  iam_role_arn = "${aws_iam_role.lex_hook.arn}"
  s3_key = "lambda/lex_hook.zip"
  local_package_file_path = "/var/lambda/lex_hook.zip"
  project_name = "${var.project_name}"
  vpc_id = "${var.vpc_id}"
}