resource "aws_s3_bucket_object" "lambda_function" {
  bucket = "${var.aws_s3_bucket_id}"
  key = "${var.s3_key}"
  source = "${var.local_package_file_path}"
  etag = "${md5(file(var.local_package_file_path))}"
}

resource "aws_lambda_function" "default" {
  s3_bucket = "${var.aws_s3_bucket_id}"
  s3_key = "${var.s3_key}"
  source_code_hash = "${base64sha256(file(var.local_package_file_path))}"
  function_name = "${var.function_name}"
  role = "${var.iam_role_arn}"
  handler = "${var.handler}"
  runtime = "${var.runtime}"
  memory_size = "${var.memory_size}"
  timeout = "${var.timeout}"
  publish = "true"
  tracing_config {
    mode = "Active"
  }
  environment {
    variables = "${var.environment_variables}"
  }
  tags {
    Project = "${var.project_name}"
  }

  depends_on = [
    "aws_s3_bucket_object.lambda_function"]
}
