variable "app_name" {}
variable "environment" {}
variable "vpc_id" {}
variable "aws_s3_bucket_id" {}

variable "runtime" {
  default = "python3.6"
}
variable "handler" {
  default = "lambda_function.handler"
}
variable "memory_size" {
  default = 128
}
variable "timeout" {
  default = 300
}
variable "s3_key" {}
variable "local_package_file_path" {}

variable "project_name" {}

variable "environment_variables" {
  type = "map"
}

variable "iam_role_arn" {}

variable "function_name" {}