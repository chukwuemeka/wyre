resource "aws_waf_web_acl" "default" {
  depends_on = [
    "aws_waf_rule.admin_access",
    "aws_waf_rule.csrf_token",
    "aws_waf_rule.path_traversal",
    "aws_waf_rule.xss",
    "aws_waf_rule.sql_injection",
    "aws_waf_rule.size_restrictions",
    "aws_waf_rule.server_side_includes",
    "aws_waf_rule.ip_blacklist",
  ]
  name = "${var.app_name}-${var.environment}-global"
  metric_name = "${title(var.app_name)}${title(var.environment)}Global"

  default_action {
    type = "ALLOW"
  }

  rules {
    priority = 100
    rule_id = "${aws_waf_rule.admin_access.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }

  rules {
    priority = 90
    rule_id = "${aws_waf_rule.server_side_includes.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }


//  rules {
//    priority = 80
//    rule_id = "${aws_waf_rule.csrf_token.id}"
//    type = "REGULAR"
//    action {
//      type = "BLOCK"
//    }
//  }

  rules {
    priority = 60
    rule_id = "${aws_waf_rule.path_traversal.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }

  rules {
    priority = 50
    rule_id = "${aws_waf_rule.xss.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }

  rules {
    priority = 40
    rule_id = "${aws_waf_rule.sql_injection.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }

  rules {
    priority = 20
    rule_id = "${aws_waf_rule.ip_blacklist.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }

  rules {
    priority = 10
    rule_id = "${aws_waf_rule.size_restrictions.id}"
    type = "REGULAR"
    action {
      type = "BLOCK"
    }
  }
}

// 1.
// OWASP Top 10 A1
// Mitigate SQL Injection Attacks
// Matches attempted SQLi patterns in the URI, QUERY_STRING, BODY, COOKIES
resource "aws_waf_rule" "sql_injection" {
  name = "${var.app_name}-${var.environment}-sql-injection"
  metric_name = "${title(var.app_name)}${title(var.environment)}SqlInjection"

  predicates {
    data_id = "${aws_waf_sql_injection_match_set.default.id}"
    negated = false
    type = "SqlInjectionMatch"
  }
}

resource "aws_waf_sql_injection_match_set" "default" {
  name = "${var.app_name}-${var.environment}-sql-injection-match-set"

  sql_injection_match_tuples {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "URI"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "URI"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "QUERY_STRING"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "QUERY_STRING"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "BODY"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "BODY"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "URL_DECODE"
    field_to_match {
      type = "HEADER"
      data = "cookie"
    }
  }

  sql_injection_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    field_to_match {
      type = "HEADER"
      data = "cookie"
    }
  }
}

// 2.
// OWASP Top 10 A2
// Blacklist bad/hijacked JWT tokens or session IDs
// Matches the specific values in the cookie or Authorization header
// for JWT it is sufficient to check the signature
//resource "aws_waf_byte_match_set" "default" {
//  name = "${var.app_name}-${var.environment}-byte-match-set"
//
//  byte_match_tuples {
//    text_transformation   = "URL_DECODE"
//    target_string         = "CHANGEMEMEMEM NO!!!!"
//    positional_constraint = "CONTAINS"
//
//    field_to_match {
//      type = "HEADER"
//      data = "cookie"
//    }
//  }
//
//  byte_match_tuples {
//    text_transformation   = "URL_DECODE"
//    target_string         = "CHANGEMEMEMEM NO!!!!"
//    positional_constraint = "ENDS_WITH"
//
//    field_to_match {
//      type = "HEADER"
//      data = "authorization"
//    }
//  }
//}

// 3.
// OWASP Top 10 A3
// Mitigate Cross Site Scripting Attacks
// Matches attempted XSS patterns in the URI, QUERY_STRING, BODY, COOKIES
resource "aws_waf_rule" "xss" {
  name = "${var.app_name}-${var.environment}-xss"
  metric_name = "${title(var.app_name)}${title(var.environment)}Xss"

  predicates {
    data_id = "${aws_waf_xss_match_set.default.id}"
    negated = false
    type = "XssMatch"
  }
}

resource "aws_waf_xss_match_set" "default" {
  name = "${var.app_name}-${var.environment}-xss-match-set"

  xss_match_tuples {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "URI"
    }
  }

  xss_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "URI"
    }
  }

  xss_match_tuples {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  xss_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  xss_match_tuples {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "BODY"
    }
  }

  xss_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "BODY"
    }
  }

  xss_match_tuples {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "HEADER"
      data = "cookie"
    }
  }

  xss_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "HEADER"
      data = "cookie"
    }
  }
}

// 4.
// OWASP Top 10 A4
// Path Traversal, LFI, RFI
// Matches request patterns designed to traverse filesystem paths, and include
// local or remote files
resource "aws_waf_rule" "path_traversal" {
  name = "${var.app_name}-${var.environment}-path-traversal"
  metric_name = "${title(var.app_name)}${title(var.environment)}PathTraversal"

  predicates {
    data_id = "${aws_waf_byte_match_set.path_traversal.id}"
    negated = false
    type = "ByteMatch"
  }
}

resource "aws_waf_byte_match_set" "path_traversal" {
  name = "${var.app_name}-${var.environment}-path-traversal"

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "../"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "URI"
    }
  }

  byte_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    target_string = "../"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "URI"
    }
  }

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "../"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  byte_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    target_string = "../"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "://"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "URI"
    }
  }

  byte_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    target_string = "://"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "URI"
    }
  }

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "://"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  byte_match_tuples {
    text_transformation = "HTML_ENTITY_DECODE"
    target_string = "://"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "QUERY_STRING"
    }
  }
}

// 5.
// OWASP Top 10 A4
// Privileged Module Access Restrictions
// Restrict access to the admin interface to known source IPs only
// Matches the URI prefix, when the remote IP isn't in the whitelist
resource "aws_waf_rule" "admin_access" {
  name = "${var.app_name}-${var.environment}-admin-access"
  metric_name = "${title(var.app_name)}${title(var.environment)}AdminAccess"

  predicates {
    data_id = "${aws_waf_byte_match_set.admin_url.id}"
    negated = false
    type = "ByteMatch"
  }
}

resource "aws_waf_byte_match_set" "admin_url" {
  name = "${var.app_name}-${var.environment}-admin-url"

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "/admin"
    positional_constraint = "STARTS_WITH"

    field_to_match {
      type = "URI"
    }
  }
}


// 7.
// OWASP Top 10 A7
// Mitigate abnormal requests via size restrictions
// Enforce consistent request hygene, limit size of key elements
resource "aws_waf_rule" "size_restrictions" {
  name = "${var.app_name}-${var.environment}-size-restrictions"
  metric_name = "${title(var.app_name)}${title(var.environment)}SizeRestrictions"

  predicates {
    data_id = "${aws_waf_size_constraint_set.request.id}"
    negated = false
    type = "SizeConstraint"
  }
}

resource "aws_waf_size_constraint_set" "request" {
  name = "${var.app_name}-${var.environment}-request"
  size_constraints {
    comparison_operator = "GT"
    "field_to_match" {
      type = "URI"
    }
    size = 512
    text_transformation = "NONE"
  }
  size_constraints {
    comparison_operator = "GT"
    "field_to_match" {
      type = "QUERY_STRING"
    }
    size = 1024
    text_transformation = "NONE"
  }
  size_constraints {
    comparison_operator = "GT"
    "field_to_match" {
      type = "BODY"
    }
    size = 4096
    text_transformation = "NONE"
  }
  size_constraints {
    comparison_operator = "GT"
    "field_to_match" {
      type = "HEADER"
      data = "cookie"
    }
    size = 4093
    text_transformation = "NONE"
  }
}


// 8.
// OWASP Top 10 A8
// CSRF token enforcement example
// Enforce the presence of CSRF token in request header
resource "aws_waf_rule" "csrf_token" {
  name = "${var.app_name}-${var.environment}-csrf-token"
  metric_name = "${title(var.app_name)}${title(var.environment)}CsrfToken"

  predicates {
    data_id = "${aws_waf_size_constraint_set.csrf_token.id}"
    negated = false
    type = "SizeConstraint"
  }

  predicates {
    data_id = "${aws_waf_byte_match_set.csrf_method.id}"
    negated = false
    type = "ByteMatch"
  }

  predicates {
    data_id = "${aws_waf_byte_match_set.lead_creation_page.id}"
    negated = true
    type = "ByteMatch"
  }

  predicates {
    data_id = "${aws_waf_byte_match_set.lex_webhook.id}"
    negated = true
    type = "ByteMatch"
  }

}

resource "aws_waf_byte_match_set" "csrf_method" {
  name = "${var.app_name}-${var.environment}-csrf-method"
  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = "post"
    positional_constraint = "EXACTLY"

    field_to_match {
      type = "METHOD"
    }
  }
}

resource "aws_waf_size_constraint_set" "csrf_token" {
  name = "${var.app_name}-${var.environment}-csrf-token"
  size_constraints {
    comparison_operator = "EQ"
    "field_to_match" {
      type = "HEADER"
      data = "x-csrftoken"
    }
    size = 64
    text_transformation = "LOWERCASE"
  }
}

resource "aws_waf_byte_match_set" "lead_creation_page" {
  name = "${var.app_name}-${var.environment}-lead-creation-page"

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "/"
    positional_constraint = "EXACTLY"

    field_to_match {
      type = "URI"
    }
  }
}

resource "aws_waf_byte_match_set" "lex_webhook" {
  name = "${var.app_name}-${var.environment}-lex-webhook"

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "/${var.django_lex_webhook_path}"
    positional_constraint = "STARTS_WITH"

    field_to_match {
      type = "URI"
    }
  }
}


## 9.
## OWASP Top 10 A9
## Server-side includes & libraries in webroot
## Matches request patterns for webroot objects that shouldn't be directly accessible
resource "aws_waf_rule" "server_side_includes" {
  name = "${var.app_name}-${var.environment}-server-side-includes"
  metric_name = "${title(var.app_name)}${title(var.environment)}ServerSideIncludes"

  predicates {
    data_id = "${aws_waf_byte_match_set.server_side_includes.id}"
    negated = false
    type = "ByteMatch"
  }

}

resource "aws_waf_byte_match_set" "server_side_includes" {
  name = "${var.app_name}-${var.environment}-server-side-includes"

  byte_match_tuples {
    text_transformation = "URL_DECODE"
    target_string = "/includes"
    positional_constraint = "STARTS_WITH"

    field_to_match {
      type = "URI"
    }
  }

  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".cfg"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }

  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".conf"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }
  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".config"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }
  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".ini"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }
  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".log"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }
  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".bak"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }
  byte_match_tuples {
    text_transformation = "LOWERCASE"
    target_string = ".backup"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "URI"
    }
  }
}


## 10.
## Generic
## IP Blacklist
## Matches IP addresses that should not be allowed to access content
resource "aws_waf_rule" "ip_blacklist" {
  name = "${var.app_name}-${var.environment}-ip-blacklist"
  metric_name = "${title(var.app_name)}${title(var.environment)}IpBlacklist"

  predicates {
    data_id = "${aws_waf_ipset.ip_blacklist.id}"
    negated = false
    type = "IPMatch"
  }

}

resource "aws_waf_ipset" "ip_blacklist" {
  name = "${var.app_name}-${var.environment}-ip-blacklist"

  ip_set_descriptors {
    type  = "IPV4"
    value = "10.0.0.0/8"
  }
  ip_set_descriptors {
    type  = "IPV4"
    value = "192.168.0.0/16"
  }
  ip_set_descriptors {
    type  = "IPV4"
    value = "169.254.0.0/16"
  }
  ip_set_descriptors {
    type  = "IPV4"
    value = "172.16.0.0/16"
  }
  ip_set_descriptors {
    type  = "IPV4"
    value = "127.0.0.1/32"
  }
}