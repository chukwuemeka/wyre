variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "django_secret_key" {}
variable "environment" {}
variable "debug" {}
variable "site_domain" {}

variable "aws_region" {
  default = "us-east-1"
}
variable "aws_availability_zone" {
  default = "us-east-1a"
}

variable "api_name" {
  default = "Wyre"
}
variable "app_name" {
  default = "wyre"
}
variable "project_name" {
  default = "Wyre"
}
variable "s3_bucket_name" {}

variable "database_user" {
  default = "postgres"
}
variable "database_password" {
  default = "c38bab650dcb46c59e0541fa0bc23b96"
}
variable "database_max_connections" {
}
variable "database_instance_class" {
}
variable "database_engine" {
  default = "postgres"
}
variable "database_engine_version" {
  default = "9.6.6"
}
variable "database_allocated_storage" {
}
variable "database_storage_type" {
  default = "gp2"
}
variable "database_port" {
  default = 5432
}
variable "database_availability_zone" {
  default = "us-east-1b"
}

variable "django_staticfiles_location" {}
variable "django_mediafiles_location" {
  default = "media"
}
variable "django_lex_webhook_path" {
}
variable "django_lex_webhook_secret_key" {
}

variable "lex_bot_name" {}
variable "lex_bot_alias" {}

variable "commit_hash" {}
variable "sms_numbers" {
  type = "list"
}

variable "vpc_id" {}
variable "nat_gateway_security_group_id" {}
variable "node_security_group_id" {}
variable "private_subnet_ids" {
  type = "list"
}
variable "public_subnet_ids" {
  type = "list"
}
variable "node_role_arn" {}

variable "txt_records" {
  type = "map"
}

variable "cname_records" {
  type = "map"
}

variable "mx_records" {
  type = "map"
}

variable "ns_records" {
  type = "map"
}

variable "aws_alb_domain" {}
variable "aws_alb_zone_id" {}
