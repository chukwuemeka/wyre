resource "aws_lambda_permission" "lex" {
  statement_id   = "AllowExecutionFromLex"
  action         = "lambda:InvokeFunction"
  function_name  = "${module.lex_hook.function_name}"
  principal      = "lex.amazonaws.com"
  source_arn = "arn:aws:lex:${var.aws_region}:${data.aws_caller_identity.current.account_id}:intent:*:*"
}
