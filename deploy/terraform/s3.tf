data "aws_iam_policy_document" "s3_bucket" {
  statement {
    principals {
      identifiers = ["${aws_cloudfront_origin_access_identity.default.s3_canonical_user_id}"]
      type = "CanonicalUser"
    }
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
  }
  statement {
    principals {
      identifiers = [
        "*",
      ]
      type = "*"
    }
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*",
    ]
    condition {
      test = "StringEquals"
      values = ["${var.vpc_id}"]
      variable = "aws:sourceVpc"
    }
  }
}


resource "aws_s3_bucket" "default" {
  bucket = "${var.app_name}-${var.environment}"
  acl    = "private"
  policy = "${data.aws_iam_policy_document.s3_bucket.json}"
  cors_rule {
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    allowed_headers = ["Authorization"]
  }
  tags {
    Name = "${var.app_name}"
    Environment = "${var.environment}"
    Project = "${var.project_name}"
  }
}