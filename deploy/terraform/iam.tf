resource "aws_iam_user" "pod" {
  name = "${var.app_name}-${var.environment}-pod"
}

resource "aws_iam_user_policy" "pod" {
  user = "${aws_iam_user.pod.name}"
  policy = "${data.aws_iam_policy_document.pod.json}"
}