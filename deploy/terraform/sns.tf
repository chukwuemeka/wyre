resource "aws_sns_topic" "sqs_alarms" {
  name = "${var.app_name}-${var.environment}-sqs-alarms"
}

resource "aws_sns_topic" "database_alarms" {
  name = "${var.app_name}-${var.environment}-database-alarms"
}

resource "aws_sns_topic" "cloudfront_alarms" {
  name = "${var.app_name}-${var.environment}-cloudfront-alarms"
}

resource "aws_sns_topic_subscription" "database_alarms_sms" {
  count = "${length(var.sms_numbers)}"
  topic_arn = "${aws_sns_topic.database_alarms.arn}"
  protocol = "sms"
  endpoint = "${var.sms_numbers[count.index]}"
}

resource "aws_sns_topic_subscription" "cloudfront_alarms_sms" {
  count = "${length(var.sms_numbers)}"
  topic_arn = "${aws_sns_topic.cloudfront_alarms.arn}"
  protocol = "sms"
  endpoint = "${var.sms_numbers[count.index]}"
}

resource "aws_sns_topic_subscription" "sqs_alarms_sms" {
  count = "${length(var.sms_numbers)}"
  topic_arn = "${aws_sns_topic.sqs_alarms.arn}"
  protocol = "sms"
  endpoint = "${var.sms_numbers[count.index]}"
}
