data "aws_iam_policy_document" "pod" {
  statement {
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.default.id}",
      "arn:aws:s3:::${aws_s3_bucket.default.id}/*",
    ]
  }
  statement {
    actions = [
      "sdb:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "cloudfront:ListDistributions",
      "cloudfront:CreateInvalidation"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "xray:*",
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "ses:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "sqs:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "rds:*"
    ]
    resources = [
      "arn:aws:rds:::${aws_db_instance.default.id}"
    ]
  }
  statement {
    actions = [
      "logs:*"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "ec2:AttachNetworkInterface",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeInstances",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DetachNetworkInterface",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:ResetNetworkInterfaceAttribute"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "lex:PostText",
      "lex:PostContent"
    ]
    resources = [
      "arn:aws:lex:${var.aws_region}:${data.aws_caller_identity.current.account_id}:bot:${var.lex_bot_name}:${var.lex_bot_alias}"
    ]
  }
}

resource "aws_iam_role_policy" "pod" {
  name = "${var.app_name}-${var.environment}-pod-policy"
  role = "${aws_iam_role.pod.id}"
  policy = "${data.aws_iam_policy_document.pod.json}"
}

//arn:aws:sts::245984414167:assumed-role/nodes.legion.k8s.local/i-0c912db7257f9b178
data "aws_iam_policy_document" "pod_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
      ]
    }
  }
  statement {
    actions = [
      "sts:AssumeRole"]
    principals {
      type = "AWS"
      identifiers = [
        //        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.app_name}-${var.environment}-pod-role",
        "${var.node_role_arn}"
      ]
    }
  }
}


resource "aws_iam_role" "pod" {
  name = "${var.app_name}-${var.environment}-pod-role"
  assume_role_policy = "${data.aws_iam_policy_document.pod_assume_role.json}"
}
