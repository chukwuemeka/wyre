import bjoern
import argparse
import logging
from wyre import wsgi

# Args
parser = argparse.ArgumentParser()
parser.add_argument("--host", dest='host', required=True)
parser.add_argument("--port", dest='port', type=int, required=True)
args = parser.parse_args()
logging.basicConfig(level=logging.INFO, format='%(message)s')
logger = logging.getLogger()
logger.info('host = {}'.format(args.host))
logger.info('port = {}'.format(args.port))
logger.info('Starting bjoern!')
bjoern.run(wsgi.application, args.host, args.port)
logger.info('Ending bjoern!')
