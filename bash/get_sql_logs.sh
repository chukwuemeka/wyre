#!/bin/bash


get_sql_logs() {
  start_time=$1
  app=$2
  log_file=$3
  first_cut_file=$(mktemp)
  second_cut_file=$(mktemp)
  docker-compose -p wyre -f /vagrant/deploy/compose/development.yml run web ./manage.py eslogs --namespace wyre-staging --app $app --start "$start_time" | egrep '\([0-9]\.[0-9]*\)' | tee $log_file
  cat $log_file | cut -d ' ' -f 1,2,3,4 --output-delimiter='|' | tr -d '()' > $first_cut_file
  cat $log_file | cut -d ' ' -f 5- --output-delimiter=' ' > $second_cut_file
  paste -d '|' $first_cut_file $second_cut_file > $log_file
  rm $first_cut_file
  rm $second_cut_file
}

worker_logs=$(mktemp)
web_logs=$(mktemp)
inbox_worker_logs=$(mktemp)

get_sql_logs "$1" "web" ${web_logs}
get_sql_logs "$1" "inbox-worker" ${inbox_worker_logs}
get_sql_logs "$1" "worker" ${worker_logs}

cat $web_logs $worker_logs $inbox_worker_logs > $2
