from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class AbstractModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Profile(AbstractModel):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True
    )
    braintree_id = models.CharField(max_length=100, unique=True, null=True, blank=True)
    twitter_id = models.CharField(max_length=100, unique=True, null=True, blank=True)
    twitter_screen_name = models.CharField(max_length=100, default='')
