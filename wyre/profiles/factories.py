from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid

import factory
from faker import Faker

from .models import Profile


class ProfileFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory('wyre.users.factories.UserFactory')

    @factory.lazy_attribute
    def twitter_screen_name(self):
        faker = Faker()
        return '{0}'.format(faker.user_name())

    @factory.lazy_attribute
    def twitter_id(self):
        return uuid.uuid4().hex

    class Meta:
        model = Profile
