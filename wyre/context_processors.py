from __future__ import absolute_import

from django.conf import settings


def api_keys(request):
    return {
        'facebook_public_key': getattr(settings, 'FACEBOOK_PUBLIC_KEY', None),
        'stripe_public_key': getattr(settings, 'STRIPE_PUBLIC_KEY', None),
    }


def constants(request):
    return {
        'site_domain': getattr(settings, 'SITE_DOMAIN', None),
        'site_display_name': getattr(settings, 'SITE_DISPLAY_NAME', None),
        'site_tagline': getattr(settings, 'SITE_TAGLINE', None),
        'login_url': getattr(settings, 'LOGIN_URL', None),
        'email_closing': getattr(settings, 'EMAIL_CLOSING', None),
        'support_email': getattr(settings, 'SUPPORT_EMAIL', None),
        'info_email': getattr(settings, 'INFO_EMAIL', None),
        'facebook_page_url': getattr(settings, 'FACEBOOK_PAGE_URL', None),
        'twitter_profile_url': getattr(settings, 'TWITTER_PROFILE_URL', None),
        'twitter_screen_name': getattr(settings, 'TWITTER_SCREEN_NAME', None),
        'bot_twitter_profile_url': getattr(settings, 'BOT_TWITTER_PROFILE_URL', None),
        'erc20_token_symbol': getattr(settings, 'ERC20_TOKEN_SYMBOL', None),
        'bot_display_name': getattr(settings, 'BOT_DISPLAY_NAME', None),

    }
