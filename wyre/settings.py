import ast
import json
import sys
from datetime import timedelta
from decimal import Decimal

import os
from eth_utils.address import to_checksum_address
from moneyed import Money

ENVIRONMENT = os.environ['ENVIRONMENT']

IS_TEST_MODE = len(sys.argv) > 1 and sys.argv[1] == 'test'

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = ast.literal_eval(os.environ.get('DEBUG', 'False').capitalize())

DEFAULT_EXCEPTION_REPORTER_FILTER = 'wyre.debug.SafeExceptionReporterFilter'

APPEND_SLASH = True
if DEBUG:
    ALLOWED_HOSTS = [
        '*'
    ]
else:
    ALLOWED_HOSTS = [
        '*'
    ]
    # NODE_SERVICE_PORT = 30000
    # ALLOWED_HOSTS = [
    #     'www.%s' % os.environ['SITE_DOMAIN'],
    #     os.environ['SITE_DOMAIN']
    # ]
    # ALLOWED_HOSTS += ['172.20.{0}.{1}:{2}'.format(i, j, NODE_SERVICE_PORT) for i in range(256) for j in range(256)]
    # # ALLOWED_HOSTS += ['172.20.{0}.{1}:30001'.format(i, j) for i in range(256) for j in range(256)]
    # if 'AWS_ALB_DOMAIN' in os.environ:
    #     ALLOWED_HOSTS.append(os.environ['AWS_ALB_DOMAIN'])
    # if 'AWS_CLOUDFRONT_DOMAIN' in os.environ:
    #     ALLOWED_HOSTS.append(os.environ['AWS_CLOUDFRONT_DOMAIN'])
    # if 'AWS_CONTAINER_INSTANCE_DOMAIN' in os.environ:
    #     ALLOWED_HOSTS.append(os.environ['AWS_CONTAINER_INSTANCE_DOMAIN'])
    # if 'AWS_CONTAINER_INSTANCE_IP_ADDRESS' in os.environ:
    #     ALLOWED_HOSTS.append(os.environ['AWS_CONTAINER_INSTANCE_IP_ADDRESS'])

INTERNAL_IPS = [
    'localhost',
    'localhost:4497',
    '127.0.0.1',
    '0.0.0.0',
    '0.0.0.0:80',
]

AUTH_USER_MODEL = 'users.User'

# Application definition

INSTALLED_APPS = [
    'wyre.elasticsearch',
    'wyre.email',
    'wyre.leads',
    'wyre.users',
    'wyre.twitter',
    'wyre.utils',
    'wyre.profiles',
    'wyre.lex',
    'wyre.ethereum',
    'wyre.braintree',
    'django_extensions',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'admin_honeypot',
    'rest_framework',
    'django_celery_beat',
    'aws_xray_sdk.ext.django'
]

MIDDLEWARE = [
    'aws_xray_sdk.ext.django.middleware.XRayMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
]

ROOT_URLCONF = 'wyre.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'wyre/templates/')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'wyre.context_processors.api_keys',
                'wyre.context_processors.constants',
            ],
        },
    },
]

WSGI_APPLICATION = 'wyre.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.environ['POSTGRES_NAME'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ['POSTGRES_HOST'],
        'PORT': os.environ['POSTGRES_PORT'],
    }
}

# Cache
REDIS_HOST = os.environ['REDIS_HOST']
REDIS_PORT = os.environ['REDIS_PORT']
CACHE_MIDDLEWARE_ALIAS = 'site'
CACHE_MIDDLEWARE_SECONDS = 60 * 60 * 24
CACHE_BACKEND = os.environ['CACHE_BACKEND']
CACHE_KEY_PREFIX = os.environ.get('CACHE_KEY_PREFIX')
CACHE_VERSION = os.environ.get('CACHE_VERSION')
url = ''
if CACHE_BACKEND == 'django_redis.cache.RedisCache':
    url = 'redis://%s:%s' % (
        os.environ['REDIS_HOST'],
        os.environ['REDIS_PORT']
    )
CACHES = {
    'redlock': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/7' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'web3': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/6' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'email': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/5' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'sessions': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/4' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'thumbnails': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/3' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'site': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/2' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'default': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/1' % (url,),
        'KEY_PREFIX': os.environ['CACHE_KEY_PREFIX'],
        'VERSION': os.environ['CACHE_VERSION'],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# AWS
AWS_LEX_BOT_NAME = os.environ['AWS_LEX_BOT_NAME']
AWS_LEX_BOT_ALIAS = os.environ['AWS_LEX_BOT_ALIAS']
AWS_DEFAULT_ACL = 'private'
AWS_S3_SECURE_URLS = True
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_CLOUDFRONT_DOMAIN = os.environ['AWS_CLOUDFRONT_DOMAIN']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
# AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
# AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_S3_REGION_NAME = 'us-east-1'
AWS_AUTO_CREATE_BUCKET = False
AWS_S3_CUSTOM_DOMAIN = AWS_CLOUDFRONT_DOMAIN
S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN

# Media
DEFAULT_FILE_STORAGE = os.environ['DEFAULT_FILE_STORAGE']
if IS_TEST_MODE or DEFAULT_FILE_STORAGE == 'django.core.files.storage.FileSystemStorage':
    MEDIA_URL = 'http://localhost:4197/media/'
    MEDIA_ROOT = '/media'
    if not os.path.exists(MEDIA_ROOT):
        os.makedirs(MEDIA_ROOT)
else:
    MEDIAFILES_LOCATION = 'media'
    MEDIA_URL = '%s/%s/' % (S3_URL, MEDIAFILES_LOCATION)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
STATICFILES_DOMAIN = os.environ.get('STATICFILES_DOMAIN', AWS_S3_CUSTOM_DOMAIN)
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'wyre/static/dist/')]
STATICFILES_STORAGE = os.environ['STATICFILES_STORAGE']
if IS_TEST_MODE or STATICFILES_STORAGE == 'django.contrib.staticfiles.storage.StaticFilesStorage':
    STATIC_URL = '/static/'
    STATIC_ROOT = '/static'
    if not os.path.exists(STATIC_ROOT):
        os.makedirs(STATIC_ROOT)
else:
    STATICFILES_LOCATION = os.environ['STATICFILES_LOCATION']
    STATIC_URL = 'https://%s/%s/' % (STATICFILES_DOMAIN, STATICFILES_LOCATION)

# # GeoDjango
# geo_dir = os.path.dirname(os.environ['GEOS_LIBRARY_PATH'])
# file_paths = [
#     (os.path.join(geo_dir, 'libgeos-3.4.2.so'), os.path.join(geo_dir, 'libgeos.so')),
#     (os.path.join(geo_dir, 'libgeos_c.so.1.8.2'), os.path.join(geo_dir, 'libgeos_c.so.1')),
#     (os.path.join(geo_dir, 'libgeos_c.so.1'), os.path.join(geo_dir, 'libgeos_c.so')),
# ]
# for src, dest in file_paths:
#     if not os.path.exists(dest):
#         os.symlink(src, dest)
# GDAL_LIBRARY_PATH = os.environ['GDAL_LIBRARY_PATH']
# GEOS_LIBRARY_PATH = os.environ['GEOS_LIBRARY_PATH']
# GDAL_LIBRARY_PATH = '/tmp/wyre/lib/libgdal.so'
# GEOS_LIBRARY_PATH = '/tmp/wyre/lib/libgeos_c.so'

# Which site...?
SITE_ID = 1
SITE_DOMAIN = os.environ['SITE_DOMAIN']  # 'localhost:5031'
SITE_NAME = os.environ['SITE_NAME']  # 'SHOTZU'
SITE_DISPLAY_NAME = os.environ['SITE_DISPLAY_NAME']  # 'SHOTZU'
SITE_TAGLINE = 'Put Your Money Where Your Emoji Is'

# Wagtail
WAGTAIL_SITE_NAME = SITE_NAME
WAGTAILIMAGES_IMAGE_MODEL = 'wagtail_images.WagtailImage'

# Sessions
SESSION_COOKIE_SECURE = not DEBUG
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_CACHE_ALIAS = 'sessions'

# Bot
BOT_DISPLAY_NAME = 'Bae'
BOT_TWITTER_PROFILE_URL = 'https://twitter.com/baetoken'
TOKEN_PURCHASE_ORDER_TIMEOUT = 60 * 10
WEI_PURCHASE_ORDER_TIMEOUT = 60 * 10
TOKEN_PRICE = 0.01

# Email
ADMINS = [
    ('Chukwuemeka Ezekwe', 'cue0083@gmail.com')
]
STAFF = (
    ('Chukwuemeka Ezekwe', 'cue0083@gmail.com'),
    # ('Chukwuemeka Ezekwe', 'emeka@baetoken.com'),
    ('Julian Everly', 'julian@julianeverly.com'),
    # ('Julian Everly', 'julian@baetoken.com')
)

WRAPPED_EMAIL_BACKEND = os.environ['EMAIL_BACKEND']
EMAIL_BACKEND = 'wyre.email.backends.CeleryEmailBackend'
SERVER_EMAIL = 'Bot <bot@%s>' % SITE_DOMAIN
DEFAULT_FROM_EMAIL = 'Bae <hello@%s>' % SITE_DOMAIN
SUPPORT_EMAIL = os.environ.get('SUPPORT_EMAIL', 'support@%s' % SITE_DOMAIN)
INFO_EMAIL = 'support@%s' % SITE_DOMAIN
EMAIL_CLOSING = 'Peace,\nThe {0} Token Team'.format(os.environ['ERC20_TOKEN_SYMBOL'])
STAFF_EMAILS = [staff_email[1] for staff_email in STAFF]

# Logging
# 'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
LOG_LEVEL = os.getenv('DJANGO_LOG_LEVEL', 'INFO')
if ast.literal_eval(os.environ.get('IS_LOGGING_ENABLED', 'True').capitalize()):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            },
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            },
            'require_is_test_mode_false': {
                '()': 'wyre.log.RequireIsTestModeFalse',
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'stream': sys.stdout
            },
            'mail_admins': {
                'level': 'ERROR',
                'class': 'wyre.log.AdminEmailHandler',
                'include_html': True,
                'filters': ['require_is_test_mode_false'],
            }
        },
        'loggers': {
            '': {
                'handlers': ['console', 'mail_admins'],
                'level': LOG_LEVEL,
                'propagate': False
            },
            'django': {
                'handlers': ['console', 'mail_admins'],
                'level': LOG_LEVEL,
                'propagate': False
            },
            'django.request': {
                'handlers': ['console', 'mail_admins'],
                'level': LOG_LEVEL,
                'propagate': False
            },
            # 'django.db.backends': {
            #     'handlers': ['console'],
            #     'level': LOG_LEVEL,
            #     'propagate': False
            # },
            'wyre': {
                'handlers': ['console', 'mail_admins'],
                'level': LOG_LEVEL,
                'propagate': False
            },
            'celery.task': {
                'handlers': ['console', 'mail_admins'],
                'level': LOG_LEVEL,
                'propagate': False
            },
            'aws_xray_sdk': {
                'handlers': ['console', 'mail_admins'],
                'level': 'ERROR',
                'propagate': False
            },
        },
    }

# Stripe
STRIPE_PUBLIC_KEY = os.environ.get('STRIPE_PUBLIC_KEY')
STRIPE_SECRET_KEY = os.environ.get('STRIPE_SECRET_KEY')

# For Zappa
DO_REMOVE_WSGI_SCRIPT_ALIAS = ast.literal_eval(os.environ.get('DO_REMOVE_WSGI_SCRIPT_ALIAS', 'False').capitalize())
WSGI_SCRIPT_ALIAS = os.environ.get('WSGI_SCRIPT_ALIAS', None)
# if DO_REMOVE_WSGI_SCRIPT_ALIAS:
#     FORCE_SCRIPT_NAME = WSGI_SCRIPT_ALIAS

# Minify
HTML_MINIFY = not IS_TEST_MODE and not DEBUG

# CSRF
CSRF_TRUSTED_ORIGINS = ALLOWED_HOSTS
USE_X_FORWARDED_HOST = False
CSRF_COOKIE_HTTPONLY = False
# CSRF_COOKIE_SECURE = False
# CSRF_USE_SESSIONS = True

# Connect
TWITTER_PROFILE_URL = 'https://twitter.com/baetoken/'
FACEBOOK_PAGE_URL = 'https://www.facebook.com/BAE-Token-167542473853444/'

# Celery
CELERY_ACCEPT_CONTENT = ['pickle', 'json']
CELERY_WORKER_PREFETCH_MULTIPLIER = 1
CELERY_WORKER_HIJACK_ROOT_LOGGER = False
CELERY_BROKER_URL = os.environ.get('BROKER_URL')
CELERY_BROKER_TRANSPORT_OPTIONS = {
    'visibility_timeout': 120
}
CELERY_TASK_DEFAULT_QUEUE = os.environ.get('CELERY_TASK_DEFAULT_QUEUE')
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERY_BEAT_SCHEDULE = {
    'wyre.ethereum.tasks.set_usd_to_wei_exchange_rate': {
        'task': 'wyre.ethereum.tasks.set_usd_to_wei_exchange_rate',
        'schedule': timedelta(seconds=5)
    },
    'wyre.ethereum.tasks.set_wei_transfer_gas_estimate': {
        'task': 'wyre.ethereum.tasks.set_wei_transfer_gas_estimate',
        'schedule': timedelta(minutes=4)
    },
    'wyre.ethereum.tasks.set_token_transfer_gas_estimate': {
        'task': 'wyre.ethereum.tasks.set_token_transfer_gas_estimate',
        'schedule': timedelta(minutes=7)
    },
    # 'wyre.ethereum.tasks.set_gas_price': {
    #     'task': 'wyre.ethereum.tasks.set_gas_price',
    #     'schedule': timedelta(minutes=7)
    # },
    'wyre.ethereum.tasks.submit_transactions': {
        'task': 'wyre.ethereum.tasks.submit_transactions',
        'schedule': timedelta(seconds=60)
    }
}

# Lex
LEX_WEBHOOK_PATH = os.environ.get('LEX_WEBHOOK_PATH')
LEX_WEBHOOK_SECRET_KEY = os.environ.get('LEX_WEBHOOK_SECRET_KEY')
LEX_SESSION_AGE = 600

# REST
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    )
}

# Ethereum
GETH_HOST = os.environ['GETH_SERVICE_HOST']
GETH_PORT = int(os.environ['GETH_SERVICE_PORT'])
ERC20_TOKEN_OWNER_ADDRESS = to_checksum_address(os.environ['ERC20_TOKEN_OWNER_ADDRESS'])
ERC20_TOKEN_AGENT_PRIVATE_KEY = os.environ['ERC20_TOKEN_AGENT_PRIVATE_KEY']
ERC20_TOKEN_AGENT_PASSPHRASE = os.environ['ERC20_TOKEN_AGENT_PASSPHRASE']
ERC20_TOKEN_ADDRESS = to_checksum_address(os.environ['ERC20_TOKEN_ADDRESS'])
ERC20_TOKEN_NUM_DECIMAL_PLACES = int(os.environ['ERC20_TOKEN_NUM_DECIMAL_PLACES'])
ERC20_TOKEN_ABI = json.loads(os.environ['ERC20_TOKEN_ABI'].replace('\\"', '"'))
ERC20_TOKEN_SYMBOL = os.environ['ERC20_TOKEN_SYMBOL']
ETHERSCAN_DOMAIN = os.environ['ETHERSCAN_DOMAIN']
DEFAULT_WEI_CHARGE = Money(1.00, 'USD')
USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR = Decimal(1.3)
MIN_ESTIMATED_NUM_TRANSACTIONS = 5

# Braintree
BRAINTREE_ENVIRONMENT = os.environ['BRAINTREE_ENVIRONMENT']
BRAINTREE_MERCHANT_ID = os.environ['BRAINTREE_MERCHANT_ID']  # Failure is a feature lol
BRAINTREE_PUBLIC_KEY = os.environ['BRAINTREE_PUBLIC_KEY']  # Failure is a feature lol
BRAINTREE_PRIVATE_KEY = os.environ['BRAINTREE_PRIVATE_KEY']  # Failure is a feature lol
BRAINTREE_TRANSACTION_RATE = 0.029
BRAINTREE_TRANSACTION_FEE = 0.30

# Coinbase
COINBASE_PUBLIC_KEY = os.environ['COINBASE_PUBLIC_KEY']
COINBASE_PRIVATE_KEY = os.environ['COINBASE_PRIVATE_KEY']

# Security
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Twitter Auth
TWITTER_ID = os.environ['TWITTER_ID']
TWITTER_SCREEN_NAME = os.environ['TWITTER_SCREEN_NAME']
TWITTER_PUBLIC_CONSUMER_KEY = os.environ['TWITTER_PUBLIC_CONSUMER_KEY']
TWITTER_PRIVATE_CONSUMER_KEY = os.environ['TWITTER_PRIVATE_CONSUMER_KEY']
TWITTER_PUBLIC_ACCESS_KEY = os.environ['TWITTER_PUBLIC_ACCESS_KEY']
TWITTER_PRIVATE_ACCESS_KEY = os.environ['TWITTER_PRIVATE_ACCESS_KEY']

# AWS X-Ray
XRAY_HOST = 'xray'
XRAY_PORT = int(os.environ['XRAY_SERVICE_PORT'])
XRAY_RECORDER = dict(
    AWS_XRAY_DAEMON_ADDRESS='{0}:{1}'.format(XRAY_HOST, XRAY_PORT),
    AWS_XRAY_TRACING_NAME='web.{0}'.format(SITE_DOMAIN),
    AWS_XRAY_CONTEXT_MISSING='LOG_WARNING',
    AUTO_INSTRUMENT=True,
    PLUGINS=('ec2_plugin',),
    SAMPLING=False
)

ELASTICSEARCH_HOST = os.environ.get('ELASTICSEARCH_HOST', 'aws_elasticsearch_proxy')
ELASTICSEARCH_PORT = int(os.environ.get('ELASTICSEARCH_SERVICE_PORT', 9200))

ADMIN_PATH = os.environ.get('ADMIN_PATH')
