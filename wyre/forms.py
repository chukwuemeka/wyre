from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import binascii
import imghdr
import uuid
from decimal import Decimal

import base64
from abc import abstractmethod
from dateutil.parser import parse as parse_date
from django import forms
from django.core import signing
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.validators import RegexValidator
from django.forms import ImageField, ValidationError, Textarea
from django.forms.fields import BaseTemporalField
from django.utils import six
from django.utils.translation import ugettext as _
from djmoney.money import Money


class IntentFormMixin(object):
    @abstractmethod
    def get_field_to_elicit(self):
        raise NotImplementedError

    def is_fulfilled(self):
        return self.is_valid()


class Base64ImageField(ImageField):
    widget = Textarea
    INVALID_FILE_MESSAGE = _("Please upload a valid file.")
    INVALID_TYPE_MESSAGE = _("The type of the file couldn't be determined.")
    EMPTY_VALUES = (None, '', [], (), {})
    ALLOWED_TYPES = (
        "jpeg",
        "jpg",
        "png",
        "gif"
    )

    def to_python(self, base64_data):
        # Check if this is a base64 string
        if base64_data in self.EMPTY_VALUES:
            return None

        if isinstance(base64_data, six.string_types):
            # Strip base64 header.
            if ';base64,' in base64_data:
                header, base64_data = base64_data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(base64_data)
            except (TypeError, binascii.Error):
                raise ValidationError(self.INVALID_FILE_MESSAGE)
            # Generate file name:
            file_name = str(uuid.uuid4().hex)
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)
            if file_extension not in self.ALLOWED_TYPES:
                raise ValidationError(self.INVALID_TYPE_MESSAGE)
            complete_file_name = file_name + "." + file_extension
            data = SimpleUploadedFile(name=complete_file_name, content=decoded_file)
            return super(Base64ImageField, self).to_python(data)
        raise ValidationError(_('This is not an base64 string'))

    def get_file_extension(self, filename, decoded_file):
        extension = imghdr.what(filename, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension
        return extension


class YesNoField(forms.TypedChoiceField):
    def __init__(self, *args, **kwargs):
        choices = [
            ('yes', True),
            ('no', False),
        ]

        def coerce(value):
            return value == 'yes'

        super(YesNoField, self).__init__(choices=choices, coerce=coerce, *args, **kwargs)

    def to_python(self, value):
        return super(YesNoField, self).to_python(value).lower()


class MoneyField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super().__init__(min_length=1, *args, **kwargs)
        self.validators.append(
            RegexValidator(regex='^\$?(([1-9]\d{0,2}(,\d{3})*)|(([1-9]\d*)?\d))(\.\d\d)?$')
        )

    def clean(self, value):
        value = super().clean(value)
        if not value:
            return value
        amount = None
        try:
            amount = Money(Decimal(value.lstrip('$').replace(',', '')), currency='USD')
        except ValueError:
            pass
        return amount


class DateField(BaseTemporalField):
    input_formats = ['']
    default_error_messages = {
        'invalid': _('Enter a valid date.'),
    }

    def strptime(self, value, format):
        return parse_date(value).date()


class ModelTokenField(forms.ModelChoiceField):
    widget = forms.HiddenInput

    def __init__(self, queryset, salt='django.core.signing', max_age=None, **kwargs):
        self.salt = salt
        self.max_age = max_age
        super(ModelTokenField, self).__init__(queryset=queryset, **kwargs)

    def decrypt_value(self, value):
        try:
            data = signing.loads(value, salt=self.salt, max_age=self.max_age)
        except signing.BadSignature:
            raise forms.ValidationError(self.error_messages['invalid_choice'], code='invalid_choice')
        return data.get('id')

    def to_python(self, value):
        if value in self.empty_values:
            return None
        value = self.decrypt_value(value)
        try:
            key = self.to_field_name or 'pk'
            value = self.queryset.get(**{key: value})
        except (ValueError, TypeError, self.queryset.model.DoesNotExist):
            raise forms.ValidationError(self.error_messages['invalid_choice'], code='invalid_choice')
        return value
