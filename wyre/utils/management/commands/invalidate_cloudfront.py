from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
from uuid import uuid4

import boto3
from django.conf import settings
from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        client = boto3.client('cloudfront')
        response = client.list_distributions()
        distribution = None
        for item in response.get('DistributionList').get('Items'):
            if item.get('DomainName') in settings.AWS_CLOUDFRONT_DOMAIN:
                distribution = item
                break
        response = client.create_invalidation(
            DistributionId=distribution.get('Id'),
            InvalidationBatch={
                'Paths': {
                    'Quantity': 1,
                    'Items': [
                        '/*'
                    ]
                },
                'CallerReference': uuid4().hex
            }
        )
        logger.info(response)
