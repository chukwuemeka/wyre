from __future__ import absolute_import

import logging

import boto3
from django.conf import settings
from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.debug('Starting the boto3 test!!!')
        client = boto3.client('s3')
        logger.debug(client.list_objects(Bucket=settings.AWS_STORAGE_BUCKET_NAME))
