from decimal import Decimal

import random
import re
from django.template import Context, Template


def get_lorem_text(count, method):
    context = Context()
    template_string = '{%' + ('lorem %d %s random' % (count, method)) + '%}'
    template = Template(template_string)
    return template.render(context)


def is_int(x):
    try:
        int(x)
        return True
    except TypeError:
        pass
    return False


def get_title(min_length, max_length):
    toks = get_lorem_text(max_length, 'w')[:random.randrange(min_length, max_length)].split()
    return ' '.join([x for x in toks]).title()


def to_money(value):
    from ..forms import MoneyField
    try:
        return MoneyField().clean(value)
    except Exception:
        pass
    return None


def to_int(value):
    if isinstance(value, int):
        return value
    try:
        return int(value)
    except Exception:
        pass
    return None


def to_decimal(value):
    if isinstance(value, Decimal):
        return value
    try:
        return Decimal(value)
    except Exception:
        pass
    return None


def format_decimal(value):
    if not re.search('^\d+\.\d*$', str(value)):
        return value
    return '{0}'.format(str(value).rstrip('0').rstrip('.'))


def pluralize(unit, amount):
    return '%s%s' % (unit, '' if Decimal(abs(amount - 1)) <= Decimal(1 / (10 ** 18)) else 's')
