module.exports = {
    npm: {
        globals: {
            $: 'jquery',
            Popper: 'popper.js'
        }
    },
    files: {
        javascripts: {
            joinTo: {
                'js/vendor.js': /^(?!app)/,
                'js/app.js': /^app\/js/
            }
        },
        stylesheets: {
            joinTo: 'css/app.css'
        }
    },
    paths: {
        public: '/dist'
    },
    plugins: {
        babel: {
            presets: ['env']
        },
        sass: {
            mode: 'native'
        },
        autoReload: {
            port: 9485
        }
    },
    watcher: {
        //usePolling: true,
        // interval: 750
    }

};