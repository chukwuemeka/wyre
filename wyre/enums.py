from enum import IntEnum


class Platform(IntEnum):
    TWITTER = 0
    MESSENGER = 1
