from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from importlib import import_module

import braintree
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

braintree.Configuration.configure(
    braintree.Environment.Sandbox if settings.BRAINTREE_ENVIRONMENT.lower() == 'sandbox' else braintree.Environment.Production,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY
)

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class BraintreeWebhookViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(BraintreeWebhookViewTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(BraintreeWebhookViewTestCase, cls).tearDownClass()

    def setUp(self):
        self.data = braintree.WebhookTesting.sample_notification(
            braintree.WebhookNotification.Kind.TransactionSettled,
            uuid.uuid4().hex
        )

    def tearDown(self):
        pass

    def test_get_not_allowed(self):
        url = reverse('braintree_webhook')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_webhook(self):
        data = self.data.copy()
        url = reverse('braintree_webhook')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content_type, 'application/json')
