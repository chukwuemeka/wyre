from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.views import APIView

from .tasks import process_webhook_notification

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
User = get_user_model()

logger = logging.getLogger(__name__)


class BraintreeWebhookView(APIView):
    http_method_names = ['post']

    def post(self, request, format=None):
        logger.info('Processing Braintree web hook\n{0}'.format(json.dumps(request.data, indent=2)))
        process_webhook_notification.apply_async((request.data.get('bt_signature'), request.data.get('bt_payload')))
        return Response({}, content_type='application/json')
