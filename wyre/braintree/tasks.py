from importlib import import_module

import braintree
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.auth import get_user_model

logger = get_task_logger(__name__)

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
Session = SessionStore().model

braintree.Configuration.configure(
    braintree.Environment.Sandbox if settings.BRAINTREE_ENVIRONMENT.lower() == 'sandbox' else braintree.Environment.Production,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY
)


@shared_task(queue='Wyre{0}BraintreeWebhookNotifications.fifo'.format(settings.ENVIRONMENT.capitalize()))
def process_webhook_notification(signature, payload):
    webhook_notification = braintree.WebhookNotification.parse(signature, payload)
    logger.info('Processing Braintree webhook notification\n{0}'.format(webhook_notification))
    pass
