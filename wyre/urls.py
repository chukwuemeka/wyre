import django
from admin_honeypot.urls import urlpatterns as admin_honeypot_url_patterns
from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.urls import re_path
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from wyre.braintree.api import BraintreeWebhookView
from wyre.ethereum.views import TokenCheckoutView, TokenCheckoutSuccessView
from wyre.ethereum.views import WeiCheckoutView, WeiCheckoutSuccessView
from wyre.lex.api import LexWebhookView
from .leads.views import LeadCreationSuccessView
from .leads.views import LeadCreationView

urlpatterns = []

# Static stuff
if settings.DEBUG:
    urlpatterns.append(
        re_path(r'^media/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT})
    )
    urlpatterns.append(
        re_path(r'^static/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.STATIC_ROOT})
    )

urlpatterns.extend([
    re_path(r'^{}'.format(settings.ADMIN_PATH), admin.site.urls),
    re_path(r'^admin/', include((admin_honeypot_url_patterns, 'admin_honeypot'), namespace='admin_honeypot')),
    re_path(r'^braintree/8A9BR2xmXEJgRZ98hIaLtl33zVFFtG3J/$', BraintreeWebhookView.as_view(), name='braintree_webhook'),
    re_path(r'^{0}$'.format(settings.LEX_WEBHOOK_PATH), LexWebhookView.as_view(), name='lex_webhook'),
    re_path(r'^tokens/(?P<token>[0-9a-zA-Z:\-_]+)/checkout/$', TokenCheckoutView.as_view(), name='token_checkout'),
    re_path(r'^tokens/(?P<token>[0-9a-zA-Z:\-_]+)/success/$', TokenCheckoutSuccessView.as_view(),
            name='token_checkout_success'),
    re_path(r'^ether/(?P<token>[0-9a-zA-Z:\-_]+)/checkout/$', WeiCheckoutView.as_view(), name='wei_checkout'),
    re_path(r'^ether/(?P<token>[0-9a-zA-Z:\-_]+)/success/$', WeiCheckoutSuccessView.as_view(),
            name='wei_checkout_success'),
    re_path(r'^terms/$', cache_page(60 * 60 * 24)(TemplateView.as_view(template_name='terms_of_service.html')),
            name='terms_of_service'),
    re_path(r'^privacy/$', cache_page(60 * 60 * 24)(TemplateView.as_view(template_name='privacy_policy.html')),
            name='privacy_policy'),
    re_path(r'^health/$', TemplateView.as_view(template_name='health_check.html'), name='health_check'),
    re_path(r'^success/$', LeadCreationSuccessView.as_view(), name='create_lead_success'),
    re_path(r'^$', LeadCreationView.as_view(), name='create_lead')
    # url(r'^$', TemplateView.as_view(template_name='index.html'), name='index')
])
