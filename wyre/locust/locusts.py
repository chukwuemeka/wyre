import json
import time
import uuid

import boto3
import gevent
from django.utils import timezone
from faker import Faker
from locust import HttpLocust, Locust
from locust import events
from locust.clients import HttpSession
from tenacity import retry, wait_fixed

from wyre.twitter.tasks import process_incoming_direct_message, process_incoming_status
from wyre.xray.utils import create_trace_id
from .task_sets import LeadTaskSet, TwitterTaskSet

TWITTER_LOCUST_SCREEN_NAME_IDENTIFIER = '!!locust!!'


class TwitterClient(object):
    def __init__(self):
        self.session = HttpSession(base_url='')
        faker = Faker()
        self._num_tokens = 0
        self._num_estimated_transactions = 0
        self._user = {
            'id': uuid.uuid4().hex,
            'screen_name': '{0}{1}'.format(TWITTER_LOCUST_SCREEN_NAME_IDENTIFIER, faker.user_name()),
            'email_address': 'success@simulator.amazonses.com'
        }

    @property
    def user(self):
        return self._user

    @property
    def num_estimated_transactions(self):
        return self._num_estimated_transactions

    @num_estimated_transactions.setter
    def num_estimated_transactions(self, x):
        self._num_estimated_transactions = x

    @property
    def num_tokens(self):
        return self._num_tokens

    @num_tokens.setter
    def num_tokens(self, x):
        self._num_tokens = x

    def get(self, *args, **kwargs):
        return self.session.get(*args, **kwargs)

    def post(self, *args, **kwargs):
        return self.session.post(*args, **kwargs)

    def status(self, transcript, request_name, is_success):
        trace_id = create_trace_id()
        status_id = uuid.uuid4().hex
        status = {
            'user': {
                'id_str': self._user.get('id'),
                'screen_name': self._user.get('screen_name')
            },
            'id': status_id,
            'id_str': status_id,
            'text': transcript
        }
        process_incoming_status.apply_async(
            args=(status,),
            kwargs=dict(trace_id=trace_id, do_mock_reply=True),
            MessageGroupId=status.get('user').get('id_str'),
            MessageDeduplicationId=uuid.uuid4().hex
        )
        return self.process_trace(trace_id, request_name, 'STATUS', is_success)

    def direct_message(self, transcript, request_name, is_success):
        trace_id = create_trace_id()
        message_id = uuid.uuid4().hex
        direct_message = {
            'id': message_id,
            'id_str': message_id,
            'sender_id_str': self._user.get('id'),
            'sender_screen_name': self._user.get('screen_name'),
            'text': transcript
        }
        process_incoming_direct_message.apply_async(
            args=(direct_message,),
            kwargs=dict(trace_id=trace_id, do_mock_reply=True),
            MessageGroupId=direct_message.get('sender_id_str'),
            MessageDeduplicationId=uuid.uuid4().hex
        )
        return self.process_trace(trace_id, request_name, 'DIRECT_MESSAGE', is_success)

    def process_trace(self, trace_id, request_name, request_type, is_success):
        start_time = timezone.now()
        trace = simple_database_client.get_trace(trace_id)
        end_time = timezone.now()
        if trace:
            response_time = (end_time - start_time).total_seconds() * 1000
            is_response_successful = is_success(trace)
            if is_response_successful:
                events.request_success.fire(
                    request_type=request_type,
                    name=request_name,
                    response_time=response_time,
                    response_length=0
                )
                return trace
            else:
                events.request_failure.fire(
                    request_type=request_type,
                    name=request_name,
                    response_time=response_time,
                    exception=Exception('Incorrect response ({0})({1})'.format(
                        trace_id,
                        trace.get('messages')
                    ))
                )
        else:
            events.request_failure.fire(
                request_type=request_type,
                name=request_name,
                exception=Exception('Failed to fetch trace {0}'.format(trace_id)),
                response_time=(end_time - start_time).total_seconds() * 1000,
            )
        return None


class SimpleDatabaseClient(object):
    traces = {}
    domain_name = 'traces.staging.baetoken.com'

    def __init__(self):
        self.client = boto3.client('sdb')
        self.client.delete_domain(
            DomainName=self.domain_name
        )
        self.client.create_domain(
            DomainName=self.domain_name
        )

    @retry(wait=wait_fixed(0.1))
    def get_trace(self, trace_id):
        trace = self.traces.get(trace_id)
        assert trace
        return trace

    def remove_trace_id(self, trace_id):
        self.client.delete_attributes(
            DomainName=self.domain_name,
            ItemName=trace_id
        )
        self.traces.pop(trace_id)

    def run(self):
        while True:
            next_token = None
            is_first_request = True
            while is_first_request or next_token:
                kwargs = dict(
                    SelectExpression='select * from `{0}` limit 2500'.format(self.domain_name),
                    ConsistentRead=True
                )
                if next_token:
                    kwargs['NextToken'] = next_token

                response = self.client.select(**kwargs)
                # logger.info('response = {0}'.format(response))
                next_token = response.get('NextToken')
                items = response.get('Items', [])
                for item in items:
                    self.traces[item.get('Name')] = json.loads(item.get('Attributes')[0].get('Value'))
                is_first_request = False
                # logger.info('traces = {0}'.format(json.dumps(self.traces, indent=2)))
            time.sleep(0.1)


simple_database_client = SimpleDatabaseClient()
gevent.spawn(simple_database_client.run)


class TwitterLocust(Locust):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.client = TwitterClient()

    task_set = TwitterTaskSet
    min_wait = 1000
    max_wait = 5000


class WebLocust(HttpLocust):
    task_set = LeadTaskSet
    min_wait = 5000
    max_wait = 10000
