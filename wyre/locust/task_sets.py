import logging
import uuid

import re
import requests
from deepdiff import DeepSearch
from django.urls import reverse
from locust import TaskSet, task

logger = logging.getLogger(__name__)
TWITTER_LOCUST_SCREEN_NAME_IDENTIFIER = '!!locust!!'


class TwitterTaskSet(TaskSet):
    @task(0)
    class StatusTaskSet(TaskSet):
        @task(1)
        def stop(self):
            self.interrupt()

        @task(6)
        def hello(self):
            self.client.status(
                'Hello',
                request_name='hello',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hello', verbose_level=2).get('matched_values'))
            )

        @task(3)
        def how_it_works(self):
            self.client.status(
                'How does BAE token work?',
                request_name='how_it_works',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Check out this video', verbose_level=2).get('matched_values'))
            )

        @task(3)
        def help(self):
            self.client.status(
                'Help',
                request_name='help',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Try saying', verbose_level=2).get('matched_values'))
            )

        @task(1)
        def cancel(self):
            self.client.status(
                'Cancel',
                request_name='cancel',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'I will stop', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def get_wei_balance(self):
            self.client.status(
                'How much gas do I have?',
                request_name='get_wei_balance',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hey there', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def get_token_balance(self):
            self.client.status(
                'How many BAE tokens do I have?',
                request_name='get_token_balance',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hey there', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def purchase_wei(self):
            self.client.status(
                'I want to buy gas',
                request_name='purchase_wei',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hey there', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def purchase_tokens(self):
            self.client.status(
                'I want to buy 1 token',
                request_name='purchase_wei',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hey there', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def withdraw_tokens(self):
            self.client.status(
                'I want to withdraw my tokens',
                request_name='withdraw_tokens',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hey there', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def transfer_tokens(self):
            if self.client.num_tokens <= 0 or self.client.num_estimated_transactions <= 0:
                return
            trace = self.client.status(
                'Send 1 bae token to @stagebot',
                request_name='transfer_tokens',
                is_success=lambda trace: bool(DeepSearch(trace, 'Done', verbose_level=2).get('matched_values'))
            )
            if trace:
                self.client.num_tokens = 0
                self.client.num_estimated_transactions = 0

    @task(5)
    class DirectMessageTaskSet(TaskSet):
        @task(1)
        def stop(self):
            self.interrupt()

        @task(6)
        def hello(self):
            self.client.direct_message(
                'Hello',
                request_name='hello',
                is_success=lambda trace: bool(DeepSearch(trace, 'Hello', verbose_level=2).get('matched_values'))
            )

        @task(3)
        def how_it_works(self):
            self.client.direct_message(
                'How does BAE token work?',
                request_name='how_it_works',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Check out this video', verbose_level=2).get('matched_values'))
            )

        @task(3)
        def help(self):
            self.client.direct_message(
                'Help',
                request_name='help',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Try saying', verbose_level=2).get('matched_values'))
            )

        @task(1)
        def cancel(self):
            self.client.direct_message(
                'Cancel',
                request_name='cancel',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Okay okay', verbose_level=2).get('matched_values'))
            )

        @task(6)
        def get_wei_balance(self):
            trace = self.client.direct_message(
                'How much gas do I have?',
                request_name='get_wei_balance',
                is_success=lambda t: bool(DeepSearch(t, 'ETH', verbose_level=2).get('matched_values'))
            )
            assert trace
            match = re.search('(\d+) transactions', trace.get('messages', [{}, {}])[1].get('text', ''))
            if match:
                self.client.num_estimated_transactions = int(match.group(1))

        @task(6)
        def get_token_balance(self):
            trace = self.client.direct_message(
                'How many BAE tokens do I have?',
                request_name='get_token_balance',
                is_success=lambda trace: bool(DeepSearch(trace, 'tokens', verbose_level=2).get('matched_values'))
            )
            assert trace
            match = re.search('(\d+) BAE tokens', trace.get('messages', [{}])[0].get('text', ''))
            assert match
            self.client.num_tokens = int(match.group(1))

        @task(6)
        def withdraw_tokens(self):
            if self.client.num_tokens <= 0 or self.client.num_estimated_transactions <= 0:
                return
            traces = []
            trace = self.client.direct_message(
                'I want to withdraw my tokens',
                request_name='token_withdrawal/0',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'would you like to withdraw', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            trace = self.client.direct_message(
                '1',
                request_name='token_withdrawal/1',
                is_success=lambda trace: bool(DeepSearch(trace, 'Which address', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            trace = self.client.direct_message(
                '0x7de16d3300f0b2fe48e20a0942089dbbb5c30c06',
                request_name='token_withdrawal/2',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Just to confirm', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            trace = self.client.direct_message(
                'Yes',
                request_name='token_withdrawal/3',
                is_success=lambda trace: bool(DeepSearch(trace, 'Done!', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)
            if trace:
                self.client.num_tokens = 0
                self.client.num_estimated_transactions = 0

        @task(6)
        def transfer_tokens(self):
            if self.client.num_tokens <= 0 or self.client.num_estimated_transactions <= 0:
                return
            trace = self.client.direct_message(
                'Send 1 bae token to @stagebot',
                request_name='token_transfer',
                is_success=lambda trace: bool(DeepSearch(trace, 'Done', verbose_level=2).get('matched_values'))
            )
            if trace:
                self.client.num_tokens = 0
                self.client.num_estimated_transactions = 0

        @task(1)
        def purchase_wei(self):
            traces = []
            trace = self.client.direct_message(
                'I want to buy gas',
                request_name='purchase_wei/0',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'How much would you like to spend on gas', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            trace = self.client.direct_message(
                '$5.00',
                request_name='purchase_wei/1',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'send your receipt to', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            if DeepSearch(traces, 'I need to know which email address I should send your receipt to',
                          verbose_level=2).get('matched_values'):
                trace = self.client.direct_message(
                    self.client.user.get('email_address'),
                    request_name='purchase_wei/2',
                    is_success=lambda trace: bool(
                        DeepSearch(trace, 'Just to confirm', verbose_level=2).get('matched_values'))
                )
                assert trace
                traces.append(trace)
            else:
                trace = self.client.direct_message(
                    'Yes',
                    request_name='purchase_wei/2',
                    is_success=lambda trace: bool(
                        DeepSearch(trace, 'Just to confirm', verbose_level=2).get('matched_values'))
                )
                assert trace
                traces.append(trace)

            trace = self.client.direct_message(
                'Yes',
                request_name='purchase_wei/3',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Please tap the button below', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            url = trace.get('messages', [{}])[0].get('buttons')[0].get('url')
            assert url
            assert url.startswith('https://')
            response = self.client.get(url, name='/ether/<token>/checkout/')
            assert response.status_code == requests.codes.ok
            match = re.search('<input type=\'hidden\' name=\'csrfmiddlewaretoken\' value=\'(\w{5,})\' />',
                              response.text)
            assert match
            csrf_token = match.group(1)
            assert csrf_token

            payload = dict(
                csrfmiddlewaretoken=csrf_token,
                nonce='fake-valid-nonce'
            )
            headers = dict(
                Referer=url
            )
            with self.client.post(url, name='/ether/<token>/checkout/', headers=headers, data=payload,
                                  catch_response=True) as response:
                if 'We have sent you your receipt' not in response.text:
                    response.failure('Failed to submit form')

        @task(6)
        def purchase_tokens(self):
            traces = []
            trace = self.client.direct_message(
                'I want to buy 1 token',
                request_name='purchase_tokens/0',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Your subtotal for', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            if DeepSearch(traces, 'I need to know which email address I should send your receipt to',
                          verbose_level=2).get('matched_values'):
                trace = self.client.direct_message(
                    self.client.user.get('email_address'),
                    request_name='purchase_tokens/1',
                    is_success=lambda trace: bool(
                        DeepSearch(trace, 'Just to confirm', verbose_level=2).get('matched_values'))
                )
                assert trace
                traces.append(trace)
            else:
                trace = self.client.direct_message(
                    'Yes',
                    request_name='purchase_tokens/1',
                    is_success=lambda trace: bool(
                        DeepSearch(trace, 'Just to confirm', verbose_level=2).get('matched_values'))
                )
                assert trace
                traces.append(trace)

            trace = self.client.direct_message(
                'Yes',
                request_name='purchase_tokens/2',
                is_success=lambda trace: bool(
                    DeepSearch(trace, 'Please tap the button below', verbose_level=2).get('matched_values'))
            )
            assert trace
            traces.append(trace)

            url = trace.get('messages', [{}])[0].get('buttons')[0].get('url')
            assert url
            assert url.startswith('https://')
            response = self.client.get(url, name='/tokens/<token>/checkout/')
            assert response.status_code == requests.codes.ok
            match = re.search(
                '<input type=\'hidden\' name=\'csrfmiddlewaretoken\' value=\'(\w{5,})\' />',
                response.text
            )
            assert match
            csrf_token = match.group(1)
            assert csrf_token

            payload = dict(
                csrfmiddlewaretoken=csrf_token,
                nonce='fake-valid-nonce'
            )
            headers = dict(
                Referer=url
            )
            with self.client.post(url, name='/tokens/<token>/checkout/', headers=headers, data=payload,
                                  catch_response=True) as response:
                if 'We have sent you your receipt' not in response.text:
                    response.failure('Failed to submit form')


class LeadTaskSet(TaskSet):
    @task(10)
    def index(self):
        self.client.get(reverse('create_lead'))

    @task(1)
    def privacy_policy(self):
        self.client.get(reverse('privacy_policy'))

    @task(1)
    def terms_of_service(self):
        self.client.get(reverse('terms_of_service'))

    @task(2)
    def create_lead(self):
        payload = dict(
            email_address='success+{0}@simulator.amazonses.com'.format(uuid.uuid4().hex)
        )
        with self.client.post(reverse('create_lead'), data=payload, catch_response=True) as response:
            if 'One of our team members will reach out to you shortly.' not in response.text:
                response.failure('Failed to submit form')
