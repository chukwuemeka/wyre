from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.urls import reverse
from django_webtest import WebTest
from rest_framework import status


# class IndexViewTestCase(WebTest):
#     def test_page_exists(self):
#         url = reverse('index')
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertTemplateUsed(response, 'index.html')


class TermsOfServiceViewTestCase(WebTest):
    def test_page_exists(self):
        url = reverse('terms_of_service')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertTemplateUsed(response, 'terms_of_service.html')

    def test_page_is_cached(self):
        url = reverse('terms_of_service')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['cache-control'], 'max-age=86400')


class PrivacyPolicyViewTestCase(WebTest):
    def test_page_exists(self):
        url = reverse('privacy_policy')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertTemplateUsed(response, 'privacy_policy.html')

    def test_page_is_cached(self):
        url = reverse('privacy_policy')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['cache-control'], 'max-age=86400')
