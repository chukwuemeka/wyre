# Create your views here.
import calendar
from datetime import datetime

import arrow
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import CreateView

from .forms import LeadForm
from .models import Lead


@method_decorator(csrf_exempt, name='dispatch')
class LeadCreationView(CreateView):
    model = Lead
    form_class = LeadForm
    template_name = 'leads/lead_creation_form.html'
    success_url = reverse_lazy('create_lead_success')

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        now_time = timezone.now()
        month_range = calendar.monthrange(now_time.year, now_time.month)
        if len(month_range) >= 2:
            context_data['deadline'] = arrow.Arrow.fromdate(datetime(year=now_time.year, month=now_time.month, day=month_range[1])).format('Do of MMMM YYYY')
        return context_data

    def form_valid(self, form):
        response = super().form_valid(form)
        lead = self.object

        # Send mail
        context = {
            'lead': lead
        }
        subject = ''.join(loader.render_to_string('leads/emails/lead_creation_subject.txt', context, request=self.request).splitlines())
        body = loader.render_to_string('leads/emails/lead_creation_body.txt', context, request=self.request)
        html_body = loader.render_to_string('leads/emails/lead_creation_body.html', context, request=self.request)
        email_message = EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[lead.email_address],
            bcc=[settings.SERVER_EMAIL, settings.SUPPORT_EMAIL]
        )
        email_message.attach_alternative(html_body, 'text/html')
        email_message.send()

        return response


class LeadCreationSuccessView(LeadCreationView):
    def get_context_data(self, **kwargs):
        context_data = super(LeadCreationSuccessView, self).get_context_data(**kwargs)
        context_data['is_success'] = True
        return context_data
