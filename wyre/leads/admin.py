import csv

import io
from django.contrib import admin
from django.http import HttpResponse

from .models import Lead


# Register your models here.
@admin.register(Lead)
class LeadAdmin(admin.ModelAdmin):
    list_display = ('email_address', 'date_created')
    ordering = ('-date_created',)
    actions = ['export_csv_file']

    def export_csv_file(self, request, queryset):
        content = io.StringIO()
        csv_writer = csv.DictWriter(content, fieldnames=['email_address'])
        csv_writer.writeheader()
        for lead in queryset:
            csv_writer.writerow({'email_address': lead.email_address})
        content.seek(0)
        response = HttpResponse(content, content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="leads.csv"'
        return response

    export_csv_file.short_description = 'Export selected leads to CSV file'
