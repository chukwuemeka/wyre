from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import url

from .views import LeadCreationSuccessView
from .views import LeadCreationView

urlpatterns = [
    url(r'^success/$', LeadCreationSuccessView.as_view(), name='create_lead_success'),
    url(r'^$', LeadCreationView.as_view(), name='create_lead'),
]
