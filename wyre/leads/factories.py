from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory

from .models import Lead


class LeadFactory(factory.django.DjangoModelFactory):
    email_address = factory.Faker('email')

    class Meta:
        model = Lead
