from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import csv

import io
from django.contrib.admin.sites import AdminSite
from django.contrib.auth import get_user_model
from django.test import TestCase

from ..admin import LeadAdmin
from ..factories import LeadFactory
from ..models import Lead

User = get_user_model()


class LeadAdminTestCase(TestCase):
    def setUp(self):
        self.lead_admin = LeadAdmin(model=Lead, admin_site=AdminSite())
        [LeadFactory() for i in range(100)]

    def tearDown(self):
        Lead.objects.all().delete()

    def test_export_csv_file(self):
        response = self.lead_admin.export_csv_file(None, Lead.objects.all())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'text/csv')
        self.assertEqual(response['Content-Disposition'], 'attachment; filename="leads.csv"')
        csv_reader = csv.DictReader(io.StringIO(response.content.decode('utf-8')), fieldnames=['email_address'])
        num_rows = -1
        for row in csv_reader:
            num_rows += 1
            if num_rows <= 0:
                continue
            self.assertTrue(Lead.objects.filter(email_address=row.get('email_address')).exists())
        self.assertEqual(num_rows, Lead.objects.all().count())
