from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from ..factories import LeadFactory
from ..forms import LeadForm
from ..models import Lead


class LeadFormTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com'
        }

    def tearDown(self):
        Lead.objects.all().delete()

    def test_create_lead(self):
        data = self.data.copy()
        form = LeadForm(data=data)
        self.assertTrue(form.is_valid())
        lead = form.save()
        self.assertIsNotNone(lead)
        self.assertEqual(Lead.objects.all().count(), 1)

    def test_existing_email_address(self):
        data = self.data.copy()
        LeadFactory(email_address=data.get('email_address'))
        form = LeadForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('email_address' in form.errors.as_data())
        self.assertEqual(Lead.objects.all().count(), 1)
