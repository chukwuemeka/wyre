from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.core import mail
from django.urls import reverse
from django_webtest import WebTest
from rest_framework import status

from ..models import Lead


class LeadCreationViewTestCase(WebTest):
    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com',
        }

    def tearDown(self):
        Lead.objects.all().delete()

    def test_page(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.forms)
        self.assertEqual(len(response.forms), 4)

    def test_create_lead(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = self.data.copy()
        form = response.forms[0]
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()

        self.assertRedirects(response, reverse('create_lead_success'))
        self.assertEqual(Lead.objects.all().count(), 1)

    def test_cache_headers(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=86400', response.headers['Cache-Control'])

    def test_email(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = self.data.copy()
        form = response.forms[0]
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertEqual(mail.outbox[0].to, [data.get('email_address')])
        self.assertEqual(mail.outbox[0].bcc, [settings.SERVER_EMAIL, settings.SUPPORT_EMAIL])

    def get_url(self):
        return reverse('create_lead')
