import uuid

from django.core.cache import caches
from django.core.mail.backends.base import BaseEmailBackend

from .tasks import send_email_message


class CeleryEmailBackend(BaseEmailBackend):
    def send_messages(self, email_messages):
        num_sent = 0
        for email_message in email_messages:
            key = uuid.uuid4().hex
            caches['email'].set(key, email_message, 86400)
            send_email_message.apply_async(
                args=(key,),
            )
            num_sent += 1
        return num_sent
