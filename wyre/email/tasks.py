from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.cache import caches
from django.core.mail import get_connection

logger = get_task_logger(__name__)


@shared_task(
    queue='Wyre{0}Emails'.format(settings.ENVIRONMENT.capitalize()),
    rate_limit=10
)
def send_email_message(key):
    try:
        email_message = caches['email'].get(key)
        backend = get_connection(backend=settings.WRAPPED_EMAIL_BACKEND)
        backend.send_messages([email_message])
    except Exception as e:
        print(e)
