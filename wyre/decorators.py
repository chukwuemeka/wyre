import logging
import pickle

import base64
import hashlib
import re
from django.conf import settings
from django.core.cache import caches
from functools import update_wrapper, wraps
from redlock import Redlock


def method_decorator(decorator, name=''):
    """
    Convert a function decorator into a method decorator
    """
    # 'obj' can be a class or a function. If 'obj' is a function at the time it
    # is passed to _dec,  it will eventually be a method of the class it is
    # defined on. If 'obj' is a class, the 'name' is required to be the name
    # of the method that will be decorated.
    def _dec(obj):
        is_class = isinstance(obj, type)
        if is_class:
            if name and hasattr(obj, name):
                func = getattr(obj, name)
                if not callable(func):
                    raise TypeError(
                        "Cannot decorate '{0}' as it isn't a callable "
                        "attribute of {1} ({2})".format(name, obj, func)
                    )
            else:
                raise ValueError(
                    "The keyword argument `name` must be the name of a method "
                    "of the decorated class: {0}. Got '{1}' instead".format(
                        obj, name,
                    )
                )
        else:
            func = obj

        def decorate(function):
            """
            Apply a list/tuple of decorators if decorator is one. Decorator
            functions are applied so that the call order is the same as the
            order in which they appear in the iterable.
            """
            if isinstance(decorator, list):
                for dec in decorator[::-1]:
                    function = dec(function)
                return function
            return decorator(function)

        def _wrapper(self, *args, **kwargs):
            @decorate
            def bound_func(*args2, **kwargs2):
                return func.__get__(self, type(self))(*args2, **kwargs2)
            # bound_func has the signature that 'decorator' expects i.e.  no
            # 'self' argument, but it is a closure over self so it can call
            # 'func' correctly.
            return bound_func(*args, **kwargs)
        # In case 'decorator' adds attributes to the function it decorates, we
        # want to copy those. We don't have access to bound_func in this scope,
        # but we can cheat by using it on a dummy function.

        @decorate
        def dummy(*args, **kwargs):
            pass
        update_wrapper(_wrapper, dummy)
        # Need to preserve any existing attributes of 'func', including the name.
        update_wrapper(_wrapper, func)

        if is_class:
            setattr(obj, name, _wrapper)
            return obj

        return _wrapper
    # Don't worry about making _dec look similar to a list/tuple as it's rather
    # meaningless.
    if not hasattr(decorator, '__iter__'):
        update_wrapper(_dec, decorator)
    # Change the name to aid debugging.
    if hasattr(decorator, '__name__'):
        _dec.__name__ = 'method_decorator(%s)' % decorator.__name__
    else:
        _dec.__name__ = 'method_decorator(%s)' % decorator.__class__.__name__
    return _dec


def log_exception(logger):
    def _decorator(method):
        @wraps(method)
        def wrapper(*args, **kwargs):
            try:
                method(*args, **kwargs)
            except Exception:
                logger.exception('')
                raise

        return wrapper

    return _decorator


def memoize(prefix, cache_alias='default', timeout=86400):
    logger = logging.getLogger(__name__)

    def _decorator(method):
        @wraps(method)
        def wrapper(*args, **kwargs):
            do_reset_cache = kwargs.get('do_reset_cache', False)
            args_hash = base64.b64encode(pickle.dumps(args)).decode()
            kwargs_hash = base64.b64encode(pickle.dumps(kwargs)).decode()
            key = hashlib.sha256(
                '{0}{1}{2}{3}'.format(
                    prefix,
                    method.__name__,
                    args_hash,
                    kwargs_hash
                ).encode()
            ).hexdigest()
            value = caches[cache_alias].get(key)
            if value and not do_reset_cache:
                logger.debug('Hit from cache!')
                return value
            else:
                value = method(*args, **kwargs)
                caches[cache_alias].set(key, value, timeout)
                return value

        return wrapper

    return _decorator


def lock(resource, validity_timeout=5, retry_count=3, retry_delay=0.2, cache_alias='redlock'):
    prefixed_resource = '{}#{}#{}'.format(
        settings.CACHE_PREFIX if hasattr(settings, 'CACHE_PREFIX') else '',
        settings.CACHE_VERSION if hasattr(settings, 'CACHE_VERSION') else '',
        resource
    )
    match = re.search(r'(\d+$)', settings.CACHES.get(cache_alias).get('LOCATION'))
    db = int(match.groups()[0])

    def _decorator(method):
        @wraps(method)
        def wrapper(*args, **kwargs):
            if not settings.CACHE_BACKEND == 'django_redis.cache.RedisCache':
                return method(*args, **kwargs)
            distributed_lock_manager = Redlock(
                [{"host": settings.REDIS_HOST, "port": settings.REDIS_PORT, "db": db}],
                retry_count=retry_count,
                retry_delay=retry_delay
            )
            _lock = distributed_lock_manager.lock(prefixed_resource, validity_timeout * 1000)
            assert _lock, 'Failed to get a lock for {}'.format(prefixed_resource)
            value = method(*args, **kwargs)
            distributed_lock_manager.unlock(_lock)
            return value
        return wrapper

    return _decorator
