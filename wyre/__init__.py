from __future__ import absolute_import, unicode_literals

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app

__all__ = ['celery_app']

import os
import ast

from aws_xray_sdk.core import patch

libraries = ('botocore', 'requests')

if ast.literal_eval(os.environ.get('DO_APPLY_AWS_XRAY_PATCHES', 'True').capitalize()):
    patch(libraries)
