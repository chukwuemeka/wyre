import json
import logging

import requests
from aws_xray_sdk.core import xray_recorder
from requests_oauthlib import OAuth1
from twitter.twitter_utils import calc_expected_status_length

from wyre.twitter.utils import get_media_id
from .utils import get_chunks
from ..decorators import method_decorator
from ..twitter.utils import Api as TwitterApi

MAXIMUM_STATUS_LENGTH = 140
logger = logging.getLogger(__name__)


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'UrlButton.to_twitter')), name='to_twitter')
class UrlButton(object):
    def __init__(self, *, label, url=None):
        self.label = label
        self.url = url

    def to_json(self):
        return {
            'label': self.label,
            'url': self.url
        }

    def to_twitter(self):
        return {
            'type': 'web_url',
            'label': self.label,
            'url': self.url
        }


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Attachment.to_twitter')), name='to_twitter')
class Attachment(object):
    def __init__(self, *, category, url):
        self.url = url

    def to_json(self):
        return {
            'type': 'attachment',
            'url': self.url
        }

    def to_twitter(self, *, recipient_id, public_consumer_key, private_consumer_key, public_access_key,
                   private_access_key):
        media_id = get_media_id(self.url, public_consumer_key, private_consumer_key, public_access_key,
                                private_access_key)
        logger.info('Attaching media {0} to direct message'.format(media_id))
        auth = OAuth1(public_consumer_key, private_consumer_key, public_access_key, private_access_key)
        response = requests.post('https://api.twitter.com/1.1/direct_messages/events/new.json', auth=auth, json={
            'event': {
                'type': 'message_create',
                'message_create': {
                    'target': {
                        'recipient_id': recipient_id
                    },
                    'message_data': {
                        'attachment': {
                            'type': 'media',
                            'media': {
                                'id': media_id
                            }
                        }
                    }
                }
            }
        })
        xray_recorder.current_subsegment().put_metadata('attachment', {
            'url': self.url,
            'media_id': media_id,
            'response': {
                'status_code': response.status_code,
                'json': response.json()
            }
        })
        logger.info('response.status_code = {0}'.format(response.status_code))
        logger.info('response = {0}'.format(json.dumps(response.json(), indent=2)))


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Text.to_twitter')), name='to_twitter')
class Text(object):
    def __init__(self, text, buttons=None):
        self.text = text
        self.buttons = buttons

    def to_json(self):
        ret = {
            'type': 'text',
            'text': self.text,
        }
        if self.buttons:
            ret['buttons'] = [b.to_json() for b in self.buttons]
        return ret

    def to_twitter(self, *, recipient_id, public_consumer_key, private_consumer_key, public_access_key,
                   private_access_key):
        auth = OAuth1(public_consumer_key, private_consumer_key, public_access_key, private_access_key)
        payload = {
            'event': {
                'type': 'message_create',
                'message_create': {
                    'target': {
                        'recipient_id': recipient_id
                    },
                    'message_data': {
                        'text': self.text,
                    }
                }
            }
        }
        if self.buttons:
            payload['event']['message_create']['message_data']['ctas'] = [button.to_twitter() for button in self.buttons]

        response = requests.post('https://api.twitter.com/1.1/direct_messages/events/new.json', auth=auth, json=payload)
        xray_recorder.current_subsegment().put_metadata('text', {
            'text': self.text,
            'response': {
                'status_code': response.status_code,
                'json': response.json()
            }
        })
        logger.info('response.status_code = {0}'.format(response.status_code))
        logger.info('response = {0}'.format(json.dumps(response.json(), indent=2)))


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Status.post')), name='post')
class Status(object):
    def __init__(self, *, status, media_url=None):
        self.status = status
        self.media_url = media_url

    def to_json(self):
        return {
            'type': 'status',
            'media_url': self.media_url,
            'status': self.status
        }

    def post(self, *, public_consumer_key, private_consumer_key, public_access_key, private_access_key,
             in_reply_to_status_id=None):
        media_id = None
        if self.media_url:
            media_id = get_media_id(self.media_url, public_consumer_key, private_consumer_key, public_access_key,
                                    private_access_key, is_direct_message_attachment=False)
        api = TwitterApi(
            consumer_key=public_consumer_key,
            consumer_secret=private_consumer_key,
            access_token_key=public_access_key,
            access_token_secret=private_access_key
        )
        status_length = MAXIMUM_STATUS_LENGTH - 6
        statuses_metadata = []
        if calc_expected_status_length(self.status) > status_length:
            statuses = get_chunks(self.status, status_length)
            last_in_reply_to_status_id = in_reply_to_status_id
            for i, s in enumerate(statuses, 1):
                kwargs = dict(
                    status='{0} ({1}/{2})'.format(s, i, len(statuses)),
                    media=media_id if i == 1 else None,
                    in_reply_to_status_id=last_in_reply_to_status_id,
                    auto_populate_reply_metadata=bool(last_in_reply_to_status_id)
                )
                statuses_metadata.append(kwargs)
                response = api.PostUpdate(**kwargs)
                last_in_reply_to_status_id = response.AsDict().get('id_str')
                logger.info('response = {0}'.format(response))
        else:
            kwargs = dict(
                status=self.status,
                media=media_id,
                in_reply_to_status_id=in_reply_to_status_id,
                auto_populate_reply_metadata=bool(in_reply_to_status_id)
            )
            statuses_metadata.append(kwargs)
            response = api.PostUpdate(**kwargs)
            logger.info('response = {0}'.format(response))

        xray_recorder.current_subsegment().put_metadata('statuses', statuses_metadata)


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'QuickReply.to_twitter')), name='to_twitter')
class QuickReply(object):
    def __init__(self, *, text, options):
        self.text = text
        self.options = options

    def to_json(self):
        return {
            'type': 'quick_reply',
            'text': self.text,
            'options': self.options
        }

    def to_twitter(self, *, recipient_id, public_consumer_key, private_consumer_key, public_access_key,
                   private_access_key):
        # Build options
        options = list()
        for o in self.options:
            assert 'title' in o
            options.append({
                'label': o.get('title'),
                'description': o.get('description'),
                'metadata': o.get('payload'),
            })

        # POST
        auth = OAuth1(public_consumer_key, private_consumer_key, public_access_key, private_access_key)
        response = requests.post('https://api.twitter.com/1.1/direct_messages/events/new.json', auth=auth, json={
            'event': {
                'type': 'message_create',
                'message_create': {
                    'target': {
                        'recipient_id': recipient_id
                    },
                    'message_data': {
                        'text': self.text,
                        'quick_reply': {
                            'type': 'options',
                            'options': options
                        }
                    }
                }
            }
        })
        xray_recorder.current_subsegment().put_metadata('quick_reply', {
            'text': self.text,
            'options': self.options,
            'response': {
                'status_code': response.status_code,
                'json': response.json()
            }
        })
        logger.info('response.status_code = {0}'.format(response.status_code))
        if response.status_code != requests.codes.ok:
            try:
                raise Exception('Failed to send quick reply')
            except Exception:
                logger.exception('Failed to send quick reply')
        logger.info('response = {0}'.format(json.dumps(response.json(), indent=2)))
