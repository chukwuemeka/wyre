from django.test import TestCase

from ..utils import get_chunks


class GetChunksTestCase(TestCase):
    def test_get_chunks(self):
        text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a nibh lacus. Nulla mi nisl, vestibulum ' \
               'eget dui in, interdum dignissim diam. Integer ipsum lectus, imperdiet eu pretium a, ' \
               'vestibulum nec est. Morbi sit amet tristique neque, a bibendum ligula. Nullam auctor id dui eu ' \
               'ullamcorper. Vestibulum sed urna egestas, fermentum lorem vitae, hendrerit nulla. Aenean et lacus ' \
               'sit amet mauris ullamcorper efficitur. Interdum et malesuada fames ac ante ipsum primis in faucibus. ' \
               'Suspendisse feugiat nulla bibendum orci porttitor lacinia.'
        chunks = get_chunks(text, 137)
        self.assertEqual(chunks, [
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a nibh lacus. Nulla mi nisl, vestibulum eget dui in, interdum dignissim',
            'diam. Integer ipsum lectus, imperdiet eu pretium a, vestibulum nec est. Morbi sit amet tristique neque, a bibendum ligula. Nullam auctor',
            'id dui eu ullamcorper. Vestibulum sed urna egestas, fermentum lorem vitae, hendrerit nulla. Aenean et lacus sit amet mauris ullamcorper',
            'efficitur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse feugiat nulla bibendum orci porttitor lacinia.'
        ])
