from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import uuid
from importlib import import_module
from unittest import mock
from unittest import skip

import copy
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase

from ..api import process_event
from ..enums import InvocationSource, IntentName
from ...enums import Platform
from ...ethereum.factories import AccountFactory
from ...ethereum.factories import TokenPurchaseOrderFactory, WeiPurchaseOrderFactory
from ...ethereum.models import TokenPurchaseOrder, WeiPurchaseOrder, TokenWithdrawal, WeiWithdrawal
from ...ethereum.utils import get_address, generate_key_pair
from ...users.factories import UserFactory

User = get_user_model()

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
Session = SessionStore.get_model_class()


class IntentTestCase(TestCase):
    intent_name = NotImplementedError

    def assertDictNotContainsNull(self, data):
        def is_invalid(d):
            ret = False
            for k, v in d.items():
                if k != 'slots' and isinstance(v, dict):
                    if is_invalid(v):
                        ret = True
                        break
                if k is None or v is None:
                    ret = True
                    break
            return ret

        self.assertFalse(is_invalid(data), json.dumps(data, indent=2))

    def assertSessionAttributes(self, response):
        self.assertIsNotNone(response.get('sessionAttributes').get('key'))
        self.assertTrue(
            SessionStore().model.objects.filter(session_key=response.get('sessionAttributes').get('key')).exists())

    def assertSessionAttributesContainAwsCredentials(self, response):
        self.assertIsNotNone(response.get('sessionAttributes'))
        self.assertIsNotNone(response.get('sessionAttributes').get('openIdToken'))

    def assertResponseEqual(self, actual, expected):
        x = copy.deepcopy(actual)
        y = copy.deepcopy(expected)
        try:
            del y['dialogAction']['message']
        except Exception:
            pass
        try:
            del y['sessionAttributes']
        except Exception:
            pass
        try:
            del x['dialogAction']['message']
        except Exception:
            pass
        try:
            del x['sessionAttributes']
        except Exception:
            pass
        self.assertEqual(x, y)

    def _init_slots(self, slots):
        if 'intents' not in self.session:
            self.session['intents'] = {}
        if self.intent_name not in self.session['intents']:
            self.session['intents'][self.intent_name] = {}
        self.session['intents'][self.intent_name]['slots'] = slots
        self.session.save()


class GetWeiBalanceIntentTestCase(IntentTestCase):
    def setUp(self):
        self.patchers = [
            mock.patch('wyre.lex.intents.get_default_welcome_message_id',
                       mock.MagicMock(return_value='844385345234'))
        ]
        [p.start() for p in self.patchers]
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['twitter_sender_screen_name'] = '@megaman'
        self.session['twitter_sender_id'] = '1234567890'
        self.session.create()

        self.event = dict({
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': None,
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'WyreGetWeiBalance',
                'slots': {
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Yo! What is my balance?'
        })
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]

    def test_fulfillment(self):
        event = self.event.copy()
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_public_channel(self):
        event = self.event.copy()
        event['requestAttributes'] = {
            'isPublicChannel': str(True),
            'platform': str(int(Platform.TWITTER))
        }
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class GetTokenBalanceIntentTestCase(IntentTestCase):
    def setUp(self):
        self.patchers = [
            mock.patch('wyre.lex.intents.get_default_welcome_message_id',
                       mock.MagicMock(return_value='844385345234'))
        ]
        [p.start() for p in self.patchers]

        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['twitter_sender_screen_name'] = '@megaman'
        self.session['twitter_sender_id'] = '1234567890'
        self.session.create()

        self.event = dict({
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': None,
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'WyreGetTokenBalance',
                'slots': {
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Yo! What is my balance?'
        })
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]

    def test_fulfillment(self):
        event = self.event.copy()
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_public_channel(self):
        event = self.event.copy()
        event['requestAttributes'] = {
            'isPublicChannel': str(True),
            'platform': str(int(Platform.TWITTER))
        }
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class HowItWorksIntentTestCase(IntentTestCase):
    def setUp(self):
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        self.event = dict({
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': None,
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'WyreHowItWorks',
                'slots': {
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Yo! How do you work?'
        })
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        pass

    def test_fulfillment(self):
        event = self.event.copy()
        self.session['intents'] = {
            'WyreHowItWorks': {
                'num_events': 3
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class HelloIntentTestCase(IntentTestCase):
    def setUp(self):
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        self.event = dict({
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': None,
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'WyreHello',
                'slots': {
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Hello!'
        })
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        pass

    def test_fulfillment(self):
        event = self.event.copy()
        self.session['intents'] = {
            'Hello': {
                'num_events': 3
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class TransferTokensIntentTestCase(IntentTestCase):
    intent_name = IntentName.TRANSFER_TOKENS.value

    def setUp(self):
        def mock_memoize(*args, **kwargs):
            def inner(*args, **kwargs):
                class User:
                    id = uuid.uuid4().hex
                    screen_name = uuid.uuid4().hex

                return User()

            return inner

        self.patchers = [
            mock.patch('wyre.twitter.utils.Api'),
            mock.patch('wyre.ethereum.forms.TwitterApi'),
            mock.patch('wyre.ethereum.forms.memoize', mock.MagicMock(return_value=mock_memoize))
        ]
        [p.start() for p in self.patchers]
        self.account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = self.account.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['platform'] = Platform.TWITTER
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': {
                'key': self.session.session_key
            },
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': self.intent_name,
                'slots': {
                    'recipient': None,
                    'numTokens': None
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'I want to send WYRE'
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I to send some WYRE'
        self.session['intents'] = {
            self.intent_name: {
                'num_events': 0
            }
        }
        self.session.save()
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'recipient': None,
                    'numTokens': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'recipient'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_recipient(self):
        event = self.event.copy()
        event['inputTranscript'] = '@jigglypuff'
        event['currentIntent']['slots'] = {
            'recipient': event['inputTranscript'],
            'numTokens': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'recipient': '@jigglypuff',
                    'numTokens': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'numTokens'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_recipient(self):
        event = self.event.copy()
        event['inputTranscript'] = 'jigglypuff'
        event['currentIntent']['slots'] = {
            'recipient': event['inputTranscript'],
            'numTokens': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'recipient': None,
                    'numTokens': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'recipient'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_fulfillment(self):
        event = self.event.copy()
        event['inputTranscript'] = '9000'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'recipient': '@jigglypuff'
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        patcher = mock.patch('wyre.ethereum.forms.get_estimated_num_transactions', mock.MagicMock(return_value=3))
        patcher.start()
        event = self.event.copy()
        event['inputTranscript'] = '9000'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'recipient': '@jigglypuff'
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        patcher.stop()

    def test_multiple_slots_filled(self):
        event = self.event.copy()
        event['currentIntent'] = {
            'name': 'WyreTransferTokens',
            'slots': {
                'recipient': '@megaman',
                'numTokens': '5'
            },
            'slotDetails': {
                'recipient': {
                    'resolutions': [
                        {
                            'value': '@megaman'
                        }
                    ],
                    'originalValue': '@megaman'
                },
                'numTokens': {
                    'resolutions': [],
                    'originalValue': '5'
                }
            },
            'confirmationStatus': 'None'
        }
        event['inputTranscript'] = 'send 5 wyre to @megaman'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_num_tokens_with_commas(self):
        event = self.event.copy()
        event['currentIntent'] = {
            'name': 'WyreTransferTokens',
            'slots': {
                'recipient': '@megaman',
                'numTokens': '1000'
            },
            'slotDetails': {
                'recipient': {
                    'resolutions': [],
                    'originalValue': '@megaman'
                },
                'numTokens': {
                    'resolutions': [],
                    'originalValue': '1,000'
                }
            },
            'confirmationStatus': 'None'
        }
        event['inputTranscript'] = '1,000'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class HelpIntentTestCase(IntentTestCase):
    intent_name = IntentName.HELP.value

    def setUp(self):
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': None,
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'WyreHelp',
                'slots': {
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Help'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_fulfillment(self):
        event = self.event.copy()
        self.session['intents'] = {
            'WyreCreateClient': {
                'num_events': 3
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertFalse('intents' in self.session)


class CancelIntentTestCase(IntentTestCase):
    intent_name = IntentName.CANCEL.value

    def setUp(self):
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.FULFILLMENT_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': None,
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': 'WyreCancel',
                'slots': {
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'Cancel'
        }
        self.context = {
        }
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }

    def tearDown(self):
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_fulfillment(self):
        event = self.event.copy()
        self.session['intents'] = {
            'WyreHello': {
                'num_events': 3
            }
        }
        self.session.save()

        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertFalse('intents' in self.session)


class CreateTokenPurchaseOrderIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_TOKEN_PURCHASE_ORDER.value

    def setUp(self):
        self.patchers = [
            mock.patch('wyre.lex.intents.get_default_welcome_message_id',
                       mock.MagicMock(return_value='844385345234'))
        ]
        [p.start() for p in self.patchers]
        self.account = AccountFactory()
        self.user = self.account.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['platform'] = Platform.TWITTER
        self.session['twitter_sender_screen_name'] = '@megaman'
        self.session['twitter_sender_id'] = '1234567890'
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': {
                'key': self.session.session_key
            },
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': self.intent_name,
                'slots': {
                    'numTokens': None,
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'I want to buy WYRE'
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I want to buy some Wyre'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': None,
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'numTokens'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_do_use_last_email_address(self):
        TokenPurchaseOrderFactory(user=self.user)
        event = self.event.copy()
        event['inputTranscript'] = 'yes32313'
        event['currentIntent']['slots'] = {
            'numTokens': '1000',
            'emailAddress': None,
            'doUseLastEmailAddress': event['inputTranscript'],
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doUseLastEmailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_do_use_last_email_address(self):
        TokenPurchaseOrderFactory(user=self.user)
        event = self.event.copy()
        event['inputTranscript'] = 'yes'
        event['currentIntent']['slots'] = {
            'numTokens': '1000',
            'emailAddress': None,
            'doUseLastEmailAddress': event['inputTranscript'],
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "emailAddress": {
                "resolutions": [],
                "originalValue": None
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "doUseLastEmailAddress": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
            "numTokens": {
                "resolutions": [],
                "originalValue": 1000
            }
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000',
                    'doUseLastEmailAddress': 'yes',
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_elicit_do_use_last_email_address(self):
        TokenPurchaseOrderFactory(user=self.user)
        event = self.event.copy()
        event['inputTranscript'] = '1000'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'emailAddress': None,
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doUseLastEmailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_email_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fekfwe@gmail.com'
        event['currentIntent']['slots'] = {
            'numTokens': '1000',
            'emailAddress': event['inputTranscript'],
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000',
                    'doUseLastEmailAddress': None,
                    'emailAddress': 'fekfwe@gmail.com',
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_email_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fekfwe@gmail....feqfew'
        event['currentIntent']['slots'] = {
            'numTokens': '1000',
            'emailAddress': event['inputTranscript'],
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'emailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_num_tokens_0(self):
        event = self.event.copy()
        event['inputTranscript'] = '1000'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'emailAddress': None,
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'emailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_num_tokens_1(self):
        event = self.event.copy()
        event['inputTranscript'] = '1000.23191'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'emailAddress': None,
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1000.23191',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'emailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_do_confirm(self):
        event = self.event.copy()
        event['inputTranscript'] = 'yes342432'
        event['currentIntent']['slots'] = {
            'numTokens': '100',
            'emailAddress': 'giveittome@giveittome.com',
            'doUseLastEmailAddress': None,
            'doConfirm': event['inputTranscript']
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '100',
                    'doUseLastEmailAddress': None,
                    'emailAddress': 'giveittome@giveittome.com',
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        event = self.event.copy()
        event['inputTranscript'] = 'no'
        event['currentIntent']['slots'] = {
            'numTokens': '100',
            'emailAddress': 'giveittome@giveittome.com',
            'doUseLastEmailAddress': 'no',
            'doConfirm': 'no'
        }

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(TokenPurchaseOrder.objects.filter(user=self.user).count(), 0)

    def test_fulfillment(self):
        event = self.event.copy()
        event['currentIntent']['slots'] = {
            'numTokens': '10000',
            'emailAddress': 'giveittome@giveittome.com',
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        event['inputTranscript'] = 'yes'

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(
            TokenPurchaseOrder.objects.filter(user=self.user, email_address='giveittome@giveittome.com').count(), 1)

    def test_overwrite_existing_slots_in_session(self):  # AKA, trust that Lex got its ish together
        event = self.event.copy()
        event['currentIntent'] = {
            "name": "WyreCreateTokenPurchaseOrder",
            "slots": {
                "emailAddress": None,
                "doConfirm": None,
                "doUseLastEmailAddress": None,
                "numTokens": "10000"
            },
            "slotDetails": {
                "emailAddress": {
                    "resolutions": [],
                    "originalValue": None
                },
                "doConfirm": {
                    "resolutions": [],
                    "originalValue": None
                },
                "doUseLastEmailAddress": {
                    "resolutions": [],
                    "originalValue": None
                },
                "numTokens": {
                    "resolutions": [],
                    "originalValue": None
                }
            },
            "confirmationStatus": "None"
        }
        event['inputTranscript'] = 'I want to buy 10000 tokens'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '10000',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'emailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_null_slot_detail(self):
        event = self.event.copy()
        event['currentIntent'] = {
            "name": "WyreCreateTokenPurchaseOrder",
            "slots": {
                "emailAddress": "giveittome@giveittome.com",
                "doConfirm": "yes",
                "doUseLastEmailAddress": None,
                "numTokens": "2000"
            },
            "slotDetails": {
                "emailAddress": None,  # But why Lex...?
                "doConfirm": {
                    "resolutions": [
                        {
                            "value": "Yes"
                        }
                    ],
                    "originalValue": "yes"
                },
                "doUseLastEmailAddress": {
                    "resolutions": [],
                    "originalValue": None
                },
                "numTokens": None
            },
            "confirmationStatus": "None"
        }
        event['inputTranscript'] = 'yes'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(
            TokenPurchaseOrder.objects.filter(user=self.user, email_address='giveittome@giveittome.com').count(), 1)

    def test_public_channel(self):
        event = self.event.copy()
        event['requestAttributes'] = {
            'isPublicChannel': str(True),
            'platform': str(int(Platform.TWITTER))
        }
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class CreateWeiPurchaseOrderIntentTestCase(IntentTestCase):
    intent_name = IntentName.CREATE_WEI_PURCHASE_ORDER.value

    def setUp(self):
        self.patchers = [
            mock.patch('wyre.lex.intents.get_default_welcome_message_id',
                       mock.MagicMock(return_value='844385345234'))
        ]
        [p.start() for p in self.patchers]
        self.account = AccountFactory()
        self.user = self.account.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['platform'] = Platform.TWITTER
        self.session['twitter_sender_screen_name'] = '@megaman'
        self.session['twitter_sender_id'] = '1234567890'
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': {
                'key': self.session.session_key
            },
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': self.intent_name,
                'slots': {
                    'weiCharge': None,
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'I want to buy gas'
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I want to buy some gas'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': None,
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'weiCharge'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_do_use_last_email_address(self):
        WeiPurchaseOrderFactory(user=self.user)
        event = self.event.copy()
        event['inputTranscript'] = 'yes32313'
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': None,
            'doUseLastEmailAddress': event['inputTranscript'],
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doUseLastEmailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_do_use_last_email_address(self):
        WeiPurchaseOrderFactory(user=self.user)
        event = self.event.copy()
        event['inputTranscript'] = 'yes'
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': None,
            'doUseLastEmailAddress': event['inputTranscript'],
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "emailAddress": {
                "resolutions": [],
                "originalValue": None
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "doUseLastEmailAddress": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
            "weiCharge": {
                "resolutions": [],
                "originalValue": '$5.00'
            }
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': 'yes',
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_elicit_do_use_last_email_address(self):
        WeiPurchaseOrderFactory(user=self.user)
        event = self.event.copy()
        event['inputTranscript'] = '$5.00'
        event['currentIntent']['slots'] = {
            'weiCharge': event['inputTranscript'],
            'emailAddress': None,
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doUseLastEmailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_email_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fekfwe@gmail.com'
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': event['inputTranscript'],
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': None,
                    'emailAddress': 'fekfwe@gmail.com',
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_email_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'fekfwe@gmail....feqfew'
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': event['inputTranscript'],
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'emailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_wei_charge(self):
        event = self.event.copy()
        event['inputTranscript'] = '$5.00'
        event['currentIntent']['slots'] = {
            'weiCharge': event['inputTranscript'],
            'emailAddress': None,
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'emailAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_wei_charge(self):
        event = self.event.copy()
        event['inputTranscript'] = '$6.00'
        event['currentIntent']['slots'] = {
            'weiCharge': event['inputTranscript'],
            'emailAddress': None,
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': None,
                    'doUseLastEmailAddress': None,
                    'emailAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'weiCharge'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_do_confirm(self):
        event = self.event.copy()
        event['inputTranscript'] = 'yes342432'
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': 'giveittome@giveittome.com',
            'doUseLastEmailAddress': None,
            'doConfirm': event['inputTranscript']
        }
        self.session.save()

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'weiCharge': '$5.00',
                    'doUseLastEmailAddress': None,
                    'emailAddress': 'giveittome@giveittome.com',
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        event = self.event.copy()
        event['inputTranscript'] = 'no'
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': 'giveittome@giveittome.com',
            'doUseLastEmailAddress': 'no',
            'doConfirm': 'no'
        }

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(WeiPurchaseOrder.objects.filter(user=self.user).count(), 0)

    def test_fulfillment(self):
        event = self.event.copy()
        event['currentIntent']['slots'] = {
            'weiCharge': '$5.00',
            'emailAddress': 'giveittome@giveittome.com',
            'doUseLastEmailAddress': None,
            'doConfirm': None
        }
        event['inputTranscript'] = 'yes'

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(
            WeiPurchaseOrder.objects.filter(user=self.user, email_address='giveittome@giveittome.com').count(),
            1
        )

    def test_null_slot_detail(self):
        event = self.event.copy()
        event['currentIntent'] = {
            "name": "WyreCreateWeiPurchaseOrder",
            "slots": {
                "emailAddress": "giveittome@giveittome.com",
                "doConfirm": "yes",
                "doUseLastEmailAddress": None,
                "weiCharge": "5.00"
            },
            "slotDetails": {
                "emailAddress": None,  # But why Lex...?
                "doConfirm": {
                    "resolutions": [
                        {
                            "value": "Yes"
                        }
                    ],
                    "originalValue": "yes"
                },
                "doUseLastEmailAddress": {
                    "resolutions": [],
                    "originalValue": None
                },
                "weiCharge": None
            },
            "confirmationStatus": "None"
        }
        event['inputTranscript'] = 'yes'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(
            WeiPurchaseOrder.objects.filter(user=self.user, email_address='giveittome@giveittome.com').count(),
            1
        )

    def test_public_channel(self):
        event = self.event.copy()
        event['requestAttributes'] = {
            'isPublicChannel': str(True),
            'platform': str(int(Platform.TWITTER))
        }
        session_attributes = event.get('sessionAttributes').copy()
        expected_response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.session = SessionStore(session_key=self.session.session_key)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)


class WithdrawTokensIntentTestCase(IntentTestCase):
    intent_name = IntentName.WITHDRAW_TOKENS.value

    def setUp(self):
        self.patchers = [
            mock.patch('wyre.lex.intents.get_default_welcome_message_id', mock.MagicMock(return_value='844385345234'))
        ]
        [p.start() for p in self.patchers]
        self.account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = self.account.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['platform'] = Platform.TWITTER
        self.session['twitter_sender_screen_name'] = '@megaman'
        self.session['twitter_sender_id'] = '1234567890'
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': {
                'key': self.session.session_key
            },
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': self.intent_name,
                'slots': {
                    'numTokens': None,
                    'toAddress': None,
                    'doConfirm': None
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'I want to withdraw'
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I want to withdraw'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': None,
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'numTokens'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_num_tokens(self):
        event = self.event.copy()
        event['inputTranscript'] = 'yes32313'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'toAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': None,
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'numTokens'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_num_tokens(self):
        event = self.event.copy()
        event['inputTranscript'] = '1'
        event['currentIntent']['slots'] = {
            'numTokens': event['inputTranscript'],
            'toAddress': None,
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "numTokens": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "toAddress": {
                "resolutions": [],
                "originalValue": None
            },
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1',
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'toAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_to_address(self):
        event = self.event.copy()
        event['inputTranscript'] = get_address(public_key=generate_key_pair()[0])
        event['currentIntent']['slots'] = {
            'numTokens': '1',
            'toAddress': event['inputTranscript'],
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "numTokens": {
                "resolutions": [],
                "originalValue": '1'
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "toAddress": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1',
                    'toAddress': event['inputTranscript'],
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_to_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'deadbeef'
        event['currentIntent']['slots'] = {
            'numTokens': '1',
            'toAddress': event['inputTranscript'],
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "numTokens": {
                "resolutions": [],
                "originalValue": '1'
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "toAddress": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numTokens': '1',
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'toAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        event = self.event.copy()
        event['inputTranscript'] = 'no'
        event['currentIntent']['slots'] = {
            'numTokens': '1',
            'toAddress': get_address(public_key=generate_key_pair()[0]),
            'doConfirm': event['inputTranscript']
        }

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(TokenWithdrawal.objects.filter(user=self.user).count(), 0)

    def test_fulfillment(self):
        event = self.event.copy()
        event['currentIntent']['slots'] = {
            'numTokens': '1',
            'toAddress': get_address(public_key=generate_key_pair()[0]),
            'doConfirm': 'yes'
        }
        event['inputTranscript'] = 'yes'

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(TokenWithdrawal.objects.filter(user=self.user).count(), 1)


@skip('')
class WithdrawWeiIntentTestCase(IntentTestCase):
    intent_name = IntentName.WITHDRAW_WEI.value

    def setUp(self):
        self.patchers = [
            mock.patch('wyre.lex.intents.get_default_welcome_message_id', mock.MagicMock(return_value='844385345234'))
        ]
        [p.start() for p in self.patchers]
        self.account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = self.account.user
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session['platform'] = Platform.TWITTER
        self.session['twitter_sender_screen_name'] = '@megaman'
        self.session['twitter_sender_id'] = '1234567890'
        self.session.create()

        # This is what Lex will send once the intent is identified
        self.event = {
            'messageVersion': '1.0',
            'invocationSource': InvocationSource.DIALOG_CODE_HOOK.value,
            'userId': 'User:{0}'.format(self.user.id),
            'sessionAttributes': {
                'key': self.session.session_key
            },
            'bot': {
                'name': 'Wyre',
                'alias': None,
                'version': '$LATEST'
            },
            'outputDialogMode': 'Text',
            'currentIntent': {
                'name': self.intent_name,
                'slots': {
                    'numEther': None,
                    'toAddress': None,
                    'doConfirm': None
                },
                'slotDetails': {},
                'confirmationStatus': 'None'
            },
            'inputTranscript': 'I want to withdraw ether'
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()
        Session.objects.all().delete()

    def test_init(self):
        event = self.event.copy()
        event['inputTranscript'] = 'I want to withdraw ether'
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numEther': None,
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'numEther'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_num_ether(self):
        event = self.event.copy()
        event['inputTranscript'] = 'yes32313'
        event['currentIntent']['slots'] = {
            'numEther': event['inputTranscript'],
            'toAddress': None,
            'doConfirm': None
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numEther': None,
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'numEther'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_num_ether(self):
        event = self.event.copy()
        event['inputTranscript'] = '1.5'
        event['currentIntent']['slots'] = {
            'numEther': event['inputTranscript'],
            'toAddress': None,
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "numEther": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "toAddress": {
                "resolutions": [],
                "originalValue": None
            },
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numEther': '1.5',
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'toAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_valid_to_address(self):
        event = self.event.copy()
        event['inputTranscript'] = get_address(public_key=generate_key_pair()[0])
        event['currentIntent']['slots'] = {
            'numEther': '1',
            'toAddress': event['inputTranscript'],
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "numEther": {
                "resolutions": [],
                "originalValue": '1'
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "toAddress": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numEther': '1',
                    'toAddress': event['inputTranscript'],
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'doConfirm'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_invalid_to_address(self):
        event = self.event.copy()
        event['inputTranscript'] = 'deadbeef'
        event['currentIntent']['slots'] = {
            'numEther': '1',
            'toAddress': event['inputTranscript'],
            'doConfirm': None
        }

        event['currentIntent']['slotDetails'] = {
            "numEther": {
                "resolutions": [],
                "originalValue": '1'
            },
            "doConfirm": {
                "resolutions": [],
                "originalValue": None
            },
            "toAddress": {
                "resolutions": [],
                "originalValue": event['inputTranscript']
            },
        }
        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'ElicitSlot',
                'slots': {
                    'numEther': '1',
                    'toAddress': None,
                    'doConfirm': None
                },
                'intentName': self.intent_name,
                'slotToElicit': 'toAddress'
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)

    def test_failure(self):
        event = self.event.copy()
        event['inputTranscript'] = 'no'
        event['currentIntent']['slots'] = {
            'numEther': '1',
            'toAddress': get_address(public_key=generate_key_pair()[0]),
            'doConfirm': event['inputTranscript']
        }

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Failed',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(WeiWithdrawal.objects.filter(user=self.user).count(), 0)

    def test_fulfillment(self):
        event = self.event.copy()
        event['currentIntent']['slots'] = {
            'numEther': '1',
            'toAddress': get_address(public_key=generate_key_pair()[0]),
            'doConfirm': 'yes'
        }
        event['inputTranscript'] = 'yes'

        expected_response = {
            'sessionAttributes': event.get('sessionAttributes'),
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': 'Fulfilled',
            }
        }
        response = process_event(event=event)
        self.assertResponseEqual(response, expected_response)
        self.assertSessionAttributes(response)
        self.assertDictNotContainsNull(response)
        self.assertEqual(WeiWithdrawal.objects.filter(user=self.user).count(), 1)
