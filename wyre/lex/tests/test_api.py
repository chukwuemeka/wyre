from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from importlib import import_module

from aws_xray_sdk.core.models.traceid import TraceId
from django.conf import settings
from django.core import signing
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ...users.factories import UserFactory

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


class LexWebhookViewTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(LexWebhookViewTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(LexWebhookViewTestCase, cls).tearDownClass()

    def setUp(self):
        self.user = UserFactory()
        self.session = SessionStore()
        self.session['user_id'] = self.user.id
        self.session.create()
        self.event = dict({
            "messageVersion": "1.0",
            "invocationSource": "FulfillmentCodeHook",
            "userId": "User:3",
            "sessionAttributes": {
                "key": self.session.session_key
            },
            "requestAttributes": None,
            "bot": {
                "name": "Wyre",
                "alias": "Production",
                "version": "$LATEST"
            },
            "outputDialogMode": "Text",
            "currentIntent": {
                "name": "Hello",
                "slots": {},
                "slotDetails": {},
                "confirmationStatus": "None"
            },
            "inputTranscript": "Hello"
        })
        self.event['sessionAttributes'] = {
            'key': self.session.session_key
        }
        self.data = {
            'event': signing.dumps(self.event, key=settings.LEX_WEBHOOK_SECRET_KEY)
        }

    def tearDown(self):
        pass

    def test_trace_id_0(self):
        data = self.data.copy()
        url = reverse('lex_webhook')
        response = self.client.post(url, data, format='json', HTTP_X_AMZN_TRACE_ID='Root={0}'.format(TraceId().to_id()))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_trace_id_1(self):
        data = self.data.copy()
        url = reverse('lex_webhook')
        response = self.client.post(url, data, format='json', HTTP_X_AMZN_TRACE_ID='Self={0};Root={1}'.format(TraceId().to_id(), TraceId().to_id()))
        print('response.request.headers = {0}'.format(response.request))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_not_allowed(self):
        url = reverse('lex_webhook')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_lex_event(self):
        data = self.data.copy()
        url = reverse('lex_webhook')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content_type, 'application/json')
        self.assertTrue('dialogAction' in response.data.keys())
        self.assertTrue('type' in response.data.get('dialogAction'))

    def test_unsigned_event(self):
        data = {
            'event': self.event.copy()
        }
        url = reverse('lex_webhook')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_wrong_key(self):
        data = {
            'event': signing.dumps(self.event.copy(), key='thisisthewrongkeylol')
        }
        url = reverse('lex_webhook')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
