from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from enum import Enum
from enum import IntEnum


class DialogState(object):
    ELICIT_INTENT = 'ElicitIntent'
    FAILED = 'Failed'


class FulfillmentState(Enum):
    FULFILLED = 'Fulfilled'
    FAILED = 'Failed'


class InvocationSource(Enum):
    DIALOG_CODE_HOOK = 'DialogCodeHook'
    FULFILLMENT_CODE_HOOK = 'FulfillmentCodeHook'


class DialogActionType(Enum):
    CLOSE = 'Close'
    DELEGATE = 'Delegate'
    ELICIT_SLOT = 'ElicitSlot'
    ELICIT_INTENT = 'ElicitIntent'


class ConfirmationStatus(Enum):
    NONE = 'None'
    CONFIRMED = 'Confirmed'
    DENIED = 'Denied'


class IntentName(Enum):
    CANCEL = 'WyreCancel'
    HELP = 'WyreHelp'
    HELLO = 'WyreHello'
    TRANSFER_TOKENS = 'WyreTransferTokens'
    CREATE_TOKEN_PURCHASE_ORDER = 'WyreCreateTokenPurchaseOrder'
    CREATE_WEI_PURCHASE_ORDER = 'WyreCreateWeiPurchaseOrder'
    GET_TOKEN_BALANCE = 'WyreGetTokenBalance'
    GET_WEI_BALANCE = 'WyreGetWeiBalance'
    HOW_IT_WORKS = 'WyreHowItWorks'
    WITHDRAW_TOKENS = 'WyreWithdrawTokens'
    WITHDRAW_WEI = 'WyreWithdrawWei'


class Context(IntEnum):
    DEFAULT = 0
    ERROR = 1
