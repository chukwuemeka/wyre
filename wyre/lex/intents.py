from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging

import random
from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from djmoney.money import Money
from functools import reduce
from moneyed.localization import format_money

from wyre.lex.utils import to_pickle_string
from wyre.utils import to_money, to_decimal, format_decimal
from .enums import DialogActionType, InvocationSource, FulfillmentState, IntentName, Context
from .responses import Text, Attachment, Status, QuickReply, UrlButton
from .utils import to_camel_case, to_snake_case
from ..decorators import method_decorator
from ..enums import Platform
from ..ethereum.forms import TransferTokensForm, CreateTokenPurchaseOrderForm, CreateWeiPurchaseOrderForm, \
    WithdrawTokensForm
from ..ethereum.models import TokenPurchaseOrder
from ..ethereum.models import WeiPurchaseOrder
from ..ethereum.utils import get_estimated_num_transactions
from ..ethereum.utils import get_etherscan_url
from ..ethereum.utils import get_or_create_account, get_token_balance, get_ether_balance
from ..twitter.utils import get_default_welcome_message_id
from ..urlresolvers import get_absolute_static_url

User = get_user_model()
logger = logging.getLogger(__name__)
ERC20_TOKEN_SYMBOL = settings.ERC20_TOKEN_SYMBOL
COMMANDS = [
    'I want to buy some {0}'.format(ERC20_TOKEN_SYMBOL),
    'I want to buy some gas',
    'Send {0} to @username'.format(ERC20_TOKEN_SYMBOL),
    'How many tokens do I have?',
    'How much gas do I have?',
    'How does this all work?',
    'I want to withdraw my tokens',
]


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_commands_text'))
def get_commands_text(separator='\n'):
    return separator.join(['"%s"' % c for c in COMMANDS])


# def prepend_twitter_screen_name(text, session=None):
#     screen_name = '@{0} '.format(session['twitter_sender_screen_name']) if session and 'twitter_sender_screen_name' in session and \
#                                                                     session['twitter_sender_screen_name'] else ''
#     return '{0}{1}'.format(screen_name, text)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'create_elicit_intent_response'))
def create_elicit_intent_response(*, event, session=None):
    gif_filename = random.choice(['what_%d.gif' % i for i in range(7)])
    media_url = get_absolute_static_url('gif/%s' % gif_filename)
    content = {
        'statuses': [
            Status(
                status='I do not understand. Try saying something like:\n{0}'.format(get_commands_text(', '), session),
                media_url=media_url)
        ],
        'messages': [
            Attachment(category='image', url=media_url),
            Text('I do not understand that 😕'),
            Text('Try saying something like:\n{0}'.format(get_commands_text()))
        ]
    }
    return {
        'sessionAttributes': event.get('sessionAttributes'),
        'dialogAction': {
            'type': DialogActionType.ELICIT_INTENT.value,
            'message': {
                'contentType': 'PlainText',
                'content': to_pickle_string(content)
            }
        }
    }


# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.create_close_response')), name='create_close_response')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.get_failure_content')), name='get_failure_content')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.get_user')), name='get_user')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.get_account')), name='get_account')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.process_dialog')), name='process_dialog')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.process_event')), name='process_event')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.process_fulfillment')), name='process_fulfillment')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'Intent.process_rejection')), name='process_rejection')
class Intent(object):
    intent_name = None
    is_unauthentication_required = False
    is_authentication_required = False
    is_sensitive = False

    def __init__(self, *, session):
        self.event = None
        self.session = session

    @property
    def is_authenticated(self):
        if 'user_id' in self.session:
            try:
                user = User.objects.get(id=self.session['user_id'])
                return user.is_active
            except ObjectDoesNotExist:
                pass
        return False

    @staticmethod
    def to_lex_key(obj):
        return to_camel_case(obj)

    @staticmethod
    def from_lex_key(obj):
        return to_snake_case(obj)

    # def prepend_twitter_screen_name(self, text):
    #     return prepend_twitter_screen_name(text, self.session)

    def get_user(self):
        if 'user_id' in self.session:
            try:
                user = User.objects.get(id=self.session['user_id'])
                return user
            except ObjectDoesNotExist:
                pass
        return None

    def get_account(self, do_import_raw_key=True):
        user = self.get_user()
        if user:
            return get_or_create_account(user)
        return None

    def create_close_response(self, *, fulfillment_state, content):
        session_attributes = self.event.get('sessionAttributes').copy()
        response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': DialogActionType.CLOSE.value,
                'fulfillmentState': fulfillment_state.value,
                'message': {
                    'contentType': 'PlainText',
                    'content': to_pickle_string(content)
                }
            }
        }
        return response

    def get_welcome_status(self):
        welcome_message_url = None
        if self.event.get('requestAttributes', {}).get('isPublicChannel', 'False') == 'True' and int(
                self.event.get('requestAttributes', {}).get('platform', 9000)) == Platform.TWITTER:
            welcome_message_url = self.get_welcome_message_url()
        return Status(
            status='I\'d be happy to help you 😊 Please click the link below so that we can have a private chat {0}'.format(
                welcome_message_url))

    def get_welcome_message_url(self):
        direct_message_url = 'https://twitter.com/messages/compose?recipient_id={0}&welcome_message_id={1}'.format(
            settings.twitter_id,
            get_default_welcome_message_id()
        )
        logger.info('direct_message_url = {0}'.format(direct_message_url))
        return direct_message_url

    def get_failure_content(self, *args, **kwargs):
        raise NotImplementedError()

    def get_fulfillment_content(self, *args, **kwargs):
        raise NotImplementedError()

    def process_fulfillment(self):
        raise NotImplementedError()

    def process_rejection(self):
        raise NotImplementedError()

    def process_dialog(self):
        raise NotImplementedError()

    def process_event(self, event):
        self.event = event
        if self.is_sensitive and event.get('requestAttributes', {}).get('isPublicChannel', 'False') == 'True' and int(
                event.get('requestAttributes', {}).get('platform', 9000)) == Platform.TWITTER:
            welcome_message_url = 'https://twitter.com/messages/compose?recipient_id={0}&welcome_message_id={1}'.format(
                settings.TWITTER_ID,
                get_default_welcome_message_id()
            )
            text = 'Hey there 👋 I\'d be happy to help you 😊 Please click the link below so that we can have a private chat {0}'.format(
                welcome_message_url)
            return self.create_close_response(
                fulfillment_state=FulfillmentState.FAILED,
                content={
                    'messages': [
                        Text(text)
                    ],
                    'statuses': [
                        Status(
                            status=text
                        )
                    ]
                }
            )
        elif event.get('invocationSource') == InvocationSource.DIALOG_CODE_HOOK.value:
            return self.process_dialog()
        else:
            return self.process_fulfillment()


# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.create_close_response')), name='create_close_response')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.create_elicit_slot_response')), name='create_elicit_slot_response')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.form_invalid')), name='form_invalid')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.form_valid')), name='form_valid')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_account')), name='get_account')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_failure_content')), name='get_failure_content')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_form_kwargs')), name='get_form_kwargs')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_fulfillment_content')), name='get_fulfillment_content')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_initial_data')), name='get_initial_data')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_slot_to_elicit_content')), name='get_slot_to_elicit_content')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.get_user')), name='get_user')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.process_dialog')), name='process_dialog')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.process_event')), name='process_event')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.process_fulfillment')), name='process_fulfillment')
# @method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'FormIntent.process_rejection')), name='process_rejection')
class FormIntent(Intent):
    intent_name = None
    form_class = None
    fields = []

    def get_fulfillment_content(self, form):
        raise NotImplementedError()

    def get_failure_content(self, form):
        raise NotImplementedError()

    def get_slot_to_elicit_content(self, *, slot_to_elicit, context):
        raise NotImplementedError

    def get_initial_data(self):
        initial_data = {}
        return initial_data

    def form_valid(self, form):
        raise NotImplementedError

    def form_invalid(self, form):
        raise NotImplementedError

    def get_form_kwargs(self):
        return {}

    def create_elicit_slot_response(self, *, slot_to_elicit, context=Context.DEFAULT):
        intent = self.event.get('currentIntent').copy()
        session_attributes = self.event.get('sessionAttributes').copy()

        slots = dict({
            **self.event.get('currentIntent').get('slots'),
        })
        if slot_to_elicit:
            slots[slot_to_elicit] = None

        content = self.get_slot_to_elicit_content(slot_to_elicit=slot_to_elicit, context=context)
        return {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': DialogActionType.ELICIT_SLOT.value,
                'slots': self.to_lex_key(slots),
                'intentName': intent.get('name'),
                'slotToElicit': self.to_lex_key(slot_to_elicit),
                'message': {
                    'contentType': 'PlainText',
                    'content': to_pickle_string(content)
                }
            }
        }

    def process_dialog(self):
        # Number of events
        num_events = self.session.get('intents', {}).get(self.intent_name, {}).get('num_events', 0)

        # Check auth
        if self.is_authenticated and self.is_unauthentication_required:
            logger.info('User must be unauthenticated')
            return create_elicit_intent_response(
                event=self.event
            )

        if not self.is_authenticated and self.is_authentication_required:
            logger.info('User must be authenticated')
            return create_elicit_intent_response(
                event=self.event
            )

        # For some reason Lex doesn't capture garbage inputs...?
        # We'll just slide the input into an open slot
        input_transcript = str(self.event.get('inputTranscript'))
        is_transcript_unassigned = True
        for key, value in self.event.get('currentIntent').get('slotDetails').items():
            if value and value.get('originalValue') and value.get('originalValue') in input_transcript:
                is_transcript_unassigned = False
                break
        logger.debug('is_transcript_unassigned = %s' % str(is_transcript_unassigned))

        # Validate the form
        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        are_slots_empty = reduce(
            lambda x, y: x and y is None,
            slots.values(),
            True
        )
        data = self.get_initial_data()
        for field in self.fields:
            data[field] = slots.get(field)
        form = self.form_class(**{**self.get_form_kwargs(), **{'data': data}})

        # Handle unassigned input transcript
        if is_transcript_unassigned:
            field_to_elicit = form.get_field_to_elicit()
            slots[field_to_elicit] = input_transcript
            data = self.get_initial_data()
            for field in self.fields:
                data[field] = slots.get(field)
            logger.debug('We are assigning the input transcript (%s) to %s', input_transcript, field_to_elicit)
            form = self.form_class(**{**self.get_form_kwargs(), **{'data': data}})

        field_to_elicit = form.get_field_to_elicit()
        logger.debug('field_to_elicit = %s' % field_to_elicit)
        logger.debug('form.is_valid = %s' % str(form.is_valid()))
        logger.debug('data = \n%s' % json.dumps(data, indent=2))
        logger.debug('form.errors = \n%s' % json.dumps(form.errors, indent=2))

        if not form.is_valid():
            errors = form.errors
            if form.data.get(field_to_elicit) and field_to_elicit in errors:
                # logger.debug('Invalid %s!' % field_to_elicit)
                context = Context.ERROR
                if are_slots_empty and is_transcript_unassigned and num_events <= 1:
                    context = Context.DEFAULT
                response = self.create_elicit_slot_response(
                    slot_to_elicit=field_to_elicit,
                    context=context
                )
            else:  # Elicit the next slot
                response = self.create_elicit_slot_response(
                    slot_to_elicit=field_to_elicit
                )
        else:
            response = self.process_fulfillment(form)
        return response

    def process_fulfillment(self, form=None):
        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        if not form:
            data = self.get_initial_data()
            for field in self.fields:
                data[field] = slots.get(field)
            form = self.form_class(**{**self.get_form_kwargs(), **{'data': data}})

        if form.is_valid() and form.is_fulfilled():
            self.form_valid(form)
            return self.create_close_response(fulfillment_state=FulfillmentState.FULFILLED,
                                              content=self.get_fulfillment_content(form))
        else:
            logger.debug('form.errors = \n%s' % json.dumps(form.errors, indent=2))
            return self.create_close_response(fulfillment_state=FulfillmentState.FAILED,
                                              content=self.get_failure_content(form))


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.create_elicit_slot_response')), name='create_elicit_slot_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.form_invalid')), name='form_invalid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.form_valid')), name='form_valid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_form_kwargs')), name='get_form_kwargs')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_fulfillment_content')), name='get_fulfillment_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_initial_data')), name='get_initial_data')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_slot_to_elicit_content')), name='get_slot_to_elicit_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateTokenPurchaseOrderIntent.process_rejection')), name='process_rejection')
class CreateTokenPurchaseOrderIntent(FormIntent):
    intent_name = IntentName.CREATE_TOKEN_PURCHASE_ORDER.value
    form_class = CreateTokenPurchaseOrderForm
    fields = [
        'num_tokens',
        'do_use_last_email_address',
        'email_address',
        'do_confirm'
    ]
    token_purchase_order = None
    is_sensitive = True

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        user = self.get_user()

        initial_data.update({
            'user': user.id,
        })

        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        num_tokens = to_decimal(slots.get('num_tokens'))

        if num_tokens:
            token_charge, wei_charge, service_charge, num_wei, wei_estimate = TokenPurchaseOrder.get_charges(
                user=user,
                num_tokens=num_tokens
            )
            initial_data.update({
                'user': user.id,
                'token_charge': format_money(token_charge, locale='en_US'),
                'wei_charge': format_money(wei_charge, locale='en_US'),
                'service_charge': format_money(service_charge, locale='en_US'),
                'num_wei': num_wei,
                'wei_estimate': wei_estimate
            })
        logger.debug('initial_data = {0}'.format(json.dumps(initial_data, indent=2)))
        return initial_data

    def form_valid(self, form):
        self.token_purchase_order = form.save()  # Sorry so sloggy

    def form_invalid(self, form):
        pass

    def get_failure_content(self, form):
        return {
            'messages': [
                Text('No problem 👌'),
                Text('I will cancel your order'),
            ],
            'statuses': [
                Status(
                    status='No problem 👌 I will cancel your order'
                )
            ]
        }

    def get_fulfillment_content(self, form):
        assert form.is_valid()
        url = 'https://{0}{1}'.format(settings.SITE_DOMAIN,
                                      reverse('token_checkout', kwargs={'token': self.token_purchase_order.token}))
        return {
            'messages': [
                Text('Please tap the button below to complete your order. Once you complete your order, I will let you know when your {0} tokens are ready'.format(
                    ERC20_TOKEN_SYMBOL), buttons=[UrlButton(label='Proceed to Checkout 🛒', url=url)]),
            ],
            'statuses': [
                Status(status='Please go to this link to complete your order: {0}'.format(url))
            ]
        }

    def get_slot_to_elicit_content(self, *, slot_to_elicit, context=Context.DEFAULT):
        user = self.get_user()

        # Has this customer created an order in the past? Let's find the last email address
        last_email_address = None
        queryset = TokenPurchaseOrder.objects.filter(user=user).order_by('-date_created')
        if queryset.exists():
            last_email_address = queryset.first().email_address

        # Find the email address
        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        if slots.get('email_address'):
            email_address = slots.get('email_address')
        else:
            email_address = last_email_address

        num_tokens = to_decimal(slots.get('num_tokens'))
        token_charge = Money(0, 'USD')
        if num_tokens:
            try:
                token_charge = TokenPurchaseOrder.get_charges(user=user, num_tokens=num_tokens)[0]
            except Exception:
                pass

        def get_email_address_content():
            if slots.get('do_use_last_email_address') == 'no':
                return {
                    'messages': [
                        Text('Which email address should I send your receipt to?')
                    ],
                    'statuses': [
                        Status(status='Which email address should I send your receipt to?')
                    ]
                }
            return {
                'messages': [
                    Text('Your subtotal for {0} {1} tokens comes out to {2}'.format(format_decimal(num_tokens),
                                                                                    ERC20_TOKEN_SYMBOL,
                                                                                    format_money(token_charge,
                                                                                                 locale='en_US'))),
                    Text('I will generate a link that you can use to complete your order'),
                    Text('Before I can do that, I need to know which email address I should send your receipt to?')
                ],
                'statuses': [
                    Status(status='Which email address should I send your receipt to?')
                ],
            }

        content = {
            Context.DEFAULT: {
                'num_tokens': {
                    'messages': [
                        Text('How many {0} tokens would you like to purchase?'.format(ERC20_TOKEN_SYMBOL))
                    ],
                    'statuses': [
                        Status(status='How many {0} tokens would you like to purchase?'.format(ERC20_TOKEN_SYMBOL))
                    ]
                },
                'do_use_last_email_address': {
                    'messages': [
                        Text('Your subtotal for {0} {1} tokens comes out to {2}'.format(format_decimal(num_tokens),
                                                                                        ERC20_TOKEN_SYMBOL,
                                                                                        format_money(token_charge,
                                                                                                     locale='en_US'))),
                        Text('I will generate a link that you can use to complete your order'),
                        QuickReply(
                            text='Before I can do that, I need to know if would you like me to send your receipt to {0}?'.format(
                                last_email_address),
                            options=[
                                {'title': 'Yes', 'payload': 'yes'},
                                {'title': 'No', 'payload': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='Would you like me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                last_email_address))
                    ]
                },
                'email_address': get_email_address_content(),
                'do_confirm': {
                    'messages': [
                        QuickReply(
                            text='Just to confirm, do you want to purchase {0} {1} tokens and have your receipt sent to {2}?'.format(
                                format_decimal(num_tokens),
                                ERC20_TOKEN_SYMBOL,
                                email_address
                            ),
                            options=[
                                {'title': 'Yes', 'payload': 'yes'},
                                {'title': 'No', 'payload': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='Just to confirm, do you want me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                email_address))
                    ]
                },
            },
            Context.ERROR: {
                'num_tokens': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        Text(
                            'Please use a valid number for the amount of {0} tokens that you would like to purchase.'.format(
                                ERC20_TOKEN_SYMBOL))
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand 😕 Please use a valid number for the amount of {0} tokens that you would like to purchase.'.format(
                                ERC20_TOKEN_SYMBOL))
                    ]
                },
                'do_use_last_email_address': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        QuickReply(
                            text='Do you want me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                last_email_address),
                            options=[
                                {'title': 'Yes', 'metadata': 'yes'},
                                {'title': 'No', 'metadata': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(status='I don\'t understand 😕 Do you want me to send your receipt to {0}? '
                                      'Please answer with a "Yes" or "No"'.format(last_email_address))
                    ]
                },
                'email_address': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        Text('Please give me a valid email address (ex: johndoe@gmail.com) to send your receipt to.')
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand 😕 Please give me a valid email address (ex: johndoe@gmail.com) to send your receipt to.')
                    ]
                },
                'do_confirm': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        QuickReply(
                            text='Do you want to purchase {0} {1} tokens and have your receipt sent to {2}? Please answer with a "Yes" or "No"'.format(
                                format_decimal(num_tokens),
                                ERC20_TOKEN_SYMBOL,
                                email_address
                            ),
                            options=[
                                {'title': 'Yes', 'metadata': 'yes'},
                                {'title': 'No', 'metadata': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand 😕 Do you want me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                email_address))
                    ]
                }
            }
        }
        return content.get(context).get(slot_to_elicit)


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.create_elicit_slot_response')), name='create_elicit_slot_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.form_invalid')), name='form_invalid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.form_valid')), name='form_valid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_form_kwargs')), name='get_form_kwargs')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_fulfillment_content')), name='get_fulfillment_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_initial_data')), name='get_initial_data')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_slot_to_elicit_content')), name='get_slot_to_elicit_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CreateWeiPurchaseOrderIntent.process_rejection')), name='process_rejection')
class CreateWeiPurchaseOrderIntent(FormIntent):
    intent_name = IntentName.CREATE_WEI_PURCHASE_ORDER.value
    form_class = CreateWeiPurchaseOrderForm
    fields = [
        'wei_charge',
        'do_use_last_email_address',
        'email_address',
        'do_confirm'
    ]
    wei_purchase_order = None
    is_sensitive = True

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        user = self.get_user()

        initial_data.update({
            'user': user.id,
        })

        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        wei_charge = to_money(slots.get('wei_charge'))
        if wei_charge:
            service_charge, num_wei, wei_estimate = WeiPurchaseOrder.get_charges(
                user=user,
                wei_charge=wei_charge
            )
            initial_data.update({
                'user': user.id,
                'service_charge': format_money(service_charge, locale='en_US'),
                'num_wei': num_wei,
                'wei_estimate': wei_estimate
            })
        logger.debug('initial_data = {0}'.format(json.dumps(initial_data, indent=2)))
        return initial_data

    def form_valid(self, form):
        self.wei_purchase_order = form.save()  # Sorry so sloggy

    def form_invalid(self, form):
        pass

    # def process_dialog(self):
    #     response = super().process_dialog()
    #     if response.get('dialogAction'):
    #         if response.get('dialogAction').get('slots'):
    #             if response.get('dialogAction').get('slots').get('numTokens'):
    #                 response['dialogAction']['slots']['numTokens'] = int(response['dialogAction']['slots']['numTokens'])
    #     return response

    def get_failure_content(self, form):
        return {
            'messages': [
                Text('No problem 👌'),
                Text('I will cancel your order'),
            ],
            'statuses': [
                Status(
                    status='No problem 👌 I will cancel your order'
                )
            ]
        }

    def get_fulfillment_content(self, form):
        assert form.is_valid()
        url = 'https://{0}{1}'.format(settings.SITE_DOMAIN,
                                      reverse('wei_checkout', kwargs={'token': self.wei_purchase_order.token}))
        return {
            'messages': [
                Text('Please tap the button below to complete your order. Once you complete your order, I will let you know when your gas is ready', buttons=[UrlButton(label='Proceed to Checkout 🛒', url=url)]),
            ],
            'statuses': [
                Status(status='Please go to this link to complete your order: {0}'.format(url))
            ]
        }

    def get_slot_to_elicit_content(self, *, slot_to_elicit, context=Context.DEFAULT):
        user = self.get_user()

        # Has this customer created an order in the past? Let's find the last email address
        last_email_address = None
        queryset = WeiPurchaseOrder.objects.filter(user=user).order_by('-date_created')
        if queryset.exists():
            last_email_address = queryset.first().email_address

        # Find the email address
        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        if slots.get('email_address'):
            email_address = slots.get('email_address')
        else:
            email_address = last_email_address

        def get_email_address_content():
            if slots.get('do_use_last_email_address') == 'no':
                return {
                    'messages': [
                        Text('Which email address should I send your receipt to?')
                    ],
                    'statuses': [
                        Status(status='Which email address should I send your receipt to?')
                    ]
                }
            return {
                'messages': [
                    Text('I will generate a link that you can use to complete your order'),
                    Text('Before I can do that, I need to know which email address I should send your receipt to?')
                ],
                'statuses': [
                    Status(status='Which email address should I send your receipt to?')
                ],
            }

        content = {
            Context.DEFAULT: {
                'wei_charge': {
                    'messages': [
                        QuickReply(
                            text='How much would you like to spend on gas?',
                            options=[
                                {'title': '$5.00', 'payload': 5.00},
                                {'title': '$10.00', 'payload': 10.00},
                                {'title': '$25.00', 'payload': 25.00},
                                {'title': '$50.00', 'payload': 50.00},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(status='How much would you like to spend on gas?')
                    ]
                },
                'do_use_last_email_address': {
                    'messages': [
                        Text('I will generate a link that you can use to complete your order'),
                        QuickReply(
                            text='Before I can do that, I need to know if would you like me to send your receipt to {0}?'.format(
                                last_email_address),
                            options=[
                                {'title': 'Yes', 'payload': 'yes'},
                                {'title': 'No', 'payload': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='Would you like me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                last_email_address))
                    ]
                },
                'email_address': get_email_address_content(),
                'do_confirm': {
                    'messages': [
                        QuickReply(
                            text='Just to confirm, do you want to have your receipt sent to {0}?'.format(
                                email_address
                            ),
                            options=[
                                {'title': 'Yes', 'payload': 'yes'},
                                {'title': 'No', 'payload': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='Just to confirm, do you want me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                email_address))
                    ]
                },
            },
            Context.ERROR: {
                'wei_charge': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        QuickReply(
                            text='Please use the options below to select how much would you like to spend on gas.',
                            options=[
                                {'title': '$5.00', 'payload': 5.00},
                                {'title': '$10.00', 'payload': 10.00},
                                {'title': '$25.00', 'payload': 25.00},
                                {'title': '$50.00', 'payload': 50.00},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(status='How much would you like to spend on gas?')
                    ]
                },
                'do_use_last_email_address': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        QuickReply(
                            text='Do you want me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                last_email_address),
                            options=[
                                {'title': 'Yes', 'metadata': 'yes'},
                                {'title': 'No', 'metadata': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(status='I don\'t understand 😕 Do you want me to send your receipt to {0}? '
                                      'Please answer with a "Yes" or "No"'.format(last_email_address))
                    ]
                },
                'email_address': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        Text('Please give me a valid email address (ex: johndoe@gmail.com) to send your receipt to.')
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand 😕 Please give me a valid email address (ex: johndoe@gmail.com) to send your receipt to.')
                    ]
                },
                'do_confirm': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        QuickReply(
                            text='Do you want to purchase $99.00 worth of gas and have your receipt sent to {0}? Please answer with a "Yes" or "No"'.format(
                                email_address
                            ),
                            options=[
                                {'title': 'Yes', 'metadata': 'yes'},
                                {'title': 'No', 'metadata': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand 😕 Do you want me to send your receipt to {0}? Please answer with a "Yes" or "No"'.format(
                                email_address))
                    ]
                }
            }
        }
        return content.get(context).get(slot_to_elicit)


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.create_elicit_slot_response')), name='create_elicit_slot_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.form_invalid')), name='form_invalid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.form_valid')), name='form_valid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_form_kwargs')), name='get_form_kwargs')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_fulfillment_content')), name='get_fulfillment_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_initial_data')), name='get_initial_data')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_slot_to_elicit_content')), name='get_slot_to_elicit_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'TransferTokensIntent.process_rejection')), name='process_rejection')
class TransferTokensIntent(FormIntent):
    intent_name = IntentName.TRANSFER_TOKENS.value
    form_class = TransferTokensForm
    fields = [
        'recipient',
        'num_tokens',
        'do_confirm'
    ]

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['platform'] = self.session.get('platform')
        return form_kwargs

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        user = self.get_user()
        initial_data.update({
            'sender': user.id
        })
        return initial_data

    def form_valid(self, form):
        logger.info('Form is valid')
        if form.is_fulfilled():
            logger.info('Form is fulfilled')
            form.transfer()

    def form_invalid(self, form):
        pass

    def get_fulfillment_content(self, form):
        assert form.is_valid()
        recipient = form.cleaned_data.get('recipient')
        return {
            'messages': [
                Text('Done! I will let you now when @{0} receives the tokens 💸'.format(recipient)),
            ],
            'statuses': [
                Status(
                    status='Done! I will let you now when @{0} receives the tokens 💸'.format(recipient)
                )
            ]
        }

    def get_failure_content(self, form):
        return {
            'messages': [
                Text('You are running low on gas ⚠️ You will need to buy some more so that I can do the transfer.'),
                Text('Try buying some more by saying something like "Buy more gas"')
            ],
            'statuses': [
                Status(
                    status='You are running low on gas ⚠️ You will need to buy some more so that I can do the transfer. Try buying some more by saying something like "Buy more gas"'
                )
            ]
        }

    def get_slot_to_elicit_content(self, *, slot_to_elicit, context=Context.DEFAULT):
        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        num_tokens = to_decimal(slots.get('num_tokens'))
        recipient = slots.get('recipient')
        token_balance = get_token_balance(account=self.get_account(do_import_raw_key=False))
        content = {
            Context.DEFAULT: {
                'recipient': {
                    'messages': [
                        Text('Who would you like to send {0} tokens to?'.format(ERC20_TOKEN_SYMBOL))
                    ],
                    'statuses': [
                        Status(status='Who would you like to send {0} tokens to?'.format(ERC20_TOKEN_SYMBOL))
                    ]
                },
                'num_tokens': {
                    'messages': [
                        Text('How many {0} tokens would you like to send to {1}?'.format(ERC20_TOKEN_SYMBOL, recipient))
                    ],
                    'statuses': [
                        Status(status='How many {0} tokens would you like to send to {1}?'.format(ERC20_TOKEN_SYMBOL,
                                                                                                  recipient))
                    ]
                },
                'do_confirm': {
                    'messages': [
                        QuickReply(
                            text='Just to confirm, do you want to send {0} {1} tokens to {2}?'.format(
                                format_decimal(num_tokens),
                                ERC20_TOKEN_SYMBOL,
                                recipient
                            ),
                            options=[
                                {'title': 'Yes', 'payload': 'yes'},
                                {'title': 'No', 'payload': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(status='Just to confirm, do you want to send {0} {1} tokens to {2}? '
                                      'Please answer with a "Yes" or "No"'.format(format_decimal(num_tokens),
                                                                                  ERC20_TOKEN_SYMBOL,
                                                                                  recipient))
                    ]
                }
            },
            Context.ERROR: {
                'recipient': {
                    'messages': [
                        Text('I do not understand that'),
                        Text('Please use a valid Twitter handle (ex: @iamcardib)'),
                    ],
                    'statuses': [
                        Status(
                            status='I do not understand that. Please use a valid Twitter handle (ex: @iamcardib)'
                        )
                    ]
                },
                'num_tokens': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        Text(
                            'Please use a whole number for the amount of {0} tokens that you would like to send to {1}'.format(
                                ERC20_TOKEN_SYMBOL, recipient)),
                        Text(
                            'Keep in mind, the amount of tokens you send must be less than your current balance of {0} {1} tokens'.format(
                                token_balance, ERC20_TOKEN_SYMBOL)),
                        Text('How many {0} tokens would you like to send to {1}?'.format(ERC20_TOKEN_SYMBOL, recipient))
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand. Please use a whole number for the amount of {0} tokens '
                                   'that you would like to send to {1}. Keep in mind, the amount of tokens you send must '
                                   'be less than your current balance of {2} {0} tokens'.format(ERC20_TOKEN_SYMBOL,
                                                                                                recipient,
                                                                                                token_balance))
                    ]
                },
                'do_confirm': {
                    'messages': [
                        Text('I don\'t understand that'),
                        QuickReply(
                            text='Do you want to send {0} {1} tokens to {2}? Please answer with a "Yes" or "No"'.format(
                                format_decimal(num_tokens),
                                ERC20_TOKEN_SYMBOL,
                                recipient
                            ),
                            options=[
                                {'title': 'Yes', 'metadata': 'yes'},
                                {'title': 'No', 'metadata': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(status='I don\'t understand. Do you want to send {0} {1} tokens to {2}? '
                                      'Please answer with a "Yes" or "No"'.format(format_decimal(num_tokens),
                                                                                  ERC20_TOKEN_SYMBOL,
                                                                                  recipient))
                    ]
                }
            }
        }
        return content.get(context).get(slot_to_elicit)


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelloIntent.process_rejection')), name='process_rejection')
class HelloIntent(Intent):
    intent_name = IntentName.HELLO.value

    def process_fulfillment(self):
        content = {
            'statuses': [
                Status(status='👋 Hello 😊 I\'m {0}. My job is to help you out with purchasing {1} tokens'.format(
                    settings.BOT_DISPLAY_NAME, settings.ERC20_TOKEN_SYMBOL))
            ],
            'messages': [
                Text('👋 Hello 😊'),
                Text('I\'m {0} 😍'.format(settings.BOT_DISPLAY_NAME)),
                Text('My job is to help you out with purchasing and transferring {0} tokens'.format(
                    settings.ERC20_TOKEN_SYMBOL)),
                Text('To get started'),
                Text('Try saying something like:\n{0}'.format(get_commands_text())),
                Text('You can also say something like "Cancel" or "Quit" if you need me to stop 🛑 doing anything'),
                Text('Say "Help" if you ever need a refresher on my commands')
            ]
        }
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED,
            content=content
        )


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetTokenBalanceIntent.process_rejection')), name='process_rejection')
class GetTokenBalanceIntent(Intent):
    intent_name = IntentName.GET_TOKEN_BALANCE.value
    is_sensitive = True

    def process_fulfillment(self):
        account = self.get_account(do_import_raw_key=False)
        balance = get_token_balance(account=account)
        text = random.choice([
            'You have {0} {1} tokens'.format(balance, settings.ERC20_TOKEN_SYMBOL),
            'Your account has {0} {1} tokens'.format(balance, settings.ERC20_TOKEN_SYMBOL),
            'There are {0} {1} tokens in your account'.format(balance, settings.ERC20_TOKEN_SYMBOL),
        ])
        content = {
            'statuses': [
                Status(status=self.get_welcome_status())
            ],
            'messages': [
                Text(text),
                Text(
                    'Feel free geek out 🤓 on the state of your account within Ethereum', buttons=[
                        UrlButton(label='View My Account', url=get_etherscan_url(address=self.get_account().address))
                    ]
                ),
            ]
        }
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED,
            content=content
        )


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'GetWeiBalanceIntent.process_rejection')), name='process_rejection')
class GetWeiBalanceIntent(Intent):
    intent_name = IntentName.GET_WEI_BALANCE.value
    is_sensitive = True

    def process_fulfillment(self):
        account = self.get_account(do_import_raw_key=False)
        ether_balance = get_ether_balance(account=account)
        num_transactions = get_estimated_num_transactions(account=account)
        gas_text = random.choice([
            'You have {0} ETH'.format(ether_balance),
            'Your account has {0} ETH'.format(ether_balance),
            'There is {0} ETH in your account'.format(ether_balance),
            'You have {0} ETH'.format(ether_balance),
            'Your account has {0} ETH'.format(ether_balance),
            'There is {0} ETH in your account'.format(ether_balance),
        ])
        if num_transactions > settings.MIN_ESTIMATED_NUM_TRANSACTIONS:
            num_transactions_text = 'That should be enough fuel ⛽ for about {0} transactions'.format(num_transactions)
        else:
            num_transactions_text = 'You are running low on gas ⚠️ Try buying some more by saying something like "Buy more gas"'.format(
                num_transactions)
        content = {
            'statuses': [
                Status(status='{0}. {1}'.format(gas_text, num_transactions_text))
            ],
            'messages': [
                Text(gas_text),
                Text(num_transactions_text),
                Text(
                    'Feel free geek out 🤓 on the state of your account within Ethereum', buttons=[
                        UrlButton(label='View My Account', url=get_etherscan_url(address=self.get_account().address))
                    ]
                ),
            ]
        }

        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED,
            content=content
        )


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HowItWorksIntent.process_rejection')), name='process_rejection')
class HowItWorksIntent(Intent):
    intent_name = IntentName.HOW_IT_WORKS.value

    def process_fulfillment(self):
        media_url = get_absolute_static_url('vid/explainer_0.mp4')
        content = {
            'statuses': [
                Status(status='Check out this video 📼 to see how {0}Token works'.format(
                    settings.ERC20_TOKEN_SYMBOL
                ), media_url=media_url)
            ],
            'messages': [
                Text('Check out this video 📼 to see how {0}Token works'.format(settings.ERC20_TOKEN_SYMBOL)),
                Attachment(category='video', url=media_url)
            ]
        }
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED,
            content=content
        )


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'CancelIntent.process_rejection')), name='process_rejection')
class CancelIntent(Intent):
    intent_name = IntentName.CANCEL.value

    def process_fulfillment(self):
        self.session.pop('intents')
        self.session.save()
        content = {
            'statuses': [
                Status(status='I will stop 🛑 Let me know if there is anything else I can do for you')
            ],
            'messages': [
                Text('Okay okay'),
                Text('I will stop 🛑'),
                Text('Let me know if there is anything else I can do for you'),
            ]
        }
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED,
            content=content
        )


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'HelpIntent.process_rejection')), name='process_rejection')
class HelpIntent(Intent):
    intent_name = IntentName.HELP.value

    def process_fulfillment(self):
        self.session.pop('intents')
        self.session.save()
        content = {
            'statuses': [
                Status(status='Try saying something like:\n{0}\nYou can also say something like "Cancel" or '
                              '"Quit" if you need me to stop 🛑 doing anything'.format(get_commands_text()))
            ],
            'messages': [
                Text('Try saying something like:\n{0}'.format(get_commands_text())),
                Text('You can also say something like "Cancel" or "Quit" if you need me to stop 🛑 doing anything'),
            ]
        }
        return self.create_close_response(
            fulfillment_state=FulfillmentState.FULFILLED,
            content=content
        )


@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.create_close_response')), name='create_close_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.create_elicit_slot_response')), name='create_elicit_slot_response')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.form_invalid')), name='form_invalid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.form_valid')), name='form_valid')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_account')), name='get_account')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_failure_content')), name='get_failure_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_form_kwargs')), name='get_form_kwargs')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_fulfillment_content')), name='get_fulfillment_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_initial_data')), name='get_initial_data')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_slot_to_elicit_content')), name='get_slot_to_elicit_content')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.get_user')), name='get_user')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.process_dialog')), name='process_dialog')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.process_event')), name='process_event')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.process_fulfillment')), name='process_fulfillment')
@method_decorator(xray_recorder.capture('{0}.{1}'.format(__name__, 'WithdrawTokensIntent.process_rejection')), name='process_rejection')
class WithdrawTokensIntent(FormIntent):
    intent_name = IntentName.WITHDRAW_TOKENS.value
    form_class = WithdrawTokensForm
    fields = [
        'num_tokens',
        'to_address',
        'do_confirm'
    ]
    is_sensitive = True

    def get_initial_data(self):
        initial_data = super().get_initial_data()
        user = self.get_user()
        initial_data.update({
            'user': user.id
        })
        return initial_data

    def form_valid(self, form):
        if form.is_fulfilled():
            form.withdraw()

    def form_invalid(self, form):
        pass

    def get_fulfillment_content(self, form):
        assert form.is_valid()
        return {
            'messages': [
                Text('Done! I will let you now when those tokens hit that account 💸'),
            ],
            'statuses': [
                Status(
                    status='Done! I will let you now when those tokens hit that account 💸'
                )
            ]
        }

    def get_failure_content(self, form):
        if not form.cleaned_data.get('do_confirm'):
            return {
                'messages': [
                    Text('No problem 👌'),
                    Text('I will not execute that transaction'),
                ],
                'statuses': [
                    Status(
                        status='No problem 👌 I will not execute that transaction'
                    )
                ]
            }
        return {
            'messages': [
                Text('You are running low on gas ⚠️ You will need to buy some more so that I can do the transfer.'),
                Text('Try buying some more by saying something like "Buy more gas"')
            ],
            'statuses': [
                Status(
                    status='You are running low on gas ⚠️ You will need to buy some more so that I can do the transfer. Try buying some more by saying something like "Buy more gas"'
                )
            ]
        }

    def get_slot_to_elicit_content(self, *, slot_to_elicit, context=Context.DEFAULT):
        slots = to_snake_case(self.event.get('currentIntent').get('slots'))
        to_address = slots.get('to_address')
        num_tokens = to_decimal(slots.get('num_tokens'))
        token_balance = get_token_balance(account=self.get_account(do_import_raw_key=False))
        content = {
            Context.DEFAULT: {
                'to_address': {
                    'messages': [
                        Text('Which address would you like to send the tokens to?')
                    ],
                    'statuses': [
                        Status(status='Which address would you like to send the tokens to?')
                    ]
                },
                'num_tokens': {
                    'messages': [
                        Text('How many {0} tokens would you like to withdraw?'.format(ERC20_TOKEN_SYMBOL))
                    ],
                    'statuses': [
                        Status(status='How many {0} tokens would you like to withdraw?'.format(ERC20_TOKEN_SYMBOL))
                    ]
                },
                'do_confirm': {
                    'messages': [
                        QuickReply(
                            text='Just to confirm, do you want to withdraw {0} {1} tokens and have them sent to {2}?'.format(
                                format_decimal(num_tokens),
                                ERC20_TOKEN_SYMBOL,
                                to_address
                            ),
                            options=[
                                {'title': 'Yes', 'payload': 'yes'},
                                {'title': 'No', 'payload': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='Just to confirm, do you want to withdraw {0} {1} tokens and have them sent to {2}? '
                                   'Please answer with a "Yes" or "No"'.format(num_tokens, ERC20_TOKEN_SYMBOL,
                                                                               to_address))
                    ]
                }
            },
            Context.ERROR: {
                'to_address': {
                    'messages': [
                        Text('I do not understand that'),
                        Text(
                            'Please use a valid Ethereum account address (ex: c2d7cf95645d33006175b78989035c7c9061d3f9)'),
                    ],
                    'statuses': [
                        Status(
                            status='I do not understand that. Please use a valid Please use a valid Ethereum account address (ex: c2d7cf95645d33006175b78989035c7c9061d3f9)'
                        )
                    ]
                },
                'num_tokens': {
                    'messages': [
                        Text('I don\'t understand that 😕'),
                        Text(
                            'Please use a whole number for the amount of {0} tokens that you would like to withdraw'.format(
                                ERC20_TOKEN_SYMBOL)),
                        Text(
                            'Keep in mind, the amount of tokens you withdraw must be less than your current balance of {0} {1} tokens'.format(
                                token_balance, ERC20_TOKEN_SYMBOL)),
                        Text('How many {0} tokens would you like to withdraw?'.format(ERC20_TOKEN_SYMBOL))
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand. Please use a whole number for the amount of {0} tokens '
                                   'that you would like to withdraw. Keep in mind, the amount of tokens you withdraw must '
                                   'be less than your current balance of {1} {0} tokens'.format(ERC20_TOKEN_SYMBOL,
                                                                                                token_balance))
                    ]
                },
                'do_confirm': {
                    'messages': [
                        Text('I don\'t understand that'),
                        QuickReply(
                            text='Do you want to withdraw {0} {1} tokens and have them sent to {2}? Please answer with a "Yes" or "No"'.format(
                                format_decimal(num_tokens),
                                ERC20_TOKEN_SYMBOL,
                                to_address
                            ),
                            options=[
                                {'title': 'Yes', 'metadata': 'yes'},
                                {'title': 'No', 'metadata': 'no'},
                            ]
                        )
                    ],
                    'statuses': [
                        Status(
                            status='I don\'t understand. Do you want to withdraw {0} {1} tokens and have them sent to {2}? '
                                   'Please answer with a "Yes" or "No"'.format(format_decimal(num_tokens),
                                                                               ERC20_TOKEN_SYMBOL,
                                                                               to_address))
                    ]
                }
            }
        }
        return content.get(context).get(slot_to_elicit)

# class WithdrawWeiIntent(FormIntent):
#     intent_name = IntentName.WITHDRAW_WEI.value
#     form_class = WithdrawWeiForm
#     fields = [
#         'num_wei',
#         'to_address',
#         'do_confirm'
#     ]
#     is_sensitive = True
#
#     def get_initial_data(self):
#         initial_data = super().get_initial_data()
#         user = self.get_user()
#         initial_data.update({
#             'user': user.id
#         })
#         return initial_data
#
#     def form_valid(self, form):
#         if form.is_fulfilled():
#             form.withdraw()
#
#     def form_invalid(self, form):
#         pass
#
#     def get_fulfillment_content(self, form):
#         assert form.is_valid()
#         return {
#             'messages': [
#                 Text('Done! I will let you now when those wei hit your account 💸'),
#             ],
#             'statuses': [
#                 Status(
#                     status='Done! I will let you now when those wei hit your account 💸'
#                 )
#             ]
#         }
#
#     def get_failure_content(self, form):
#         if not form.cleaned_data.get('do_confirm'):
#             return {
#                 'messages': [
#                     Text('No problem 👌'),
#                     Text('I will not execute that transaction'),
#                 ],
#                 'statuses': [
#                     Status(
#                         status='No problem 👌 I will not execute that transaction'
#                     )
#                 ]
#             }
#         return {
#             'messages': [
#                 Text('You are running low on gas ⚠️ You will need to buy some more so that I can do the transfer.'),
#                 Text('Try buying some more by saying something like "Buy more gas"')
#             ],
#             'statuses': [
#                 Status(
#                     status='You are running low on gas ⚠️ You will need to buy some more so that I can do the transfer. Try buying some more by saying something like "Buy more gas"'
#                 )
#             ]
#         }
#
#     def get_slot_to_elicit_content(self, *, slot_to_elicit, context=Context.DEFAULT):
#         slots = to_snake_case(self.event.get('currentIntent').get('slots'))
#         to_address = slots.get('to_address')
#         num_wei = slots.get('num_wei')
#         token_balance = get_token_balance(account=self.get_account())
#         content = {
#             Context.DEFAULT: {
#                 'to_address': {
#                     'messages': [
#                         Text('Which address you like to send the wei to?')
#                     ],
#                     'statuses': [
#                         Status(status='Which address you like to send the wei to?')
#                     ]
#                 },
#                 'num_wei': {
#                     'messages': [
#                         Text('How many {0} wei would you like to withdraw?'.format(ERC20_TOKEN_SYMBOL))
#                     ],
#                     'statuses': [
#                         Status(status='How many {0} wei would you like to withdraw?'.format(ERC20_TOKEN_SYMBOL))
#                     ]
#                 },
#                 'do_confirm': {
#                     'messages': [
#                         QuickReply(
#                             text='Just to confirm, do you want to withdraw {0} {1} wei and have them sent to {2}?'.format(
#                                 num_wei,
#                                 ERC20_TOKEN_SYMBOL,
#                                 to_address
#                             ),
#                             options=[
#                                 {'title': 'Yes', 'payload': 'yes'},
#                                 {'title': 'No', 'payload': 'no'},
#                             ]
#                         )
#                     ],
#                     'statuses': [
#                         Status(status='Just to confirm, do you want to withdraw {0} {1} wei and have them sent to {2}? '
#                                       'Please answer with a "Yes" or "No"'.format(num_wei, ERC20_TOKEN_SYMBOL,
#                                                                                   to_address))
#                     ]
#                 }
#             },
#             Context.ERROR: {
#                 'to_address': {
#                     'messages': [
#                         Text('I do not understand that'),
#                         Text('Please use a valid Ethereum account address (ex: c2d7cf95645d33006175b78989035c7c9061d3f9)'),
#                     ],
#                     'statuses': [
#                         Status(
#                             status='I do not understand that. Please use a valid Please use a valid Ethereum account address (ex: c2d7cf95645d33006175b78989035c7c9061d3f9)'
#                         )
#                     ]
#                 },
#                 'num_wei': {
#                     'messages': [
#                         Text('I don\'t understand that 😕'),
#                         Text('Please use a whole number for the amount of {0} wei that you would like to withdraw'.format(ERC20_TOKEN_SYMBOL)),
#                         Text(
#                             'Keep in mind, the amount of wei you withdraw must be less than your current balance of {0} {1} wei'.format(
#                                 token_balance, ERC20_TOKEN_SYMBOL)),
#                         Text('How many {0} wei would you like to withdraw?'.format(ERC20_TOKEN_SYMBOL))
#                     ],
#                     'statuses': [
#                         Status(
#                             status='I don\'t understand. Please use a whole number for the amount of {0} wei '
#                                    'that you would like to withdraw. Keep in mind, the amount of wei you withdraw must '
#                                    'be less than your current balance of {1} {0} wei'.format(ERC20_TOKEN_SYMBOL,
#                                                                                                 token_balance))
#                     ]
#                 },
#                 'do_confirm': {
#                     'messages': [
#                         Text('I don\'t understand that'),
#                         QuickReply(
#                             text='Do you want to withdraw {0} {1} wei and have them sent to {2}? Please answer with a "Yes" or "No"'.format(
#                                 num_wei,
#                                 ERC20_TOKEN_SYMBOL,
#                                 to_address
#                             ),
#                             options=[
#                                 {'title': 'Yes', 'metadata': 'yes'},
#                                 {'title': 'No', 'metadata': 'no'},
#                             ]
#                         )
#                     ],
#                     'statuses': [
#                         Status(status='I don\'t understand. Do you want to withdraw {0} {1} wei and have them sent to {2}? '
#                                       'Please answer with a "Yes" or "No"'.format(num_wei, ERC20_TOKEN_SYMBOL,
#                                                                                   to_address))
#                     ]
#                 }
#             }
#         }
#         return content.get(context).get(slot_to_elicit)
