from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from importlib import import_module

from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import signing
from django.core.exceptions import SuspiciousOperation
from rest_framework.response import Response
from rest_framework.views import APIView

from .enums import IntentName
from .intents import CreateTokenPurchaseOrderIntent, HelloIntent, TransferTokensIntent, \
    HelpIntent, CancelIntent, GetTokenBalanceIntent, HowItWorksIntent, GetWeiBalanceIntent, \
    CreateWeiPurchaseOrderIntent, WithdrawTokensIntent, create_elicit_intent_response

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
User = get_user_model()

logger = logging.getLogger(__name__)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_event'))
def process_event(event):
    SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
    intent_name = event.get('currentIntent').get('name')
    logger.debug('intent_name = {0}'.format(intent_name))

    # First grab the user's session
    session_attributes = event.get('sessionAttributes')
    if not session_attributes:
        session_attributes = {}
    session_key = session_attributes.get('key')
    if not session_key:
        raise Exception('Failed to find session key')

    session = SessionStore(session_key)
    if 'intents' not in session:
        session['intents'] = {}
    if intent_name not in session['intents']:
        session['intents'][intent_name] = {
            'num_events': 0
        }
    if 'num_events' not in session['intents'][intent_name]:
        session['intents'][intent_name]['num_events'] = 0
    session['intents'][intent_name]['num_events'] += 1
    session.save()
    logger.debug('Found existing session for %s' % event.get('userId'))

    # LOG!
    user = User.objects.get(id=session['user_id'])
    logger.info('user = {0}'.format(user))
    logger.info('twitter_screen_name = {0}'.format(session.get('twitter_screen_name')))
    logger.info('twitter_id = {0}'.format(session.get('twitter_id')))
    logger.info('event = {0}'.format(json.dumps(event, indent=2)))
    logger.info('session = {0}'.format(json.dumps(session._session, indent=2)))

    # Choose an intent
    intents = {
        IntentName.HELLO.value: lambda: HelloIntent(session=session),
        IntentName.CREATE_WEI_PURCHASE_ORDER.value: lambda: CreateWeiPurchaseOrderIntent(session=session),
        IntentName.CREATE_TOKEN_PURCHASE_ORDER.value: lambda: CreateTokenPurchaseOrderIntent(session=session),
        IntentName.TRANSFER_TOKENS.value: lambda: TransferTokensIntent(session=session),
        IntentName.GET_WEI_BALANCE.value: lambda: GetWeiBalanceIntent(session=session),
        IntentName.GET_TOKEN_BALANCE.value: lambda: GetTokenBalanceIntent(session=session),
        IntentName.CANCEL.value: lambda: CancelIntent(session=session),
        IntentName.HELP.value: lambda: HelpIntent(session=session),
        IntentName.HOW_IT_WORKS.value: lambda: HowItWorksIntent(session=session),
        IntentName.WITHDRAW_TOKENS.value: lambda: WithdrawTokensIntent(session=session),
        IntentName.WITHDRAW_WEI.value: lambda: WithdrawTokensIntent(session=session)
    }
    if intent_name in intents:
        intent = intents.get(intent_name)()
        response = intent.process_event(event=event)
    else:
        response = create_elicit_intent_response(event=event)
    logger.info('response = {0}'.format(json.dumps(response, indent=2)))
    return response


class LexWebhookView(APIView):
    http_method_names = ['post']

    def post(self, request, format=None):
        logger.info('Processing Lex web hook\n{0}'.format(json.dumps(request.data, indent=2)))
        try:
            event = signing.loads(request.data.get('event'), key=settings.LEX_WEBHOOK_SECRET_KEY)
        except signing.BadSignature:
            logger.exception('Invalid Lex Event')
            raise SuspiciousOperation

        if event.get('requestAttributes'):
            current_segment = xray_recorder.current_segment()
            trace_id = event.get('requestAttributes', {}).get('traceId',
                                                              current_segment.trace_id if current_segment else None)
            if trace_id:
                xray_recorder.begin_segment('{0}.{1}'.format('web', settings.SITE_DOMAIN), traceid=trace_id)

        response = process_event(event)

        xray_recorder.current_segment().put_metadata('event', event)
        xray_recorder.current_segment().put_metadata('response', response)

        return Response(response, content_type='application/json')
