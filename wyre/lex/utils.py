from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
import pickle

import base64
import boto3
import copy
import re
from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from django.utils import timezone

from wyre.utils import pluralize

logger = logging.getLogger(__name__)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'to_pickle_string'))
def to_pickle_string(content):
    return base64.b64encode(pickle.dumps(content)).decode()


@xray_recorder.capture('{0}.{1}'.format(__name__, 'from_pickle_string'))
def from_pickle_string(content):
    return pickle.loads(base64.b64decode(content))


@xray_recorder.capture('{0}.{1}'.format(__name__, 'verbose_timedelta'))
def verbose_timedelta(start_date, end_date=None):
    if end_date is None:
        end_date = timezone.now()
    delta = end_date - start_date
    seconds = int(abs(delta.total_seconds()))
    days = int(seconds / (60 * 60 * 24))
    hours = int(seconds / (60 * 60))
    minutes = int(seconds / 60)
    if days > 0:
        text = '%d %s ago' % (days, pluralize('day', days))
    elif hours > 0:
        text = '%d %s ago' % (hours, pluralize('hour', hours))
    elif minutes > 0:
        text = '%d %s ago' % (minutes, pluralize('minute', minutes))
    else:
        text = '%d %s ago' % (seconds, pluralize('second', seconds))
    return text


@xray_recorder.capture('{0}.{1}'.format(__name__, 'to_snake_case'))
def to_snake_case(obj, mapping=None):
    if isinstance(obj, str):
        text = copy.deepcopy(obj)
        if text == 'idleSessionTTLInSeconds':
            return 'idle_session_ttl_in_seconds'
        if mapping and text in mapping:
            return mapping.get(text)
        for substring in re.compile('[A-Z]').findall(obj):
            text = text.replace(substring, '_' + substring[0].lower())
        return text
    elif isinstance(obj, dict):
        keys_to_remove = obj.keys()
        keys_to_keep = []
        temp_dict = copy.deepcopy(obj)
        for k, v in temp_dict.items():
            key_to_keep = to_snake_case(k)
            obj[key_to_keep] = v
            keys_to_keep.append(key_to_keep)
        for k in [k for k in keys_to_remove if k not in keys_to_keep]:
            del obj[k]
        return obj


@xray_recorder.capture('{0}.{1}'.format(__name__, 'to_camel_case'))
def to_camel_case(obj, mapping=None):
    if isinstance(obj, str):
        camel_case_text = copy.deepcopy(obj)
        if camel_case_text == 'idle_session_ttl_in_seconds':
            return 'idleSessionTTLInSeconds'
        if mapping and camel_case_text in mapping:
            return mapping.get(camel_case_text)
        for substring in re.compile('_[A-Za-z0-9]').findall(obj):
            camel_case_text = camel_case_text.replace(substring, substring[-1].upper())
        return camel_case_text
    elif isinstance(obj, dict):
        keys_to_remove = obj.keys()
        keys_to_keep = []
        temp_dict = copy.deepcopy(obj)
        for k, v in temp_dict.items():
            key_to_keep = to_camel_case(k)
            obj[key_to_keep] = v
            keys_to_keep.append(key_to_keep)
        for k in [k for k in keys_to_remove if k not in keys_to_keep]:
            del obj[k]
        return obj


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_chunks'))
def get_chunks(text, chunk_size):
    words = text.split()
    chunks = []
    last_chunk = []
    for i, word in enumerate(words):
        chunk = copy.deepcopy(last_chunk)
        chunk.append(word)
        if len(' '.join(chunk)) < chunk_size:
            last_chunk = copy.deepcopy(chunk)
        else:
            chunks.append(' '.join(last_chunk))
            last_chunk = [word]
    if last_chunk:
        chunks.append(' '.join(last_chunk))
    return chunks


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_lex_response'))
def get_lex_response(input_transcript, user, session, platform, is_public_channel, trace_id):
    from .enums import DialogActionType, DialogState
    from .intents import create_elicit_intent_response
    from .responses import Text, Status
    lex_client = boto3.client('lex-runtime')
    cleaned_input_transcript = input_transcript.lower().strip()
    logger.info('Sending transcript "{0}" to Lex'.format(cleaned_input_transcript))
    try:
        response = lex_client.post_text(
            botName=settings.AWS_LEX_BOT_NAME,
            botAlias=settings.AWS_LEX_BOT_ALIAS,
            userId='User:{0}'.format(user.id),
            inputText=cleaned_input_transcript,
            sessionAttributes={
                'key': session.session_key
            },
            requestAttributes={
                'platform': str(int(platform)),
                'isPublicChannel': str(is_public_channel),
                'traceId': trace_id
            }
        )
    except Exception:
        logger.exception('Failed to get Lex response')
        content = {
            'messages': [
                Text('Oops 😣'),
                Text('I just had a little hiccup processing that 🙃'),
                Text('Our engineers 🤓 have been notified'),
            ],
            'statuses': [
                Status(status='Oops 😣 I just had a little hiccup processing that 🙃 Our engineers 🤓 have been notified')
            ]
        }
        return {
            'message': to_pickle_string(content)
        }
    try:
        logger.info('Lex response is\n{0}'.format(json.dumps(response, indent=2)))
    except TypeError:
        pass

    # Lex won't let me ElicitIntents...?
    event = response.copy()
    del event['ResponseMetadata']
    if event.get('dialogState') == DialogActionType.ELICIT_INTENT.value or (event.get('dialogState') == DialogState.FAILED and 'Buh Bye' in event.get('message')):
        r = create_elicit_intent_response(event=event, session=session)
        response['message'] = r.get('dialogAction').get('message').get('content')
        logger.info('Response is now\n{0}'.format(json.dumps(response, indent=2)))
    return response
