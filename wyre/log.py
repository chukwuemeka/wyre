from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import pickle

import hashlib
from django.conf import settings
from django.core.cache import caches
from django.utils.log import AdminEmailHandler as DjangoAdminEmailHandler


class RequireIsTestModeFalse(logging.Filter):
    def filter(self, record):
        return not settings.IS_TEST_MODE


class AdminEmailHandler(DjangoAdminEmailHandler):
    def emit(self, record):
        key = '{0}.{1}.{2}'.format(
            __name__,
            'AdminEmailHandler.emit',
            hashlib.sha256(pickle.dumps({
                'name': record.name,
                'levelno': record.levelno,
                'pathname': record.pathname,
                'lineno': record.lineno,
                'funcName': record.funcName
            })).hexdigest()
        )
        if caches['email'].get(key):
            return
        caches['email'].set(key, True, 900)
        super().emit(record)
