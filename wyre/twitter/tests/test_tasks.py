import uuid
from importlib import import_module
from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase

from ..tasks import process_incoming_direct_message
from ...profiles.models import Profile

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
Session = SessionStore().model


class MockBoto3Client(mock.MagicMock):
    def post_text(self, *args, **kwargs):
        return {
            'ResponseMetadata': {}
        }


class MockResponse(mock.MagicMock):
    def json(self):
        return {}

    @property
    def status_code(self):
        return 200


class ProcessDirectMessageTaskTestCase(TestCase):
    def setUp(self):
        self.patchers = [
            mock.patch('boto3.client', mock.MagicMock(return_value=MockBoto3Client())),
            mock.patch('tweepy.OAuthHandler', mock.MagicMock(return_value=mock.MagicMock())),
            mock.patch('tweepy.API', mock.MagicMock(return_value=mock.MagicMock())),
            mock.patch('pickle.loads', mock.MagicMock(return_value=mock.MagicMock())),
            mock.patch('base64.b64decode', mock.MagicMock(return_value=mock.MagicMock())),
            mock.patch('requests.get', mock.MagicMock(return_value=MockResponse())),
            mock.patch('requests.post', mock.MagicMock(return_value=MockResponse())),
            mock.patch('wyre.lex.responses.requests.get', mock.MagicMock(return_value=MockResponse())),
            mock.patch('wyre.lex.responses.requests.post', mock.MagicMock(return_value=MockResponse())),
        ]
        [p.start() for p in self.patchers]

    def tearDown(self):
        [p.stop() for p in self.patchers]
        Session.objects.all().delete()
        User.objects.all().delete()

    def test_user_created(self):
        self.assertFalse(User.objects.all().exists())
        twitter_id = '123'
        direct_message = {
            'id': uuid.uuid4().hex,
            'id_str': uuid.uuid4().hex,
            'sender_id_str': twitter_id,
            'sender_screen_name': 'megaman',
            'text': 'Hello',
            'recipient_id_str': settings.TWITTER_ID
        }
        process_incoming_direct_message(direct_message)
        self.assertEqual(User.objects.all().count(), 1)
        self.assertEqual(Profile.objects.filter(twitter_id=twitter_id).count(), 1)

    def test_session_created(self):
        self.assertFalse(Session.objects.all().exists())
        twitter_id = '123'
        direct_message = {
            'id': uuid.uuid4().hex,
            'id_str': uuid.uuid4().hex,
            'sender_id_str': twitter_id,
            'sender_screen_name': 'megaman',
            'text': 'Hello',
            'recipient_id_str': settings.TWITTER_ID
        }
        process_incoming_direct_message(direct_message)
        self.assertEqual(Session.objects.all().count(), 1)
