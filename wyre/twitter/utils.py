from __future__ import division
from __future__ import print_function

import json
import logging
from time import sleep

import os
import requests
import tempfile
import twitter as official_twitter
from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from django.core.cache import cache
from requests_oauthlib import OAuth1
from twitter.error import (
    TwitterError,
)
from twitter.twitter_utils import (
    parse_media_file)

from ..decorators import memoize
from ..urlresolvers import get_absolute_static_url

logger = logging.getLogger(__name__)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_default_welcome_message_id'))
@memoize('{0}.{1}'.format(__name__, 'get_default_welcome_message_id', timeout=86400))
def get_default_welcome_message_id(text=None):
    text = text or '''
👋 Hello 😊

I'm Bae 😍

My job is to help you out with purchasing and transferring BAE tokens

To get started

Try saying something like:
"I want to buy some BAE"
"I want to buy some gas"
"Send BAE to @username"
"How many tokens do I have?"
"How much gas do I have?"
"How does this all work?"
"I want to withdraw my tokens"

You can also say something like "Cancel" or "Quit" if you need me to stop 🛑 doing anything

Say "Help" if you ever need a refresher on my commands
'''
    url = 'https://api.twitter.com/1.1/direct_messages/welcome_messages/new.json'
    auth = OAuth1(
        settings.TWITTER_PUBLIC_CONSUMER_KEY,
        settings.TWITTER_PRIVATE_CONSUMER_KEY,
        settings.TWITTER_PUBLIC_ACCESS_KEY,
        settings.TWITTER_PRIVATE_ACCESS_KEY
    )
    media_id = get_media_id(
        get_absolute_static_url('vid/explainer_1.mp4'),
        settings.TWITTER_PUBLIC_CONSUMER_KEY,
        settings.TWITTER_PRIVATE_CONSUMER_KEY,
        settings.TWITTER_PUBLIC_ACCESS_KEY,
        settings.TWITTER_PRIVATE_ACCESS_KEY
    )
    response = requests.post(url, auth=auth, json={
        'welcome_message': {
            'message_data': {
                'attachment': {
                    'type': 'media',
                    'media': {
                        'id': media_id
                    }
                },
                'text': text,
                'quick_reply': {
                    'type': 'options',
                    'options': [
                        {
                            'label': 'I want to buy some BAE',
                        },
                        {
                            'label': 'I would like to send BAE',
                        },
                        {
                            'label': 'I need to buy gas',
                        },
                        {
                            'label': 'How does this all work?',
                        },
                    ]
                }
            }
        }
    })
    logger.info('response = {0}'.format(response))
    logger.info('response.content = {0}'.format(response.content))
    logger.info('response.json() = {0}'.format(response.json()))
    assert response.ok, 'Failed to create a welcome message'
    return response.json().get('welcome_message').get('id')


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_welcome_message_id'))
@memoize('{0}.{1}'.format(__name__, 'get_welcome_message_id', timeout=86400))
def get_welcome_message_id(
        *,
        text=None,
        public_consumer_key=settings.TWITTER_PUBLIC_CONSUMER_KEY,
        private_consumer_key=settings.TWITTER_PRIVATE_CONSUMER_KEY,
        public_access_key=settings.TWITTER_PUBLIC_ACCESS_KEY,
        private_access_key=settings.TWITTER_PRIVATE_ACCESS_KEY):
    assert public_consumer_key
    assert private_consumer_key
    assert public_access_key
    assert private_access_key
    assert text
    url = 'https://api.twitter.com/1.1/direct_messages/welcome_messages/new.json'
    auth = OAuth1(public_consumer_key, private_consumer_key, public_access_key, private_access_key)
    response = requests.post(url, auth=auth, json={
        'welcome_message': {
            'message_data': {
                'text': text
            }
        }
    })
    logger.info('response = {0}'.format(response))
    logger.info('response.content = {0}'.format(response.content))
    logger.info('response.json() = {0}'.format(response.json()))
    assert response.ok, 'Failed to create a welcome message'
    return response.json().get('welcome_message').get('id')


class Api(official_twitter.Api):
    def _UploadMediaChunkedInit(self,
                                media,
                                additional_owners=None,
                                media_category=None,
                                shared=None):
        """Start a chunked upload to Twitter.

        Args:
            media:
                File-like object to upload.
            additional_owners: additional Twitter users that are allowed to use
                The uploaded media. Should be a list of integers. Maximum
                number of additional owners is capped at 100 by Twitter.
            media_category:
                Category with which to identify media upload. Only use with Ads
                API & video files.

        Returns:
            tuple: media_id (returned from Twitter), file-handler object (i.e., has .read()
            method), filename media file.
        """
        url = '%s/media/upload.json' % self.upload_url

        media_fp, filename, file_size, media_type = parse_media_file(media)

        if not all([media_fp, filename, file_size, media_type]):
            raise TwitterError({'message': 'Could not process media file'})

        parameters = {}

        if additional_owners and len(additional_owners) > 100:
            raise TwitterError({'message': 'Maximum of 100 additional owners may be specified for a Media object'})
        if additional_owners:
            parameters['additional_owners'] = additional_owners
        if media_category:
            parameters['media_category'] = media_category

        # INIT doesn't read in any data. It's purpose is to prepare Twitter to
        # receive the content in APPEND requests.
        parameters['command'] = 'INIT'
        parameters['media_type'] = media_type
        parameters['total_bytes'] = file_size
        if shared is not None:
            parameters['shared'] = shared

        resp = self._RequestUrl(url, 'POST', data=parameters)
        data = self._ParseAndCheckTwitter(resp.content.decode('utf-8'))

        try:
            media_id = data['media_id']
        except KeyError:
            raise TwitterError({'message': 'Media could not be uploaded'})

        return (media_id, media_fp, filename)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'indicate_typing'))
def indicate_typing(recipient_id):
    assert recipient_id
    url = 'https://api.twitter.com/1.1/direct_messages/indicate_typing.json'
    auth = OAuth1(
        settings.TWITTER_PUBLIC_CONSUMER_KEY,
        settings.TWITTER_PRIVATE_CONSUMER_KEY,
        settings.TWITTER_PUBLIC_ACCESS_KEY,
        settings.TWITTER_PRIVATE_ACCESS_KEY
    )
    response = requests.post(url, auth=auth, data={
        'recipient_id': recipient_id
    })
    logger.info('response = {0}'.format(response))
    logger.info('response.content = {0}'.format(response.content))
    assert response.ok, 'Failed to indicate typing'


@xray_recorder.capture('{0}.{1}'.format(__name__, 'mark_read'))
def mark_read(message_id, recipient_id):
    assert message_id
    assert recipient_id
    url = 'https://api.twitter.com/1.1/direct_messages/mark_read.json'
    auth = OAuth1(
        settings.TWITTER_PUBLIC_CONSUMER_KEY,
        settings.TWITTER_PRIVATE_CONSUMER_KEY,
        settings.TWITTER_PUBLIC_ACCESS_KEY,
        settings.TWITTER_PRIVATE_ACCESS_KEY
    )
    response = requests.post(url, auth=auth, data={
        'last_read_event_id': message_id,
        'recipient_id': recipient_id
    })
    logger.info('response = {0}'.format(response))
    logger.info('response.request = {0}'.format(response.request))
    logger.info('response.request.headers = {0}'.format(response.request.headers))
    logger.info('response.request.body = {0}'.format(response.request.body))
    logger.info('response.content = {0}'.format(response.content))
    assert response.ok, 'Failed to mark as read'


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_media_id'))
def get_media_id(url, public_consumer_key, private_consumer_key, public_access_key, private_access_key,
                 is_direct_message_attachment=True):
    key = '{0}.{1}'.format(
        'wyre.lex.twitter.responses.get_media_id',
        [url, public_consumer_key, private_consumer_key, public_access_key, private_access_key,
         is_direct_message_attachment]
    )
    media_id = cache.get(key)
    if media_id:
        logger.debug('Hit from cache!')
        return media_id

    if is_direct_message_attachment:
        media_category = 'dm_image'
        if url.endswith('.mp4'):
            media_category = 'dm_video'
        elif url.endswith('.gif'):
            media_category = 'dm_gif'
    else:
        media_category = 'tweet_image'
        if url.endswith('.mp4'):
            media_category = 'tweet_video'
        elif url.endswith('.gif'):
            media_category = 'tweet_gif'

    api = Api(
        consumer_key=public_consumer_key,
        consumer_secret=private_consumer_key,
        access_token_key=public_access_key,
        access_token_secret=private_access_key
    )
    response = requests.get(url)
    logger.info('Uploading media {0} to Twitter'.format(url))
    temporary_file = tempfile.NamedTemporaryFile(suffix=os.path.splitext(url)[1])
    temporary_file.write(response.content)
    temporary_file.seek(0)

    shared = True if is_direct_message_attachment else None
    media_id, media_fp, filename = api._UploadMediaChunkedInit(media=temporary_file, media_category=media_category,
                                                               shared=shared)

    append = api._UploadMediaChunkedAppend(media_id=media_id,
                                           media_fp=media_fp,
                                           filename=filename)

    if not append:
        TwitterError('Media could not be uploaded.')

    data = api._UploadMediaChunkedFinalize(media_id)
    logger.info('data = {0}'.format(data))
    media_id = data.get('media_id')

    if data.get('processing_info'):
        for i in range(10):
            url = 'https://upload.twitter.com/1.1/media/upload.json?command=STATUS&media_id={0}'.format(media_id)
            logger.info('url = {0}'.format(url))
            auth = OAuth1(public_consumer_key, private_consumer_key, public_access_key, private_access_key)
            response = requests.get(url, auth=auth)
            logger.info('response.status_code = {0}'.format(response.status_code))
            logger.info('response = {0}'.format(json.dumps(response.json(), indent=2)))
            state = response.json().get('processing_info', {}).get('state', None)
            expires_after_secs = response.json().get('expires_after_secs', None)
            if state == 'succeeded':
                logger.info('Twitter finished parsing {0}'.format(url))
                cache.set(key, media_id, round(expires_after_secs * 0.95))
                break
            elif state == 'failed':
                logger.error('Twitter failed to parse {0}'.format(url))
                break
            sleep(1)
    else:
        expires_after_secs = data.get('expires_after_secs', None)
        cache.set(key, media_id, round(expires_after_secs * 0.95))

    return media_id
