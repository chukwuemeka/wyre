from __future__ import absolute_import

import logging

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from requests_oauthlib import OAuth1

from ...utils import get_default_welcome_message_id

User = get_user_model()

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        welcome_message_id = get_default_welcome_message_id()
        auth = OAuth1(
            settings.TWITTER_PUBLIC_CONSUMER_KEY,
            settings.TWITTER_PRIVATE_CONSUMER_KEY,
            settings.TWITTER_PUBLIC_ACCESS_KEY,
            settings.TWITTER_PRIVATE_ACCESS_KEY
        )

        # Get existing rules
        url = 'https://api.twitter.com/1.1/direct_messages/welcome_messages/rules/list.json'
        response = requests.get(url, auth=auth)
        print(response.content)
        assert response.ok, 'Failed to list welcome message rules'

        # Destroy existing rules
        for rule in response.json().get('welcome_message_rules'):
            url = 'https://api.twitter.com/1.1/direct_messages/welcome_messages/rules/destroy.json?id={}'.format(rule.get('id'))
            response = requests.delete(url, auth=auth)
            print(response.content)
            assert response.ok

        # Add a new rule
        url = 'https://api.twitter.com/1.1/direct_messages/welcome_messages/rules/new.json'
        response = requests.post(url, auth=auth, json={
            'welcome_message_rule': {
                'welcome_message_id': welcome_message_id
            }
        })
        print(response)
        print(response.status_code)
        print(response.content)
        assert response.ok, 'Failed to create a default welcome message'
