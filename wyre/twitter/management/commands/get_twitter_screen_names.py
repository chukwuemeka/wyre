from __future__ import absolute_import

import logging
import sys

import tweepy
import yaml
from django.conf import settings
from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class StreamListener(tweepy.StreamListener):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.screen_names = set()

    def on_status(self, status):
        status = status._json
        # logger.debug('Received tweet\n{0}'.format(json.dumps(status, indent=2)))
        self.screen_names.add(status.get('user').get('screen_name'))
        if len(self.screen_names) % 100 == 0:
            logger.debug('Loaded {0} screen names...'.format(len(self.screen_names)))
        if len(self.screen_names) >= 10000:
            logger.info(yaml.dump({
                'slot_types': {
                    'screen_name': {
                        'name': 'TwitterScreenName',
                        'enumeration_values': ['@{0}'.format(s) for s in self.screen_names]
                    }
                }
            }, default_flow_style=False))
            sys.exit(0)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('screen_name')

    def handle(self, *args, **options):
        auth_handler = tweepy.OAuthHandler(settings.TWITTER_PUBLIC_CONSUMER_KEY, settings.TWITTER_PRIVATE_CONSUMER_KEY)
        auth_handler.set_access_token(settings.TWITTER_PUBLIC_ACCESS_KEY, settings.TWITTER_PRIVATE_ACCESS_KEY)
        api = tweepy.API(auth_handler)
        stream_listener = StreamListener()
        stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
        logger.info('Opening Twitter stream for {0}'.format(settings.TWITTER_ID))
        stream.sample()
