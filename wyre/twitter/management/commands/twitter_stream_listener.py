from __future__ import absolute_import

import json
import logging

import tweepy
from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from wyre.twitter.tasks import process_incoming_direct_message
from wyre.twitter.tasks import process_incoming_status
from wyre.xray.decorators import trace
from ....decorators import log_exception

User = get_user_model()

logger = logging.getLogger(__name__)


class StreamListener(tweepy.StreamListener):
    @log_exception(logger)
    @trace('twitter_stream_listener')
    def on_status(self, status, trace_id=None):
        status = status._json
        logger.info('Received tweet\n{0}'.format(json.dumps(status, indent=2)))
        logger.info('trace_id = {0}'.format(trace_id))
        recipient_id = status.get('in_reply_to_user_id_str')
        sender_id = status.get('user').get('id_str')
        mentions = [u.get('id_str') for u in status.get('entities', {}).get('user_mentions', [])]
        if (recipient_id == settings.TWITTER_ID and sender_id != settings.TWITTER_ID) or \
                (settings.TWITTER_ID in mentions and not status.get('in_reply_to_status_id_str')):
            process_incoming_status.apply_async(
                args=(status,),
                kwargs=dict(trace_id=trace_id),
                MessageGroupId=sender_id,
                MessageDeduplicationId=status.get('id_str')
            )
            queryset = User.objects.filter(profile__twitter_id__iexact=sender_id)
            if queryset.exists():
                user = queryset.first()
                xray_recorder.get_trace_entity().set_user('User:{0}'.format(user.id))
        else:
            logger.info('Ignoring tweet sent to {0}'.format(recipient_id))
            xray_recorder.clear_trace_entities()

    @log_exception(logger)
    @trace('twitter_stream_listener')
    def on_direct_message(self, status, trace_id=None):
        direct_message = status._json.get('direct_message')
        logger.info('Received direct message\n{0}'.format(json.dumps(direct_message, indent=2)))
        logger.info('trace_id = {0}'.format(trace_id))
        recipient_id = direct_message.get('recipient_id_str')
        sender_id = direct_message.get('sender_id_str')
        if recipient_id == settings.TWITTER_ID and sender_id != settings.TWITTER_ID:
            process_incoming_direct_message.apply_async(
                args=(direct_message,),
                kwargs=dict(trace_id=trace_id),
                MessageGroupId=sender_id,
                MessageDeduplicationId=direct_message.get('id_str')
            )
            queryset = User.objects.filter(profile__twitter_id__iexact=sender_id)
            if queryset.exists():
                user = queryset.first()
                xray_recorder.get_trace_entity().set_user('User:{0}'.format(user.id))
        else:
            logger.info('Ignoring direct message sent to {0}'.format(recipient_id))
            xray_recorder.clear_trace_entities()


class Command(BaseCommand):
    def handle(self, *args, **options):
        auth_handler = tweepy.OAuthHandler(settings.TWITTER_PUBLIC_CONSUMER_KEY, settings.TWITTER_PRIVATE_CONSUMER_KEY)
        auth_handler.set_access_token(settings.TWITTER_PUBLIC_ACCESS_KEY, settings.TWITTER_PRIVATE_ACCESS_KEY)
        api = tweepy.API(auth_handler)
        stream_listener = StreamListener()
        stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
        logger.info('Opening Twitter stream for {0}'.format(settings.TWITTER_ID))
        stream.userstream()
