import json
import time
import uuid
from importlib import import_module

import billiard
import boto3
import random
import re
import tenacity
from aws_xray_sdk.core import xray_recorder
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.utils import timezone
from functools import partial

from wyre.lex.utils import get_lex_response
from wyre.xray.decorators import trace
from .utils import indicate_typing, mark_read
from ..enums import Platform
from ..ethereum.utils import get_or_create_account
from ..lex.enums import DialogState
from ..lex.utils import from_pickle_string
from ..profiles.models import Profile

logger = get_task_logger(__name__)

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
Session = SessionStore().model


@xray_recorder.capture('{0}.{1}'.format(__name__, 'login'))
def login(sender_id, sender_screen_name):
    queryset = Profile.objects.select_related('user').filter(twitter_id=sender_id)
    if not queryset.exists():
        user = User.objects.create_user(email_address='success+{0}@simulator.amazonses.com'.format(uuid.uuid4().hex))
        profile = Profile.objects.create(twitter_id=sender_id, twitter_screen_name=sender_screen_name,
                                         user=user)
        logger.info('Created user with twitter id {0}'.format(profile.twitter_id))
    else:
        profile = queryset.first()
        profile.twitter_screen_name = sender_screen_name
        profile.save()
        user = profile.user
        logger.info('Found existing user with twitter id {0}'.format(profile.twitter_id))

    # Create Ethereum account
    get_or_create_account(user)

    # Session
    key = 'wyre.twitter.tasks.login.session_key.{0}'.format(user.id)
    session_key = cache.get(key)
    if session_key:
        session = SessionStore(session_key)
        logger.info('Retrieved session with key {0}'.format(session.session_key))
    else:
        session = SessionStore()
        session.create()
        logger.info('Created session with key {0}'.format(session.session_key))
    cache.set(key, session.session_key, settings.LEX_SESSION_AGE)
    session.set_expiry(settings.LEX_SESSION_AGE)
    session['user_id'] = user.id
    session['platform'] = Platform.TWITTER
    session.save()

    xray_recorder.get_trace_entity().set_user('User:{0}'.format(user.id))

    return user, session


@xray_recorder.capture('{0}.{1}'.format(__name__, 'send_statuses'))
def send_statuses(statuses, in_reply_to_status_id, do_mock_reply=False):
    for i, status in enumerate(statuses):
        if i > 0:
            time.sleep(abs(random.gauss(0, 0.5)))
        status.post(
            in_reply_to_status_id=in_reply_to_status_id,
            public_consumer_key=settings.TWITTER_PUBLIC_CONSUMER_KEY,
            private_consumer_key=settings.TWITTER_PRIVATE_CONSUMER_KEY,
            public_access_key=settings.TWITTER_PUBLIC_ACCESS_KEY,
            private_access_key=settings.TWITTER_PRIVATE_ACCESS_KEY
        )


# @shared_task(queue='Wyre{0}TwitterOutbox.fifo'.format(settings.ENVIRONMENT.capitalize()))
# @trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'send_direct_messages'))
def send_direct_messages(
        messages,
        user,
        do_mock_reply=False):
    for i, message in enumerate(messages):
        if i > 0:
            time.sleep(abs(random.gauss(0, 0.5)))
        if do_mock_reply:
            logger.info('THIS IS A MOCK REPLY!!!!!!?!?!?')

            # class MockResponse(mock.MagicMock):
            #     def json(self):
            #         return {}
            #
            #     @property
            #     def status_code(self):
            #         return 200
            #
            # patchers = [
            #     mock.patch('requests.get', mock.MagicMock(return_value=MockResponse())),
            #     mock.patch('requests.post', mock.MagicMock(return_value=MockResponse())),
            #     mock.patch('wyre.lex.responses.requests.get', mock.MagicMock(return_value=MockResponse())),
            #     mock.patch('wyre.lex.responses.requests.post', mock.MagicMock(return_value=MockResponse())),
            #     mock.patch('tweepy.OAuthHandler', mock.MagicMock(return_value=mock.MagicMock())),
            #     mock.patch('tweepy.API', mock.MagicMock(return_value=mock.MagicMock()))
            # ]
            # [p.start() for p in patchers]
            #
            # message.to_twitter(
            #     recipient_id=user.profile.twitter_id,
            #     public_consumer_key=settings.TWITTER_PUBLIC_CONSUMER_KEY,
            #     private_consumer_key=settings.TWITTER_PRIVATE_CONSUMER_KEY,
            #     public_access_key=settings.TWITTER_PUBLIC_ACCESS_KEY,
            #     private_access_key=settings.TWITTER_PRIVATE_ACCESS_KEY
            # )
            #
            # [p.stop() for p in patchers]
        else:
            message.to_twitter(
                recipient_id=user.profile.twitter_id,
                public_consumer_key=settings.TWITTER_PUBLIC_CONSUMER_KEY,
                private_consumer_key=settings.TWITTER_PRIVATE_CONSUMER_KEY,
                public_access_key=settings.TWITTER_PUBLIC_ACCESS_KEY,
                private_access_key=settings.TWITTER_PRIVATE_ACCESS_KEY
            )


@shared_task(queue='Wyre{0}TwitterInbox.fifo'.format(settings.ENVIRONMENT.capitalize()))
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_incoming_status'))
def process_incoming_status(status, trace_id=None, do_mock_reply=None):
    logger.info('Processing status\n{0}'.format(status))
    logger.info('trace_id = {0}'.format(trace_id))

    sender_id = status.get('user').get('id_str')
    in_reply_to_status_id = status.get('id')
    mentions = [u.get('id_str') for u in status.get('entities', {}).get('user_mentions', [])]
    recipient_id = status.get('in_reply_to_user_id_str')
    is_mention = settings.TWITTER_ID in mentions and not recipient_id
    xray_recorder.current_subsegment().put_metadata('is_mention', is_mention)

    # Auth...?
    user, session = login(sender_id, status.get('user').get('screen_name'))
    xray_recorder.get_trace_entity().set_user('User:{0}'.format(user.id))

    # Twitter stuff
    session['twitter_sender_id'] = sender_id
    session['twitter_sender_screen_name'] = status.get('user').get('screen_name')
    session.save()

    # Build transcript and send to Lex
    # toks = status.get('text').replace('#', '').split()
    # filtered_toks = list(filter(lambda x: '@' not in x, toks))
    # if not filtered_toks:
    #     input_transcript = toks[-1]
    # else:
    #     input_transcript = ' '.join(filtered_toks)
    input_transcript = re.sub(r'(@{0}|#)'.format(settings.TWITTER_SCREEN_NAME), '', status.get('text'))
    # input_transcript = re.sub(r'(#)', '', status.get('text'))
    # input_transcript = status.get('text')
    logger.info('input_transcript = {0}'.format(input_transcript))
    lex_response = get_lex_response(input_transcript, user, session, Platform.TWITTER, True, trace_id)
    if is_mention and lex_response.get('dialogState') == DialogState.ELICIT_INTENT:
        logger.info('Ignoring tweet with a mention')
        return

    xray_recorder.current_subsegment().put_metadata('twitter_sender_id', sender_id)
    xray_recorder.current_subsegment().put_metadata('twitter_sender_screen_name', status.get('user').get('screen_name'))
    xray_recorder.current_subsegment().put_metadata('input_transcript', input_transcript)
    xray_recorder.current_subsegment().put_metadata('lex_response', lex_response)

    # Reply
    payload = from_pickle_string(lex_response.get('message'))
    logger.info('payload.keys = {0}'.format(payload.keys()))
    statuses = payload.get('statuses')
    assert statuses
    if do_mock_reply:
        payload = {
            'statuses': [s.to_json() for s in statuses],
        }
        client = boto3.client('sdb')
        client.put_attributes(
            DomainName='traces.staging.baetoken.com',
            ItemName=trace_id,
            Attributes=[
                {
                    'Name': 'payload',
                    'Value': json.dumps(payload)
                },
            ]
        )
    else:
        send_statuses(statuses, in_reply_to_status_id, do_mock_reply=do_mock_reply)


@shared_task(queue='Wyre{0}TwitterInbox.fifo'.format(settings.ENVIRONMENT.capitalize()))
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_incoming_direct_message'))
def process_incoming_direct_message(direct_message, trace_id=None, do_mock_reply=False):
    logger.info('Processing direct message\n{0}'.format(direct_message))
    logger.info('trace_id = {0}'.format(trace_id))
    start_time = timezone.now()
    sender_id = direct_message.get('sender_id_str')

    # Auth...?
    user, session = login(sender_id, direct_message.get('sender_screen_name'))
    xray_recorder.get_trace_entity().set_user('User:{0}'.format(user.id))

    # Twitter stuff
    session['twitter_sender_id'] = sender_id
    session['twitter_sender_screen_name'] = direct_message.get('sender_screen_name')
    session.save()

    # Got the message
    retrying = tenacity.Retrying(
        retry=tenacity.retry_always,
        wait=tenacity.wait_fixed(2)
    )
    indicate_typing_process = billiard.Process(target=retrying.call, args=(partial(indicate_typing, sender_id), ))
    if not do_mock_reply:
        mark_read(direct_message.get('id_str'), sender_id)
        indicate_typing_process.start()

    # Lex
    input_transcript = direct_message.get('text')
    lex_response = get_lex_response(input_transcript, user, session, Platform.TWITTER, False, trace_id)

    xray_recorder.current_subsegment().put_metadata('twitter_sender_id', sender_id)
    xray_recorder.current_subsegment().put_metadata(
        'twitter_sender_screen_name',
        direct_message.get('sender_screen_name')
    )
    xray_recorder.current_subsegment().put_metadata('input_transcript', input_transcript)
    xray_recorder.current_subsegment().put_metadata('lex_response', lex_response)

    # Reply
    messages = from_pickle_string(lex_response.get('message')).get('messages')
    end_time = timezone.now()

    if indicate_typing_process.is_alive():
        indicate_typing_process.terminate()

    if do_mock_reply:
        payload = {
            'messages': [m.to_json() for m in messages],
            'start_time': start_time.isoformat(),
            'end_time': end_time.isoformat(),
            'duration_in_milliseconds': (end_time - start_time).total_seconds() * 1000
        }
        client = boto3.client('sdb')
        client.put_attributes(
            DomainName='traces.staging.baetoken.com',
            ItemName=trace_id,
            Attributes=[
                {
                    'Name': 'payload',
                    'Value': json.dumps(payload)
                },
            ]
        )
    else:
        send_direct_messages(messages, user, do_mock_reply=do_mock_reply)
