from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core.models.traceid import TraceId


def create_trace_id():
    return TraceId().to_id()


def get_current_trace_id():
    return xray_recorder.current_segment().trace_id
