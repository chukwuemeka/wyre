import logging

from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from functools import wraps

from wyre.xray.utils import create_trace_id

logger = logging.getLogger(__name__)


def trace(service):
    def _decorator(method):
        @wraps(method)
        def wrapper(*args, **kwargs):
            trace_id = kwargs.get('trace_id')
            if not trace_id:
                trace_id = create_trace_id()
                kwargs['trace_id'] = trace_id
            xray_recorder.begin_segment('{0}.{1}'.format(service, settings.SITE_DOMAIN), traceid=trace_id)
            try:
                method(*args, **kwargs)
            except Exception:
                logger.exception('')
                raise
            if xray_recorder.current_segment():
                xray_recorder.end_segment()

        return wrapper

    return _decorator
