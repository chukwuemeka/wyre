from __future__ import absolute_import

from datetime import timedelta

import arrow
import re
from colorama import Fore, init
from dateutil.parser import parse as parse_date
from dateutil.tz import tzutc
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import Range

init(autoreset=True)


class Command(BaseCommand):
    def parse_datetime(self, datetime_text):
        if not datetime_text:
            return None

        ago_regexp = r'(\d+)\s?(m|minute|minutes|h|hour|hours|d|day|days|w|weeks|weeks)(?: ago)?'
        ago_match = re.match(ago_regexp, datetime_text)

        if ago_match:
            amount, unit = ago_match.groups()
            amount = int(amount)
            unit = {'m': 60, 'h': 3600, 'd': 86400, 'w': 604800}[unit[0]]
            date_ = timezone.now() + timedelta(seconds=unit * amount * -1)
        else:
            try:
                date_ = parse_date(datetime_text)
            except ValueError:
                raise Exception(datetime_text)

        if date_.tzinfo:
            if date_.utcoffset != 0:
                date_ = date_.astimezone(tzutc())
            date_ = date_.replace(tzinfo=None)

        return arrow.get(date_)

    def add_arguments(self, parser):
        parser.add_argument('-n', '--namespace')
        parser.add_argument('-a', '--app')
        parser.add_argument('-p', '--pod-name', dest='pod_name')
        parser.add_argument('-s', '--start-time', dest='start_time', default='5m')
        parser.add_argument('-e', '--end-time', dest='end_time', default='0m')
        parser.add_argument('-t', '--time-zone', dest='time_zone', default='US/Eastern')

    def handle(self, *args, **options):
        client = Elasticsearch('{}:{}'.format(settings.ELASTICSEARCH_HOST, settings.ELASTICSEARCH_PORT))
        start_time = self.parse_datetime(options.get('start_time'))
        end_time = self.parse_datetime(options.get('end_time'))
        num_days = int((end_time - start_time).total_seconds() / 86400)

        for i in range(max(1, num_days)):
            index = 'filebeat-6.1.3-{0}'.format(start_time.shift(days=i).format('YYYY.MM.DD'))
            search_after = None
            num_hits = 0
            while search_after or num_hits <= 0:
                search = Search(using=client, index=index).query(Range(**{
                    '@timestamp': {
                        'gte': start_time.format(start_time.shift(days=i).format('YYYY-MM-DDTHH:mm:ss.000Z')),
                        'lt': end_time.format(end_time.format('YYYY-MM-DDTHH:mm:ss.000Z'))
                    }
                }))
                if options.get('namespace'):
                    search = search.filter('term', **{'kubernetes.namespace': options.get('namespace')})
                if options.get('app'):
                    search = search.filter('term', **{'kubernetes.labels.k8s-app': options.get('app')})
                if options.get('pod_name'):
                    search = search.filter('term', **{'kubernetes.pod.name': options.get('pod')})
                if search_after:
                    search = search.extra(search_after=search_after, size=200)
                else:
                    search = search.extra(size=200)
                response = search.sort('@timestamp', '_uid').execute()
                if not response.hits:
                    break
                for hit in response.hits:
                    pod_name = hit.to_dict().get('kubernetes', {}).get('pod', {}).get('name', '-')
                    self.stdout.write('{1} {3} {5}'.format(
                        Fore.MAGENTA,
                        arrow.get(hit['@timestamp']).to(options.get('time_zone')).format(),
                        Fore.CYAN,
                        pod_name,
                        Fore.LIGHTGREEN_EX,
                        hit.message
                    ))
                    num_hits += 1
                search_after = list(
                    response.hits[-1].meta.sort) if response.hits and num_hits < response.hits.total else None
