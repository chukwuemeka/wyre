from django.views.debug import SafeExceptionReporterFilter as DjangoSafeExceptionReporterFilter


class SafeExceptionReporterFilter(DjangoSafeExceptionReporterFilter):
    def get_traceback_frame_variables(self, request, tb_frame):
        traceback_frame_variables = []
        try:
            traceback_frame_variables = super().get_traceback_frame_variables(request, tb_frame)
        except Exception:
            pass
        return traceback_frame_variables
