# from __future__ import absolute_import
# from __future__ import division
# from __future__ import print_function
# from __future__ import unicode_literals
#
# from unittest import skip
#
# from django.test import TestCase
#
#
# # from graphene.test import Client
#
# # from punchkicktap.schema import schema
# # from punchkicktap.users.factories import UserFactory
# # from punchkicktap.users.models import User
#
# @skip('')
# class UserQueryTestCase(TestCase):
#     def setUp(self):
#         self.user = UserFactory()
#         context = {
#             'user': self.user
#         }
#         self.client = Client(schema, context_value=context)
#
#     def tearDown(self):
#         User.objects.all().delete()
#
#     def test_is_user_authenticated(self):
#         query = '''
#             {
#                 isUserAuthenticated
#             }
#         '''
#         response = self.client.execute(query)
#         self.assertIsNone(response.get('errors'))
#         self.assertTrue(response.get('data').get('isUserAuthenticated'))
#
#     def test_is_user_not_authenticated(self):
#         context = {
#         }
#         self.client = Client(schema, context_value=context)
#         query = '''
#             {
#                 isUserAuthenticated
#             }
#         '''
#         response = self.client.execute(query)
#         self.assertIsNone(response.get('errors'))
#         self.assertFalse(response.get('data').get('isUserAuthenticated'))
