from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase

from wyre.users.models import User


class UserTestCase(TestCase):
    def setUp(self):
        self.data = {
            'email_address': 'sterlingarcher@gmail.com',
            'password': 'password123'
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_create_user(self):
        user = User.objects.create_user(
            self.data.get('email_address'),
            self.data.get('password')
        )
        self.assertIsNotNone(user)
