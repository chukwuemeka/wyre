from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

# Get the UserModel
User = get_user_model()


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('email_address')
        parser.add_argument('password')

    def handle(self, *args, **options):
        User.objects.create_superuser(
            email_address=options.get('email_address'),
            password=options.get('password')
        )
