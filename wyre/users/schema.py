from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from graphene import AbstractType, Boolean


class Query(AbstractType):
    is_user_authenticated = Boolean(required=True)

    def resolve_is_user_authenticated(self, input, context, *args, **kwargs):
        user = context.get('user')
        if user:
            return user.is_authenticated()
        return False
