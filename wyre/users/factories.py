from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory
from faker import Faker

from .models import User
from ..profiles.factories import ProfileFactory

DEFAULT_PASSWORD = 'over9000'


class UserFactory(factory.django.DjangoModelFactory):
    full_name = factory.Faker('name')
    password = factory.PostGenerationMethodCall('set_password', DEFAULT_PASSWORD)
    profile = factory.RelatedFactory(ProfileFactory, 'user')

    @factory.lazy_attribute
    def email_address(self):
        faker = Faker()
        email_address = None
        while email_address is None or User.objects.filter(email_address=email_address).exists():
            email_address = faker.email()
        return email_address

    # @factory.post_generation
    # def post(obj, create, extracted, **kwargs):
    #     if kwargs.get('do_create_profile_image', False):
    #         response = requests.get('http://lorempixel.com/100/100/')
    #         obj.profile_image.save(uuid4().hex, ContentFile(response.content))

    class Meta:
        model = User
