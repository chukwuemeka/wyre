from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import factory
import random
from moneyed import Money

from .models import PresaleTokenPurchaseOrder
from .models import PresaleWeiPurchaseOrder
from .models import TokenPurchaseOrder, WeiWithdrawal
from .models import TokenTransfer, Account, WeiPurchaseOrder
from .models import TokenWithdrawal
from .utils import client
from .utils import generate_key_pair
from .utils import get_address


class AccountFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory('wyre.users.factories.UserFactory')
    passphrase = factory.Faker('md5')

    @factory.post_generation
    def post(self, create, extracted, **kwargs):
        public_key, private_key = generate_key_pair()
        if not self.private_key:
            self.private_key = client.toHex(private_key)
        if not self.address:
            self.address = get_address(public_key=public_key)

    class Meta:
        model = Account


class TokenPurchaseOrderFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory('wyre.users.factories.UserFactory')
    braintree_id = factory.Faker('md5')
    email_address = factory.Faker('email')

    @factory.lazy_attribute
    def token_charge(self):
        return Money(random.uniform(1, 1500), 'USD')

    @factory.lazy_attribute
    def wei_charge(self):
        return Money(random.uniform(1, 20), 'USD')

    @factory.lazy_attribute
    def service_charge(self):
        return Money(random.uniform(1, 5), 'USD')

    @factory.lazy_attribute
    def num_tokens(self):
        return max(1.0, random.random() * 10)

    @factory.lazy_attribute
    def num_wei(self):
        return random.randint(1, 9000)

    @factory.lazy_attribute
    def wei_estimate(self):
        return random.randint(1, 9000)

    class Meta:
        model = TokenPurchaseOrder


class WeiPurchaseOrderFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory('wyre.users.factories.UserFactory')
    braintree_id = factory.Faker('md5')
    email_address = factory.Faker('email')

    @factory.lazy_attribute
    def wei_charge(self):
        return Money(random.uniform(1, 20), 'USD')

    @factory.lazy_attribute
    def service_charge(self):
        return Money(random.uniform(1, 5), 'USD')

    @factory.lazy_attribute
    def num_wei(self):
        return random.randint(1, 9000)

    @factory.lazy_attribute
    def wei_estimate(self):
        return random.randint(1, 9000)

    class Meta:
        model = WeiPurchaseOrder


class TokenTransferFactory(factory.django.DjangoModelFactory):
    sender = factory.SubFactory('wyre.users.factories.UserFactory')
    recipient = factory.SubFactory('wyre.users.factories.UserFactory')

    @factory.lazy_attribute
    def num_tokens(self):
        return max(1.0, random.random() * 10)

    class Meta:
        model = TokenTransfer


class TokenWithdrawalFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory('wyre.users.factories.UserFactory')

    @factory.lazy_attribute
    def num_tokens(self):
        return max(1.0, random.random() * 10)

    @factory.lazy_attribute
    def to_address(self):
        return get_address(public_key=generate_key_pair()[0])

    class Meta:
        model = TokenWithdrawal


class WeiWithdrawalFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory('wyre.users.factories.UserFactory')

    @factory.lazy_attribute
    def num_wei(self):
        return random.randint(1, 9000)

    @factory.lazy_attribute
    def to_address(self):
        return get_address(public_key=generate_key_pair()[0])

    class Meta:
        model = WeiWithdrawal


class PresaleTokenPurchaseOrderFactory(factory.django.DjangoModelFactory):
    full_name = factory.Faker('name')
    email_address = factory.Faker('email')

    @factory.lazy_attribute
    def address(self):
        return get_address(public_key=generate_key_pair()[0])

    @factory.lazy_attribute
    def token_charge(self):
        return Money(random.uniform(1, 1500), 'USD')

    @factory.lazy_attribute
    def num_tokens(self):
        return max(1.0, random.random() * 10)

    class Meta:
        model = PresaleTokenPurchaseOrder


class PresaleWeiPurchaseOrderFactory(factory.django.DjangoModelFactory):
    full_name = factory.Faker('name')
    email_address = factory.Faker('email')

    @factory.lazy_attribute
    def address(self):
        return get_address(public_key=generate_key_pair()[0])

    @factory.lazy_attribute
    def wei_charge(self):
        return Money(random.uniform(1, 1500), 'USD')

    @factory.lazy_attribute
    def num_wei(self):
        return max(1.0, random.random() * 10)

    class Meta:
        model = PresaleWeiPurchaseOrder
