import logging

import braintree
from django.conf import settings
from django.core.exceptions import SuspiciousOperation
from django.core.mail import EmailMultiAlternatives
from django.http.response import HttpResponsePermanentRedirect
from django.template import loader
from django.template.response import TemplateResponse
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

from .forms import TokenCheckoutForm
from .forms import WeiCheckoutForm
from .models import TokenPurchaseOrder
from .models import WeiPurchaseOrder

logger = logging.getLogger(__name__)

braintree.Configuration.configure(
    braintree.Environment.Sandbox if settings.BRAINTREE_ENVIRONMENT.lower() == 'sandbox' else braintree.Environment.Production,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY
)


class TokenCheckoutSuccessView(TemplateView):
    template_name = 'ethereum/token_checkout_success.html'
    token = None

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['token_purchase_order'] = TokenPurchaseOrder.objects.from_token(self.token)
        context_data['user'] = context_data['token_purchase_order'].user
        return context_data

    def get(self, request, *args, **kwargs):
        self.token = kwargs['token']
        token_purchase_order = TokenPurchaseOrder.objects.from_token(self.token)
        if not token_purchase_order:
            raise SuspiciousOperation
        if not token_purchase_order.braintree_id:
            raise SuspiciousOperation
        return super(TokenCheckoutSuccessView, self).get(request, *args, **kwargs)


class TokenCheckoutView(FormView):
    template_name = 'ethereum/token_checkout.html'
    form_class = TokenCheckoutForm
    token = None

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['token_purchase_order'] = TokenPurchaseOrder.objects.from_token(self.token)
        context_data['user'] = context_data['token_purchase_order'].user
        context_data['client_token'] = braintree.ClientToken.generate({
            # 'customer_id': context_data['token_purchase_order'].user.braintree_id
        })
        return context_data

    def get_success_url(self):
        return reverse('token_checkout_success', kwargs={
            'token': self.token
        })

    def get_form_kwargs(self):
        form_kwargs = super(TokenCheckoutView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = self.request.POST.copy()
            data['token_purchase_order'] = self.token
            form_kwargs['data'] = data
        return form_kwargs

    def validate_token_purchase_order(self, request, token):
        if not TokenPurchaseOrder.objects.is_token_valid(token):
            return TemplateResponse(request, template='ethereum/token_checkout_bad_request.html', status=400)
        self.token = token
        token_purchase_oder = TokenPurchaseOrder.objects.from_token(self.token)
        if token_purchase_oder.braintree_id:
            return HttpResponsePermanentRedirect(self.get_success_url())
        return None

    def get(self, request, *args, **kwargs):
        bad_request = self.validate_token_purchase_order(request, kwargs.get('token'))
        if bad_request:
            return bad_request
        return super(TokenCheckoutView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        bad_request = self.validate_token_purchase_order(request, kwargs.get('token'))
        if bad_request:
            return bad_request
        return super(TokenCheckoutView, self).post(request, *args, **kwargs)

    def form_invalid(self, form):
        try:
            raise Exception('Failed to submit form for token purchase order')
        except Exception:
            logger.exception('Failed to submit form for token purchase order')
        return super().form_invalid(form)

    def form_valid(self, form):
        try:
            token_purchase_order, transaction = form.checkout()
        except Exception:
            logger.exception('Token purchase order failed')
            return self.render_to_response(self.get_context_data(form=form, did_transaction_fail=True))

        # Send mail
        context = {
            'token_purchase_order': token_purchase_order,
            'transaction': transaction
        }
        subject = ''.join(loader.render_to_string('ethereum/emails/token_receipt_subject.txt', context,
                                                  request=self.request).splitlines())
        body = loader.render_to_string('ethereum/emails/token_receipt_body.txt', context, request=self.request)
        # html_body = loader.render_to_string('ethereum/emails/token_receipt_body.html', context, request=self.request)
        email_message = EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[token_purchase_order.email_address],
            bcc=[settings.SERVER_EMAIL]
        )
        # email_message.attach_alternative(html_body, 'text/html')
        email_message.send()

        return super().form_valid(form)


class WeiCheckoutSuccessView(TemplateView):
    template_name = 'ethereum/wei_checkout_success.html'
    token = None

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['wei_purchase_order'] = WeiPurchaseOrder.objects.from_token(self.token)
        context_data['user'] = context_data['wei_purchase_order'].user
        return context_data

    def get(self, request, *args, **kwargs):
        self.token = kwargs['token']
        wei_purchase_order = WeiPurchaseOrder.objects.from_token(self.token)
        if not wei_purchase_order:
            raise SuspiciousOperation
        if not wei_purchase_order.braintree_id:
            raise SuspiciousOperation
        return super(WeiCheckoutSuccessView, self).get(request, *args, **kwargs)


class WeiCheckoutView(FormView):
    template_name = 'ethereum/wei_checkout.html'
    form_class = WeiCheckoutForm
    token = None

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['wei_purchase_order'] = WeiPurchaseOrder.objects.from_token(self.token)
        context_data['user'] = context_data['wei_purchase_order'].user
        context_data['client_token'] = braintree.ClientToken.generate({
            # 'customer_id': context_data['wei_purchase_order'].user.braintree_id
        })
        return context_data

    def get_success_url(self):
        return reverse('wei_checkout_success', kwargs={
            'token': self.token
        })

    def get_form_kwargs(self):
        form_kwargs = super(WeiCheckoutView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = self.request.POST.copy()
            data['wei_purchase_order'] = self.token
            form_kwargs['data'] = data
        return form_kwargs

    def validate_wei_purchase_order(self, request, token):
        if not WeiPurchaseOrder.objects.is_token_valid(token):
            return TemplateResponse(request, template='ethereum/wei_checkout_bad_request.html', status=400)
        self.token = token
        token_purchase_oder = WeiPurchaseOrder.objects.from_token(self.token)
        if token_purchase_oder.braintree_id:
            return HttpResponsePermanentRedirect(self.get_success_url())
        return None

    def get(self, request, *args, **kwargs):
        bad_request = self.validate_wei_purchase_order(request, kwargs.get('token'))
        if bad_request:
            return bad_request
        return super(WeiCheckoutView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        bad_request = self.validate_wei_purchase_order(request, kwargs.get('token'))
        if bad_request:
            return bad_request
        return super(WeiCheckoutView, self).post(request, *args, **kwargs)

    def form_invalid(self, form):
        try:
            raise Exception('Failed to submit form for wei purchase order')
        except Exception:
            logger.exception('Failed to submit form for wei purchase order')
        return super().form_invalid(form)

    def form_valid(self, form):
        try:
            wei_purchase_order, transaction = form.checkout()
        except Exception:
            return self.render_to_response(self.get_context_data(form=form, did_transaction_fail=True))

        # Send mail
        context = {
            'wei_purchase_order': wei_purchase_order,
            'transaction': transaction
        }
        subject = ''.join(loader.render_to_string('ethereum/emails/wei_receipt_subject.txt', context,
                                                  request=self.request).splitlines())
        body = loader.render_to_string('ethereum/emails/wei_receipt_body.txt', context, request=self.request)
        # html_body = loader.render_to_string('ethereum/emails/wei_receipt_body.html', context, request=self.request)
        email_message = EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[wei_purchase_order.email_address],
            bcc=[settings.SERVER_EMAIL]
        )
        # email_message.attach_alternative(html_body, 'text/html')
        email_message.send()

        return super().form_valid(form)
