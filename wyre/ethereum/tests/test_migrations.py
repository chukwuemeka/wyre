import uuid

import base64
from django.apps import apps
from django.db import connection
from django.db.migrations.executor import MigrationExecutor
from django.test import TestCase
from faker import Faker

from ..utils import generate_key_pair, get_address, client


class MigrationTestCase(TestCase):
    app = None
    migrate_from = None
    migrate_to = None

    def setUp(self):
        assert self.app and self.migrate_from and self.migrate_to, \
            "TestCase '{}' must define migrate_from, migrate_to, and app properties".format(type(self).__name__)
        self.migrate_from = [(self.app, self.migrate_from)]
        self.migrate_to = [(self.app, self.migrate_to)]
        executor = MigrationExecutor(connection)
        old_apps = executor.loader.project_state(self.migrate_from).apps

        # Reverse to the original migration
        executor.migrate(self.migrate_from)

        self.setUpBeforeMigration(old_apps)

        # Run the migration to test
        executor = MigrationExecutor(connection)
        executor.loader.build_graph()
        executor.migrate(self.migrate_to)

        self.apps = executor.loader.project_state(self.migrate_to).apps

    def setUpBeforeMigration(self, apps):
        pass


class ConvertPrivateKeyTestCase(MigrationTestCase):
    app = 'ethereum'
    migrate_from = '0043_account_private_key'
    migrate_to = '0044_auto_20180208_2345'

    def tearDown(self):
        User = apps.get_model('users', 'User')
        User.objects.all().delete()

    def setUpBeforeMigration(self, apps):
        User = apps.get_model('users', 'User')
        Account = apps.get_model('ethereum', 'Account')
        faker = Faker()
        self.user = User.objects.create(email_address=faker.email(), password=faker.password())
        public_key, private_key = generate_key_pair()
        self.account_pk = Account.objects.create(
            user=self.user,
            address=get_address(public_key=public_key),
            base64_private_key=base64.b64encode(private_key).decode(),
            passphrase=uuid.uuid4().hex
        ).pk

    def test_convert_private_key(self):
        Account = self.apps.get_model('ethereum', 'Account')
        account = Account.objects.get(pk=self.account_pk)
        self.assertEqual(client.toBytes(hexstr=account.private_key), base64.b64decode(account.base64_private_key))
