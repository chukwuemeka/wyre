from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from unittest import mock

import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.db.models import Q
from django.test import TestCase

from ..factories import AccountFactory
from ..factories import PresaleTokenPurchaseOrderFactory
from ..factories import PresaleWeiPurchaseOrderFactory
from ..factories import TokenPurchaseOrderFactory
from ..factories import TokenTransferFactory
from ..factories import TokenWithdrawalFactory
from ..factories import WeiPurchaseOrderFactory
from ..factories import WeiWithdrawalFactory
from ..models import Account
from ..models import PresaleTokenPurchaseOrder
from ..models import PresaleWeiPurchaseOrder
from ..models import TokenPurchaseOrder
from ..models import TokenTransfer
from ..models import TokenWithdrawal
from ..models import WeiPurchaseOrder
from ..tasks import process_log
from ..tasks import set_token_transfer_gas_estimate
from ..tasks import set_wei_transfer_gas_estimate
from ..tasks import submit_presale_token_purchase_order_transaction
from ..tasks import submit_presale_wei_purchase_order_transaction
from ..tasks import submit_token_purchase_order_transactions
from ..tasks import submit_token_transfer_transaction
from ..tasks import submit_token_withdrawal_transaction
from ..tasks import submit_transactions
from ..tasks import submit_wei_purchase_order_transaction
from ..utils import get_address

User = get_user_model()


class SetTokenTransferGasEstimateTestCase(TestCase):
    def test_set_token_transfer_gas_estimate(self):
        set_token_transfer_gas_estimate()


class SetWeiTransferGasEstimateTestCase(TestCase):
    def test_set_wei_transfer_gas_estimate(self):
        set_wei_transfer_gas_estimate()


class SubmitTokenWithdrawalTransactionTestCase(TestCase):
    def setUp(self):
        token_agent_account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        for i in range(10):
            user = token_agent_account.user
            if i < 5:
                TokenWithdrawalFactory(user=user, transaction_hash=uuid.uuid4().hex)
            else:
                TokenWithdrawalFactory(user=user)

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transaction(self):
        for t in TokenWithdrawal.objects.all():
            submit_token_withdrawal_transaction.apply((t.id,))
        self.assertEqual(
            0,
            TokenWithdrawal.objects.filter(transaction_hash__isnull=True).count()
        )


class SubmitTokenTransferTransactionTestCase(TestCase):
    def setUp(self):
        token_agent_account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        sender = token_agent_account.user
        for i in range(10):
            account = AccountFactory()
            recipient = account.user
            kwargs = {
                'sender': sender,
                'recipient': recipient,
                'num_tokens': 1
            }
            if i < 5:
                kwargs.update({
                    'transaction_hash': None
                })
            else:
                kwargs.update({
                    'transaction_hash': uuid.uuid4().hex,
                })
            TokenTransferFactory(**kwargs)

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transaction(self):
        for t in TokenTransfer.objects.all():
            submit_token_transfer_transaction.apply((t.id,))
        self.assertEqual(
            0,
            TokenTransfer.objects.filter(transaction_hash__isnull=True).count()
        )


class SubmitWeiPurchaseOrderTransactionTestCase(TestCase):
    def setUp(self):
        for i in range(10):
            account = AccountFactory()
            kwargs = {
                'user': account.user,
                'braintree_id': uuid.uuid4().hex
            }
            if i < 3:
                kwargs.update({
                    'braintree_id': None
                })
            elif i < 6:
                kwargs.update({
                    'braintree_id': uuid.uuid4().hex
                })
            else:
                kwargs.update({
                    'transaction_hash': uuid.uuid4().hex,
                    'braintree_id': uuid.uuid4().hex
                })
            WeiPurchaseOrderFactory(**kwargs)

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transaction(self):
        for w in WeiPurchaseOrder.objects.all():
            submit_wei_purchase_order_transaction.apply((w.id,))
        self.assertEqual(
            0,
            WeiPurchaseOrder.objects.exclude(braintree_id__isnull=True).filter(transaction_hash__isnull=True).count()
        )


class SubmitTokenPurchaseOrderTransactionsTestCase(TestCase):
    def setUp(self):
        for i in range(10):
            account = AccountFactory()
            kwargs = {
                'user': account.user,
                'braintree_id': uuid.uuid4().hex
            }
            if i < 2:
                kwargs.update({
                    'braintree_id': None
                })
            elif i < 4:
                kwargs.update({
                    'token_transaction_hash': uuid.uuid4().hex
                })
            elif i < 6:
                kwargs.update({
                    'wei_transaction_hash': uuid.uuid4().hex
                })
            elif i < 8:
                kwargs.update({
                    'token_transaction_hash': uuid.uuid4().hex,
                    'wei_transaction_hash': uuid.uuid4().hex,
                    'braintree_id': uuid.uuid4().hex
                })
            TokenPurchaseOrderFactory(**kwargs)

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transactions(self):
        for t in TokenPurchaseOrder.objects.all():
            submit_token_purchase_order_transactions.apply((t.id,))
        self.assertEqual(
            0,
            TokenPurchaseOrder.objects.exclude(braintree_id__isnull=True).filter(
                Q(token_transaction_hash__isnull=True) | Q(wei_transaction_hash__isnull=True)).count()
        )


class SubmitTransactionsTestCase(TestCase):
    def setUp(self):
        token_agent_account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )

        for i in range(10):
            user = token_agent_account.user
            if i < 5:
                WeiWithdrawalFactory(user=user, transaction_hash=uuid.uuid4().hex)
            else:
                WeiWithdrawalFactory(user=user)

        for i in range(10):
            user = token_agent_account.user
            if i < 5:
                TokenWithdrawalFactory(user=user, transaction_hash=uuid.uuid4().hex)
            else:
                TokenWithdrawalFactory(user=user)

        for i in range(10):
            account = AccountFactory()
            kwargs = {
                'user': account.user,
                'braintree_id': uuid.uuid4().hex
            }
            if i < 2:
                kwargs.update({
                    'braintree_id': None
                })
            elif i < 4:
                kwargs.update({
                    'token_transaction_hash': uuid.uuid4().hex
                })
            elif i < 6:
                kwargs.update({
                    'wei_transaction_hash': uuid.uuid4().hex
                })
            elif i < 8:
                kwargs.update({
                    'token_transaction_hash': uuid.uuid4().hex,
                    'wei_transaction_hash': uuid.uuid4().hex,
                    'braintree_id': uuid.uuid4().hex
                })
            TokenPurchaseOrderFactory(**kwargs)

        sender = token_agent_account.user
        for i in range(10):
            account = AccountFactory()
            recipient = account.user
            kwargs = {
                'sender': sender,
                'recipient': recipient,
                'num_tokens': random.randint(1, 9000)
            }
            if i < 5:
                kwargs.update({
                    'transaction_hash': None
                })
            else:
                kwargs.update({
                    'transaction_hash': uuid.uuid4().hex,
                })
            TokenTransferFactory(**kwargs)

        for i in range(10):
            account = AccountFactory()
            kwargs = {
                'user': account.user
            }
            if i < 3:
                kwargs.update({
                    'braintree_id': None
                })
            elif i < 6:
                kwargs.update({
                    'transaction_hash': uuid.uuid4().hex
                })
            else:
                kwargs.update({
                    'transaction_hash': uuid.uuid4().hex,
                    'braintree_id': uuid.uuid4().hex
                })
            WeiPurchaseOrderFactory(**kwargs)

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transactions(self):
        submit_transactions.apply()
        # self.assertEqual(
        #     0,
        #     TokenPurchaseOrder.objects.exclude(braintree_id__isnull=True).filter(Q(token_transaction_hash__isnull=True) | Q(wei_transaction_hash__isnull=True)).count()
        # )
        # self.assertEqual(
        #     0,
        #     WeiPurchaseOrder.objects.exclude(braintree_id__isnull=True).filter(transaction_hash__isnull=True).count()
        # )
        # self.assertEqual(0, TokenTransfer.objects.filter(transaction_hash__isnull=True).count())
        # self.assertEqual(0, TokenWithdrawal.objects.filter(transaction_hash__isnull=True).count())
        # self.assertEqual(0, WeiWithdrawal.objects.filter(transaction_hash__isnull=True).count())


class ProcessLogTestCase(TestCase):
    def setUp(self):
        self.patchers = [
            mock.patch('tweepy.OAuthHandler', mock.MagicMock(return_value=mock.MagicMock())),
            mock.patch('tweepy.API', mock.MagicMock(return_value=mock.MagicMock())),
        ]
        [p.start() for p in self.patchers]

        self.account = AccountFactory(address='0x1d1f858db5a6f3edd7ac1b2363e7fdf0e03d0e6c')
        self.assertEqual(Account.objects.filter(address='0x1d1f858db5a6f3edd7ac1b2363e7fdf0e03d0e6c').count(), 1)
        self.log = {
            'address': '0x7CF4782066e904B15a9FC3884f9BF1C2d3ad3E4b',
            'topics': [
                '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
                '0x000000000000000000000000328d9c1df7e10302dfa6e250c1423e42c8cbcb11',
                '0x0000000000000000000000001d1f858db5a6f3edd7ac1b2363e7fdf0e03d0e6c'
            ],
            'data': '0x0000000000000000000000000000000000000000000001000000000000000000',
            'blockNumber': 1427310,
            'transactionHash': '0x050fd6de67a7856ed3cd165124ef183720fbf26dcd06e1f2361e2c12ec1c3e3b',
            'transactionIndex': 2,
            'blockHash': '0xfca36fe48fd19a8281a262462f040973789f1d926d4a3716ec108d17647984e7',
            'logIndex': 0,
            'removed': False
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()

    def test_process_log(self):
        process_log.apply(args=(self.log,))

    def test_token_transfer(self):
        log = self.log.copy()
        sender = AccountFactory().user
        recipient = AccountFactory().user
        log.get('topics')[1] = sender.account.address
        log.get('topics')[2] = recipient.account.address
        process_log.apply(args=(log,))


class SubmitPresaleTokenPurchaseOrderTestCase(TestCase):
    def setUp(self):
        for i in range(5):
            PresaleTokenPurchaseOrderFactory()

    def test_submit_transaction(self):
        for t in PresaleTokenPurchaseOrder.objects.all():
            submit_presale_token_purchase_order_transaction.apply((t.id,))
        self.assertFalse(
            PresaleTokenPurchaseOrder.objects.filter(transaction_hash__isnull=True).exists()
        )
        self.assertEqual(len(mail.outbox), 5)
        for email_message in mail.outbox:
            self.assertEqual(email_message.from_email, settings.DEFAULT_FROM_EMAIL)
            self.assertEqual(email_message.bcc, [settings.SERVER_EMAIL, settings.SUPPORT_EMAIL])


class SubmitPresaleWeiPurchaseOrderTestCase(TestCase):
    def setUp(self):
        for i in range(5):
            PresaleWeiPurchaseOrderFactory()

    def test_submit_transaction(self):
        for t in PresaleWeiPurchaseOrder.objects.all():
            submit_presale_wei_purchase_order_transaction.apply((t.id,))
        self.assertFalse(
            PresaleWeiPurchaseOrder.objects.filter(transaction_hash__isnull=True).exists()
        )
        self.assertEqual(len(mail.outbox), 5)
        for email_message in mail.outbox:
            self.assertEqual(email_message.from_email, settings.DEFAULT_FROM_EMAIL)
            self.assertEqual(email_message.bcc, [settings.SERVER_EMAIL, settings.SUPPORT_EMAIL])
