from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from decimal import Decimal
from unittest import skip

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from djmoney.money import Money
from eth_utils import to_checksum_address
from web3 import Web3, HTTPProvider

from ..models import Account
from ..utils import from_natural_value
from ..utils import generate_key_pair
from ..utils import get_address
from ..utils import get_chain_id
from ..utils import get_estimated_num_transactions
from ..utils import get_ether_balance
from ..utils import get_etherscan_url
from ..utils import get_mean_gas_price
from ..utils import get_next_nonce
from ..utils import get_or_create_account
from ..utils import get_public_key
from ..utils import get_token_balance
from ..utils import get_token_transfer_gas_estimate
from ..utils import get_usd_to_wei_exchange_rate
from ..utils import get_wei_transfer_gas_estimate
from ..utils import override_pending_transaction
from ..utils import purchase_wei
from ..utils import to_natural_value
from ..utils import to_wei
from ..utils import transfer_tokens
from ..utils import transfer_wei
from ...users.factories import UserFactory

User = get_user_model()


@skip('')
class OverridePendingTransactionTestCase(TestCase):
    def test_override_pending_transaction(self):
        transaction_hash = override_pending_transaction(
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY),
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY
        )
        self.assertIsNotNone(transaction_hash)
        self.assertNotEqual(transaction_hash, '')


class GetChainIdTestCase(TestCase):
    def test_get_chain_id(self):
        self.assertGreater(get_chain_id(), 0)


class GetNonceTestCase(TestCase):
    def test_get_nonce(self):
        self.assertGreaterEqual(get_next_nonce(get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)), 0)


class GetTokenTransferGasEstimateTestCase(TestCase):
    def test_get_token_transfer_gas_estimate(self):
        gas_estimate = get_token_transfer_gas_estimate()
        self.assertTrue(gas_estimate == 58316 or gas_estimate == 58252)


class GetWeiTransferGasEstimateTestCase(TestCase):
    def test_get_wei_transfer_gas_estimate(self):
        self.assertEqual(get_wei_transfer_gas_estimate(), 21000)


class TransferWeiTestCase(TestCase):
    def setUp(self):
        self.from_account = Account.objects.create(
            user=UserFactory(),
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.to_account = get_or_create_account(UserFactory())

    def tearDown(self):
        User.objects.all().delete()

    def test_transfer_wei(self):
        transaction_hash = transfer_wei(from_account=self.from_account, to_address=self.to_account.address, num_wei=1)
        self.assertIsNotNone(transaction_hash)


class GetEstimatedNumTransactionsTestCase(TestCase):
    def test_get_estimated_num_transactions(self):
        num_transactions = get_estimated_num_transactions(
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.assertIsNotNone(num_transactions)
        self.assertGreater(num_transactions, 0)


@skip('')
class GetMeanGasPriceTestCase(TestCase):
    def test_get_mean_gas_price(self):
        mean_gas_price = get_mean_gas_price(num_blocks=1440)
        self.assertIsNotNone(mean_gas_price)
        self.assertGreater(mean_gas_price, 0)


class FromNaturalValueTestCase(TestCase):
    def test_from_natural_value(self):
        self.assertEqual(1, from_natural_value(1000000000000000000))


class ToNaturalValueTestCase(TestCase):
    def test_from_natural_value(self):
        self.assertEqual(1000000000000000000, to_natural_value(1))


class GetWeiBalanceTestCase(TestCase):
    def test_get_ether_balance(self):
        ether_balance = get_ether_balance(
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.assertIsNotNone(ether_balance)
        self.assertGreater(ether_balance, 0)


class GetUsdToWeiExchangeRateTestCase(TestCase):
    def test_get_usd_to_wei_exchange_rate(self):
        exchange_rate = get_usd_to_wei_exchange_rate()
        self.assertIsNotNone(exchange_rate)


@skip('')
class ToWeiTestCase(TestCase):
    def test_to_wei(self):
        num_wei = to_wei(money=Money(10.00, 'USD'))
        self.assertIsNotNone(num_wei)
        expected_exchange_rate = Decimal(Web3.toWei(1, 'ether') / 1380.74)  # In the ballpark lol...
        self.assertAlmostEqual(num_wei, Decimal(10.00) * expected_exchange_rate, delta=num_wei * Decimal(0.5))


class PurchaseWeiTestCase(TestCase):
    def setUp(self):
        self.from_account = Account.objects.create(
            user=UserFactory(),
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.to_account = get_or_create_account(UserFactory())

    def tearDown(self):
        User.objects.all().delete()

    def test_purchase_ether(self):
        num_wei = 1
        transaction_hash = purchase_wei(recipient_account=self.to_account, num_wei=num_wei)
        self.assertIsNotNone(transaction_hash)


class GetEtherscanUrlTestCase(TestCase):
    def test_get_account_url(self):
        url = get_etherscan_url(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        address = get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        self.assertEqual(url, '{0}/address/{1}'.format(settings.ETHERSCAN_DOMAIN, address))


class TransferTokensTestCase(TestCase):
    def setUp(self):
        self.from_account = Account.objects.create(
            user=UserFactory(),
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.to_account = get_or_create_account(UserFactory())

    def tearDown(self):
        User.objects.all().delete()

    def test_transfer_tokens(self):
        num_tokens = 1
        transaction_hash = transfer_tokens(from_account=self.from_account, to_account=self.to_account,
                                           num_tokens=num_tokens)
        self.assertIsNotNone(transaction_hash)


class GetTokenBalanceTestCase(TestCase):
    def test_get_token_balance(self):
        balance = get_token_balance(address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY))
        self.assertGreater(balance, 0)


class PurchaseTokensTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.account = get_or_create_account(self.user)

    def tearDown(self):
        User.objects.all().delete()

    # def test_purchase_tokens(self):
    #     num_tokens = 1
    #     transaction_hash = purchase_tokens(recipient_account=self.account, num_tokens=num_tokens)
    #     self.assertIsNotNone(transaction_hash)


class GetOrCreateAccountTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()

    def tearDown(self):
        User.objects.all().delete()

    def test_get_or_create_account(self):
        user = UserFactory()
        account = get_or_create_account(user)
        self.assertIsNotNone(account)
        self.assertEqual(Account.objects.filter(user=user).count(), 1)

    def test_prevent_duplicates(self):
        user = UserFactory()
        for i in range(50):
            account = get_or_create_account(user)
        self.assertIsNotNone(account)
        self.assertEqual(Account.objects.filter(user=user).count(), 1)
        self.assertEqual(Account.objects.all().count(), 1)


class GetPublicKeyTestCase(TestCase):
    def test_get_public_key(self):
        public_key, private_key = generate_key_pair()
        self.assertEqual(get_public_key(private_key), public_key)


class GetAddressTestCase(TestCase):
    def test_get_address(self):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))
        public_key, private_key = generate_key_pair()
        passphrase = uuid.uuid4().hex
        address = web3.personal.importRawKey(private_key, passphrase)
        self.assertEqual(to_checksum_address(address), get_address(public_key=public_key))
