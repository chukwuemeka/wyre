from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from datetime import datetime, timedelta
from importlib import import_module
from unittest import skip

import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import override_settings
from django.urls import reverse
from django_webtest import WebTest
from freezegun import freeze_time
from moneyed import Money
from rest_framework import status

from ..factories import TokenPurchaseOrderFactory
from ..factories import WeiPurchaseOrderFactory
from ..models import TokenPurchaseOrder
from ..models import WeiPurchaseOrder

User = get_user_model()

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
Session = SessionStore.get_model_class()


class TokenCheckoutSuccessViewTestCase(WebTest):
    def setUp(self):
        self.token_purchase_order = TokenPurchaseOrderFactory(braintree_id=uuid.uuid4().hex)
        self.user = self.token_purchase_order.user

    def tearDown(self):
        User.objects.all().delete()

    def test_page_exists(self):
        url = reverse('token_checkout_success', kwargs={'token': self.token_purchase_order.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unfulfilled_order(self):
        token_purchase_order = TokenPurchaseOrderFactory(braintree_id=None)
        url = reverse('token_checkout_success', kwargs={'token': token_purchase_order.token})
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TokenCheckoutViewTestCase(WebTest):
    def setUp(self):
        self.token_purchase_order = TokenPurchaseOrderFactory(braintree_id=None)
        self.user = self.token_purchase_order.user
        self.data = {
            'nonce': 'fake-valid-nonce',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_page_exists(self):
        url = reverse('token_checkout', kwargs={'token': self.token_purchase_order.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'ethereum/token_checkout.html')
        self.assertIsNotNone(response.form)

    def test_checkout(self):
        token = self.token_purchase_order.token
        url = reverse('token_checkout', kwargs={'token': token})
        data = self.data.copy()
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, expected_url=reverse('token_checkout_success', kwargs={'token': token}))
        token_purchase_order = TokenPurchaseOrder.objects.get(pk=self.token_purchase_order.pk)
        # self.assertIsNotNone(token_purchase_order.token_transaction_hash)
        self.assertIsNotNone(token_purchase_order.braintree_id)
        # self.assertNotEqual(token_purchase_order.token_transaction_hash, '')
        self.assertNotEqual(token_purchase_order.braintree_id, '')
        self.assertIsNone(response.context.get('did_transaction_fail'))

    def test_fulfilled_purchase_order(self):
        token_purchase_order = TokenPurchaseOrderFactory(braintree_id=uuid.uuid4().hex)
        token = token_purchase_order.token
        url = reverse('token_checkout', kwargs={'token': token})
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, expected_url=reverse('token_checkout_success', kwargs={'token': token}),
                             status_code=status.HTTP_301_MOVED_PERMANENTLY)

    def test_expired_purchase_order(self):
        token = self.token_purchase_order.token
        url = reverse('token_checkout', kwargs={'token': token})
        with freeze_time(datetime.utcnow() + timedelta(seconds=settings.TOKEN_PURCHASE_ORDER_TIMEOUT)):
            response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTemplateUsed(response, 'ethereum/token_checkout_bad_request.html')

    def test_receipt(self):
        token = self.token_purchase_order.token
        url = reverse('token_checkout', kwargs={'token': token})
        data = self.data.copy()
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, expected_url=reverse('token_checkout_success', kwargs={'token': token}))
        self.assertEqual(len(mail.outbox), 1)
        receipt = mail.outbox[0]
        self.assertEqual(receipt.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertEqual(receipt.to, [self.token_purchase_order.email_address])

    def test_invalid_nonce(self):
        self.token_purchase_order.token_charge = Money(random.uniform(2250, 2750), 'USD')
        self.token_purchase_order.save()
        token = self.token_purchase_order.token
        url = reverse('token_checkout', kwargs={'token': token})
        data = self.data.copy()
        data['nonce'] = 'fake-processor-declined-discover-nonce'
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.context.get('did_transaction_fail'))

    @skip('')
    @override_settings(GETH_HOST='fakegeth')
    def test_geth_failed(self):
        token = self.token_purchase_order.token
        url = reverse('token_checkout', kwargs={'token': token})
        data = self.data.copy()
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, expected_url=reverse('token_checkout_success', kwargs={'token': token}))
        token_purchase_order = TokenPurchaseOrder.objects.get(pk=self.token_purchase_order.pk)
        self.assertIsNotNone(token_purchase_order.token_transaction_hash)
        self.assertIsNotNone(token_purchase_order.braintree_id)
        self.assertEqual(token_purchase_order.token_transaction_hash, '')
        self.assertNotEqual(token_purchase_order.braintree_id, '')


class WeiCheckoutViewTestCase(WebTest):
    def setUp(self):
        self.wei_purchase_order = WeiPurchaseOrderFactory(braintree_id=None)
        self.user = self.wei_purchase_order.user
        self.data = {
            'nonce': 'fake-valid-nonce',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_page_exists(self):
        url = reverse('wei_checkout', kwargs={'token': self.wei_purchase_order.token})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'ethereum/wei_checkout.html')
        self.assertIsNotNone(response.form)

    def test_checkout(self):
        token = self.wei_purchase_order.token
        url = reverse('wei_checkout', kwargs={'token': token})
        data = self.data.copy()
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, expected_url=reverse('wei_checkout_success', kwargs={'token': token}))
        wei_purchase_order = WeiPurchaseOrder.objects.get(pk=self.wei_purchase_order.pk)
        # self.assertIsNotNone(wei_purchase_order.transaction_hash)
        self.assertIsNotNone(wei_purchase_order.braintree_id)
        # self.assertNotEqual(wei_purchase_order.transaction_hash, '')
        self.assertNotEqual(wei_purchase_order.braintree_id, '')
        self.assertIsNone(response.context.get('did_transaction_fail'))

    def test_fulfilled_purchase_order(self):
        wei_purchase_order = WeiPurchaseOrderFactory(braintree_id=uuid.uuid4().hex)
        token = wei_purchase_order.token
        url = reverse('wei_checkout', kwargs={'token': token})
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, expected_url=reverse('wei_checkout_success', kwargs={'token': token}),
                             status_code=status.HTTP_301_MOVED_PERMANENTLY)

    def test_expired_purchase_order(self):
        token = self.wei_purchase_order.token
        url = reverse('wei_checkout', kwargs={'token': token})
        with freeze_time(datetime.utcnow() + timedelta(seconds=settings.WEI_PURCHASE_ORDER_TIMEOUT)):
            response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTemplateUsed(response, 'ethereum/wei_checkout_bad_request.html')

    def test_receipt(self):
        token = self.wei_purchase_order.token
        url = reverse('wei_checkout', kwargs={'token': token})
        data = self.data.copy()
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, expected_url=reverse('wei_checkout_success', kwargs={'token': token}))
        self.assertEqual(len(mail.outbox), 1)
        receipt = mail.outbox[0]
        self.assertEqual(receipt.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertEqual(receipt.to, [self.wei_purchase_order.email_address])

    def test_invalid_nonce(self):
        self.wei_purchase_order.wei_charge = Money(random.uniform(2250, 2750), 'USD')
        self.wei_purchase_order.save()
        token = self.wei_purchase_order.token
        url = reverse('wei_checkout', kwargs={'token': token})
        data = self.data.copy()
        data['nonce'] = 'fake-processor-declined-discover-nonce'
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.context.get('did_transaction_fail'))

    @skip('')
    @override_settings(GETH_HOST='fakegeth')
    def test_geth_failed(self):
        token = self.wei_purchase_order.token
        url = reverse('wei_checkout', kwargs={'token': token})
        data = self.data.copy()
        response = self.app.get(url)
        form = response.form
        for key, value in data.items():
            form[key] = value
        response = form.submit()
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, expected_url=reverse('wei_checkout_success', kwargs={'token': token}))
        wei_purchase_order = WeiPurchaseOrder.objects.get(pk=self.wei_purchase_order.pk)
        self.assertIsNotNone(wei_purchase_order.transaction_hash)
        self.assertIsNotNone(wei_purchase_order.braintree_id)
        self.assertEqual(wei_purchase_order.transaction_hash, '')
        self.assertNotEqual(wei_purchase_order.braintree_id, '')
