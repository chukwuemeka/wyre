from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from decimal import Decimal
from math import ceil
from unittest import mock

import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase, override_settings
from moneyed import Money
from web3 import Web3

from ..factories import AccountFactory
from ..factories import PresaleTokenPurchaseOrderFactory
from ..factories import PresaleWeiPurchaseOrderFactory
from ..factories import TokenPurchaseOrderFactory
from ..factories import TokenTransferFactory
from ..factories import TokenWithdrawalFactory
from ..factories import WeiPurchaseOrderFactory
from ..factories import WeiWithdrawalFactory
from ..models import TokenPurchaseOrder
from ..models import WeiPurchaseOrder
from ..utils import get_usd_to_wei_exchange_rate, get_address

User = get_user_model()


class WeiPurchaseOrderTestCase(TestCase):
    def setUp(self):
        self.exchange_rate = Web3.toWei(1, 'ether') / Decimal(800)
        self.patchers = [
            mock.patch('wyre.ethereum.tests.test_models.get_usd_to_wei_exchange_rate',
                       mock.MagicMock(return_value=self.exchange_rate)),
            mock.patch('wyre.ethereum.utils.get_usd_to_wei_exchange_rate',
                       mock.MagicMock(return_value=self.exchange_rate)),
        ]
        [p.start() for p in self.patchers]

        self.account = AccountFactory()
        self.user = self.account.user

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()

    def test_submit_transaction(self):
        wei_purchase_order = WeiPurchaseOrderFactory(user=self.user)
        self.assertIsNone(wei_purchase_order.transaction_hash)
        wei_purchase_order.submit_transaction()
        self.assertIsNotNone(wei_purchase_order.transaction_hash)

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.05))
    def test_usd_to_wei_exchange_rate_scale_factor(self):
        wei_charge = Money(5.00, 'USD')
        service_charge, num_wei, wei_estimate = WeiPurchaseOrder.get_charges(user=self.user, wei_charge=wei_charge)
        self.assertLessEqual(
            num_wei,
            int(ceil(wei_charge.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertEqual(
            num_wei,
            5952380952380953
        )
        self.assertEqual(service_charge, Money(0.46, 'USD'))

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.00))
    def test_get_charges(self):
        wei_charge = Money(5.00, 'USD')
        service_charge, num_wei, wei_estimate = WeiPurchaseOrder.get_charges(user=self.user, wei_charge=wei_charge)
        self.assertEqual(
            num_wei,
            int(ceil(wei_charge.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertEqual(service_charge, Money(0.46, 'USD'))

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.00))
    def test_twenty_dollars(self):
        wei_charge = Money(20.00, 'USD')
        service_charge, num_wei, wei_estimate = WeiPurchaseOrder.get_charges(user=self.user, wei_charge=wei_charge)
        self.assertEqual(
            num_wei,
            int(ceil(wei_charge.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertEqual(service_charge, Money(0.91, 'USD'))


class TokenPurchaseOrderTestCase(TestCase):
    def setUp(self):
        self.exchange_rate = Web3.toWei(1, 'ether') / Decimal(800)
        self.patchers = [
            mock.patch('wyre.ethereum.tests.test_models.get_usd_to_wei_exchange_rate',
                       mock.MagicMock(return_value=self.exchange_rate)),
            mock.patch('wyre.ethereum.utils.get_usd_to_wei_exchange_rate',
                       mock.MagicMock(return_value=self.exchange_rate)),
        ]
        [p.start() for p in self.patchers]

        self.account = AccountFactory()
        self.user = self.account.user

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()

    def test_submit_transactions(self):
        token_purchase_order = TokenPurchaseOrderFactory(user=self.user)
        self.assertIsNone(token_purchase_order.token_transaction_hash)
        self.assertIsNone(token_purchase_order.wei_transaction_hash)
        token_purchase_order.submit_transactions()
        self.assertIsNotNone(token_purchase_order.token_transaction_hash)
        self.assertIsNotNone(token_purchase_order.wei_transaction_hash)

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.05))
    def test_usd_to_wei_exchange_rate_scale_factor(self):
        num_tokens = 100
        token_charge, wei_charge, service_charge, num_wei, wei_estimate = TokenPurchaseOrder.get_charges(user=self.user,
                                                                                                         num_tokens=num_tokens)
        self.assertEqual(token_charge, Money(1.00, 'USD'))
        self.assertEqual(wei_charge, settings.DEFAULT_WEI_CHARGE)
        self.assertLessEqual(
            num_wei,
            int(ceil(settings.DEFAULT_WEI_CHARGE.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertEqual(
            num_wei,
            1190476190476191
        )
        self.assertEqual(service_charge, Money(0.37, 'USD'))

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.00))
    def test_get_charges(self):
        num_tokens = 100
        token_charge, wei_charge, service_charge, num_wei, wei_estimate = TokenPurchaseOrder.get_charges(user=self.user,
                                                                                                         num_tokens=num_tokens)
        self.assertEqual(token_charge, Money(1.00, 'USD'))
        self.assertEqual(wei_charge, settings.DEFAULT_WEI_CHARGE)
        self.assertEqual(
            num_wei,
            int(ceil(settings.DEFAULT_WEI_CHARGE.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertEqual(service_charge, Money(0.37, 'USD'))

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.00))
    def test_19999_tokens(self):
        num_tokens = 19999
        token_charge, wei_charge, service_charge, num_wei, wei_estimate = TokenPurchaseOrder.get_charges(user=self.user,
                                                                                                         num_tokens=num_tokens)
        self.assertEqual(token_charge, Money(199.99, 'USD'))
        self.assertEqual(wei_charge, settings.DEFAULT_WEI_CHARGE)
        self.assertEqual(
            num_wei,
            int(ceil(settings.DEFAULT_WEI_CHARGE.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertEqual(service_charge, Money(6.32, 'USD'))

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.00))
    def test_1000_tokens(self):
        num_tokens = 1000
        token_charge, wei_charge, service_charge, num_wei, wei_estimate = TokenPurchaseOrder.get_charges(user=self.user,
                                                                                                         num_tokens=num_tokens)
        self.assertIsNotNone(token_charge)
        self.assertIsNotNone(wei_charge)
        self.assertEqual(
            num_wei,
            int(ceil(settings.DEFAULT_WEI_CHARGE.amount * get_usd_to_wei_exchange_rate()))
        )
        self.assertIsNotNone(service_charge)

    @override_settings(USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR=Decimal(1.00))
    def test_second_purchase(self):
        [TokenPurchaseOrderFactory(user=self.user) for i in range(random.randint(1, 10))]
        num_tokens = 100
        token_charge, wei_charge, service_charge, num_wei, wei_estimate = TokenPurchaseOrder.get_charges(user=self.user,
                                                                                                         num_tokens=num_tokens)
        self.assertEqual(token_charge, Money(1.00, 'USD'))
        self.assertEqual(wei_charge, Money(0.00, 'USD'))
        self.assertEqual(num_wei, 0)
        self.assertEqual(service_charge, Money(0.34, 'USD'))
        self.assertGreater(wei_estimate, 0)


class TokenTransferTestCase(TestCase):
    def setUp(self):
        account = AccountFactory()
        self.recipient = account.user
        account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.sender = account.user

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transaction(self):
        token_transfer = TokenTransferFactory(sender=self.sender, recipient=self.recipient)
        self.assertIsNone(token_transfer.transaction_hash)
        token_transfer.submit_transaction()
        self.assertIsNotNone(token_transfer.transaction_hash)


class TokenWithdrawalTestCase(TestCase):
    def setUp(self):
        account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = account.user

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transaction(self):
        token_withdrawal = TokenWithdrawalFactory(user=self.user)
        self.assertIsNone(token_withdrawal.transaction_hash)
        token_withdrawal.submit_transaction()
        self.assertIsNotNone(token_withdrawal.transaction_hash)


class WeiWithdrawalTestCase(TestCase):
    def setUp(self):
        account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = account.user

    def tearDown(self):
        User.objects.all().delete()

    def test_submit_transaction(self):
        wei_withdrawal = WeiWithdrawalFactory(user=self.user)
        self.assertIsNone(wei_withdrawal.transaction_hash)
        wei_withdrawal.submit_transaction()
        self.assertIsNotNone(wei_withdrawal.transaction_hash)


class PresaleTokenPurchaseOrderTestCase(TestCase):
    def test_submit_transaction(self):
        presale_token_purchase_order = PresaleTokenPurchaseOrderFactory()
        self.assertIsNone(presale_token_purchase_order.transaction_hash)
        presale_token_purchase_order.submit_transaction()
        self.assertIsNotNone(presale_token_purchase_order.transaction_hash)

    def test_receipt(self):
        presale_token_purchase_order = PresaleTokenPurchaseOrderFactory(
            num_tokens=457070,
            address='0x52F28C1644bee70864489F339cd8e70dFD22A2e7',
            token_charge=Money(457.07, 'USD')
        )
        presale_token_purchase_order.send_receipt()
        self.assertEqual(len(mail.outbox), 1)
        email_message = mail.outbox[0]
        self.assertEqual(email_message.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertEqual(email_message.to, [presale_token_purchase_order.email_address])
        self.assertEqual(email_message.bcc, [settings.SERVER_EMAIL, settings.SUPPORT_EMAIL])
        self.assertIn('457070 FAKE Tokens', email_message.body)
        self.assertIn('Total: $457.07', email_message.body)
        self.assertIn('0x52F28C1644bee70864489F339cd8e70dFD22A2e7', email_message.body)


class PresaleWeiPurchaseOrderTestCase(TestCase):
    def test_submit_transaction(self):
        presale_wei_purchase_order = PresaleWeiPurchaseOrderFactory()
        self.assertIsNone(presale_wei_purchase_order.transaction_hash)
        presale_wei_purchase_order.submit_transaction()
        self.assertIsNotNone(presale_wei_purchase_order.transaction_hash)

    def test_receipt(self):
        presale_wei_purchase_order = PresaleWeiPurchaseOrderFactory(
            num_wei=1,
            address='0x52F28C1644bee70864489F339cd8e70dFD22A2e7',
            wei_charge=Money(457.07, 'USD')
        )
        presale_wei_purchase_order.submit_transaction()
        presale_wei_purchase_order.send_receipt()
        self.assertEqual(len(mail.outbox), 1)
        email_message = mail.outbox[0]
        self.assertEqual(email_message.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertEqual(email_message.to, [presale_wei_purchase_order.email_address])
        self.assertEqual(email_message.bcc, [settings.SERVER_EMAIL, settings.SUPPORT_EMAIL])
        self.assertIn('0.000000000000000001 ETH', email_message.subject)
        self.assertIn('0.000000000000000001 ETH', email_message.body)
        self.assertIn('Total: $457.07', email_message.body)
        self.assertIn('0x52F28C1644bee70864489F339cd8e70dFD22A2e7', email_message.body)
