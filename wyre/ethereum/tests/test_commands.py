from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from unittest import mock

from django.core import management
from django.test import TestCase

from ..management.commands.create_presale_token_purchase_orders import \
    Command as CreatePresaleTokenPurchaseOrdersCommand
from ..models import PresaleTokenPurchaseOrder


class MockResponse(mock.MagicMock):
    def __init__(self, text, *args, **kwargs):
        super().__init__(args, kwargs)
        self._text = text

    @property
    def text(self):
        return self._text

    @property
    def status_code(self):
        return 200


class CreatePresaleTokenPurchaseOrdersTestCase(TestCase):
    def setUp(self):
        text = """
"Timestamp","Name","Alias ","Email Address","Phone Number","Payment Method","Transaction Number from Payment Source (for purchases made using ETH, write down the TxHash from Etherscan for Venmo it is contained in the Venmo email receipt).","Please state the dollar amount in USD that you transferred for your purchase (ex. $5000.00). Keep in mind, during the presale 1 BAE Token = $.001","Please state the exact amount of Ethereum you transferred if you made your purchased in ETH.","Ethereum Wallet Address to Deposit BAE Tokens (will be deposited on 01/16/18). If you do not have an Ethereum wallet, visit www.myetherwallet.com to get one for free. * Coinbase wallets do not support Ethereum ERC 20 tokens","How did you hear about BAE Token?","Comments","I agree to the Terms of Service Agreement "
"12/16/2017 0:15:31","Aalaa Milagro","GreenGurly","theshadetreegroup@gmail.com","4049313245","Ethereum","0xbea746e46495d1db4cf8ff0f54cc10143058c7336949a6658012b113112f9b25","457.07","0.64567","0x52F28C1644bee70864489F339cd8e70dFD22A2e7","Friend Referral (Please name the friend in the comments section for referral purposes)","TK@Cargossimo.com recommmended we explore this opportunity and we did. ","I agree"
"""
        self.patchers = [
            mock.patch('requests.get', mock.MagicMock(return_value=MockResponse(text=text.strip('\n')))),
        ]
        [p.start() for p in self.patchers]

    def tearDown(self):
        [p.stop() for p in self.patchers]
        PresaleTokenPurchaseOrder.objects.all().delete()

    def test_command(self):
        command = CreatePresaleTokenPurchaseOrdersCommand()
        command.handle(
            url='https://mockingtonbear',
            token_price=0.001
        )
        self.assertTrue(PresaleTokenPurchaseOrder.objects.filter(address__iexact='0x52F28C1644bee70864489F339cd8e70dFD22A2e7').exists())


class Erc20TokenListenerTestCase(TestCase):
    def test_command(self):
        management.call_command('erc20_token_listener', max_num_iterations=1)


class BlockListenerTestCase(TestCase):
    def test_command(self):
        management.call_command('block_listener', max_num_iterations=1)
