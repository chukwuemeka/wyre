from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import uuid
from datetime import datetime, timedelta
from decimal import Decimal
from decimal import ROUND_CEILING
from unittest import mock
from unittest import skip

import braintree
import os
import random
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from djmoney.money import Money
from freezegun import freeze_time
from moneyed.localization import format_money
from web3 import Web3

from ..factories import AccountFactory
from ..factories import TokenPurchaseOrderFactory, WeiPurchaseOrderFactory
from ..forms import TransferTokensForm, TokenCheckoutForm, CreateTokenPurchaseOrderForm, CreateWeiPurchaseOrderForm, \
    WeiCheckoutForm, WithdrawTokensForm, WithdrawWeiForm
from ..models import TokenPurchaseOrder, WeiPurchaseOrder, TokenWithdrawal, WeiWithdrawal
from ..models import TokenTransfer
from ..utils import get_address, generate_key_pair
from ...enums import Platform
from ...users.factories import UserFactory

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')

User = get_user_model()

braintree.Configuration.configure(
    braintree.Environment.Sandbox if settings.BRAINTREE_ENVIRONMENT.lower() == 'sandbox' else braintree.Environment.Production,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY
)


@skip('')
class WithdrawWeiFormTestCase(TestCase):
    def setUp(self):
        self.account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = self.account.user
        self.data = {
            'user': self.user.id,
            'num_ether': Web3.fromWei(1, 'ether'),
            'to_address': get_address(public_key=generate_key_pair()[0]),
            'do_confirm': 'yes',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = WithdrawWeiForm()
        self.assertIsNotNone(form)

    def test_withdraw_wei(self):
        data = self.data.copy()
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        transaction_hash = form.withdraw()
        self.assertIsNotNone(transaction_hash)
        self.assertNotEqual(transaction_hash, '')

    def test_create_wei_withdrawal(self):
        data = self.data.copy()
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        transaction_hash = form.withdraw()
        self.assertEqual(WeiWithdrawal.objects.filter(
            transaction_hash=transaction_hash
        ).count(), 1)

    def test_elicit_num_ether(self):
        data = {
            'user': self.user.id,
        }
        form = WithdrawWeiForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'num_ether')

    def test_elicit_to_address(self):
        data = {
            'user': self.user.id,
            'num_ether': Web3.fromWei(1, 'ether'),
        }
        form = WithdrawWeiForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'to_address')

    def test_elicit_do_confirm(self):
        data = {
            'user': self.user.id,
            'num_ether': Web3.fromWei(1, 'ether'),
            'to_address': self.data.get('to_address')
        }
        form = WithdrawWeiForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_invalid_to_address_0(self):
        data = self.data.copy()
        data['to_address'] = 'deadbeef'
        form = WithdrawWeiForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('to_address' in form.errors)

    def test_to_address_with_no_prefix(self):
        data = self.data.copy()
        data['to_address'] = 'f33e0fa8f031535d1ec01b35ddb9a68c5bb9a56e'
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())
        transaction_hash = form.withdraw()
        self.assertEqual(WeiWithdrawal.objects.filter(
            transaction_hash=transaction_hash
        ).count(), 1)

    def test_invalid_num_ether_0(self):
        data = self.data.copy()
        data['num_ether'] = 0
        form = WithdrawWeiForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('num_ether' in form.errors)

    def test_invalid_num_ether_1(self):
        patcher = mock.patch('wyre.ethereum.forms.get_ether_balance', mock.MagicMock(return_value=0))
        patcher.start()
        data = self.data.copy()
        form = WithdrawWeiForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('num_ether' in form.errors)
        patcher.stop()

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())

    def test_fulfillment(self):
        data = self.data.copy()
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())

    def test_no_confirmation(self):  # No gas lol
        data = self.data.copy()
        data['do_confirm'] = 'no'
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())

    def test_no_gas(self):  # No gas lol
        patcher = mock.patch('wyre.ethereum.forms.get_estimated_num_transactions', mock.MagicMock(return_value=3))
        patcher.start()
        data = self.data.copy()
        form = WithdrawWeiForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())
        patcher.stop()


class WithdrawTokensFormTestCase(TestCase):
    def setUp(self):
        self.account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = self.account.user
        self.data = {
            'user': self.user.id,
            'num_tokens': 1.000001,
            'to_address': get_address(public_key=generate_key_pair()[0]),
            'do_confirm': 'yes',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = WithdrawTokensForm()
        self.assertIsNotNone(form)

    def test_withdraw_tokens(self):
        data = self.data.copy()
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        form.withdraw()
        self.assertEqual(TokenWithdrawal.objects.filter(
            user=data.get('user')
        ).count(), 1)

    def test_create_token_withdrawal(self):
        data = self.data.copy()
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        form.withdraw()
        self.assertEqual(TokenWithdrawal.objects.filter(
            user=data.get('user')
        ).count(), 1)

    def test_elicit_num_tokens(self):
        data = {
            'user': self.user.id,
        }
        form = WithdrawTokensForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'num_tokens')

    def test_elicit_to_address(self):
        data = {
            'user': self.user.id,
            'num_tokens': 1,
        }
        form = WithdrawTokensForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'to_address')

    def test_elicit_do_confirm(self):
        data = {
            'user': self.user.id,
            'num_tokens': 1,
            'to_address': self.data.get('to_address')
        }
        form = WithdrawTokensForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_invalid_to_address_0(self):
        data = self.data.copy()
        data['to_address'] = 'deadbeef'
        form = WithdrawTokensForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('to_address' in form.errors)

    def test_to_address_with_no_prefix(self):
        data = self.data.copy()
        data['to_address'] = 'f33e0fa8f031535d1ec01b35ddb9a68c5bb9a56e'
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())
        form.withdraw()
        self.assertEqual(TokenWithdrawal.objects.filter(
            user=data.get('user')
        ).count(), 1)

    def test_invalid_num_tokens_0(self):
        data = self.data.copy()
        data['num_tokens'] = 0
        form = WithdrawTokensForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('num_tokens' in form.errors)

    def test_invalid_num_tokens_1(self):  # No gas lol
        patcher = mock.patch('wyre.ethereum.forms.get_token_balance', mock.MagicMock(return_value=0))
        patcher.start()
        data = self.data.copy()
        form = WithdrawTokensForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('num_tokens' in form.errors)
        patcher.stop()

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())

    def test_fulfillment(self):
        data = self.data.copy()
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())

    def test_no_confirmation(self):  # No gas lol
        data = self.data.copy()
        data['do_confirm'] = 'no'
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())

    def test_no_gas(self):  # No gas lol
        patcher = mock.patch('wyre.ethereum.forms.get_estimated_num_transactions', mock.MagicMock(return_value=3))
        patcher.start()
        data = self.data.copy()
        form = WithdrawTokensForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())
        patcher.stop()


class CreateWeiPurchaseOrderFormTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.wei_charge = Money(
            Decimal(random.choice([5.00, 10.00, 25.00])).quantize(Decimal('.01'), rounding=ROUND_CEILING), 'USD')
        self.service_charge = Money(Decimal(random.uniform(1, 20)).quantize(Decimal('.01'), rounding=ROUND_CEILING),
                                    'USD')
        self.data = {
            'user': self.user.id,
            'email_address': 'myrealemailaddress@email.com',
            'num_wei': random.randint(1, 9000),
            'wei_estimate': random.randint(1, 9000),
            'wei_charge': format_money(self.wei_charge, locale='en_US'),
            'service_charge': format_money(self.service_charge, locale='en_US'),
            'do_confirm': 'yes',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = CreateWeiPurchaseOrderForm()
        self.assertIsNotNone(form)

    def test_create_wei_purchase_order(self):
        data = self.data.copy()
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        wei_purchase_order = form.save()
        self.assertEqual(WeiPurchaseOrder.objects.filter(user=self.user).count(), 1)
        self.assertEqual(wei_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(wei_purchase_order.service_charge, self.service_charge)

    def test_create_wei_purchase_order_with_last_email_address(self):
        last_wei_purchase_order = WeiPurchaseOrderFactory(user=self.user)
        data = self.data.copy()
        data['do_use_last_email_address'] = 'yes'
        del data['email_address']
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        wei_purchase_order = form.save()
        self.assertEqual(WeiPurchaseOrder.objects.filter(user=self.user).count(), 2)
        self.assertEqual(wei_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(wei_purchase_order.service_charge, self.service_charge)
        self.assertEqual(wei_purchase_order.email_address, last_wei_purchase_order.email_address)

    def test_elicit_wei_charge(self):
        data = {
            'user': self.user.id
        }
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'wei_charge')

    def test_elicit_email_address(self):
        data = {
            'user': self.user.id,
            'wei_charge': self.data.get('wei_charge'),
        }
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'email_address')

    def test_elicit_do_confirm(self):
        data = {
            'user': self.user.id,
            'wei_charge': self.data.get('wei_charge'),
            'email_address': self.data.get('email_address')
        }
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_elicit_do_use_last_email_address(self):
        WeiPurchaseOrderFactory(user=self.user)
        data = {
            'user': self.user.id,
            'wei_charge': self.data.get('wei_charge'),
        }
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_use_last_email_address')

    def test_do_not_elicit_email_address(self):
        WeiPurchaseOrderFactory(user=self.user)
        data = {
            'user': self.user.id,
            'wei_charge': self.data.get('wei_charge'),
            'do_use_last_email_address': 'yes'
        }
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_elicit_email_address_when_do_use_last_email_address_is_false(self):
        WeiPurchaseOrderFactory(user=self.user)
        data = {
            'user': self.user.id,
            'wei_charge': self.data.get('wei_charge'),
            'do_use_last_email_address': 'no'
        }
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'email_address')

    def test_fulfillment(self):
        data = self.data.copy()
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())

    def test_failure(self):
        data = self.data.copy()
        data['do_confirm'] = 'No'
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())

    def test_wei_estimate_equals_zero(self):
        data = self.data.copy()
        data['wei_estimate'] = 0
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        wei_purchase_order = form.save()
        self.assertEqual(WeiPurchaseOrder.objects.filter(user=self.user).count(), 1)
        self.assertEqual(wei_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(wei_purchase_order.service_charge, self.service_charge)

    def test_invalid_wei_charge(self):
        data = self.data.copy()
        data['wei_charge'] = '$9000.00'
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('wei_charge' in form.errors)

    def test_big_num_wei(self):
        data = self.data.copy()
        data['num_wei'] = Web3.toWei(9000, 'ether')
        form = CreateWeiPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(WeiPurchaseOrder.objects.filter(user=self.user).count(), 1)


class CreateTokenPurchaseOrderFormTestCase(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.token_charge = Money(Decimal(random.uniform(1, 1500)).quantize(Decimal('.01'), rounding=ROUND_CEILING),
                                  'USD')
        self.wei_charge = Money(Decimal(random.uniform(1, 20)).quantize(Decimal('.01'), rounding=ROUND_CEILING), 'USD')
        self.service_charge = Money(Decimal(random.uniform(1, 20)).quantize(Decimal('.01'), rounding=ROUND_CEILING),
                                    'USD')
        self.data = {
            'user': self.user.id,
            'email_address': 'myrealemailaddress@email.com',
            'num_tokens': random.randint(1, 9000),
            'num_wei': random.randint(1, 9000),
            'wei_estimate': random.randint(1, 9000),
            'token_charge': format_money(self.token_charge, locale='en_US'),
            'wei_charge': format_money(self.wei_charge, locale='en_US'),
            'service_charge': format_money(self.service_charge, locale='en_US'),
            'do_confirm': 'yes',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = CreateTokenPurchaseOrderForm()
        self.assertIsNotNone(form)

    def test_create_token_purchase_order(self):
        data = self.data.copy()
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        token_purchase_order = form.save()
        self.assertEqual(TokenPurchaseOrder.objects.filter(user=self.user).count(), 1)
        self.assertEqual(token_purchase_order.token_charge, self.token_charge)
        self.assertEqual(token_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(token_purchase_order.service_charge, self.service_charge)

    def test_create_token_purchase_order_with_last_email_address(self):
        last_token_purchase_order = TokenPurchaseOrderFactory(user=self.user)
        data = self.data.copy()
        data['do_use_last_email_address'] = 'yes'
        del data['email_address']
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        token_purchase_order = form.save()
        self.assertEqual(TokenPurchaseOrder.objects.filter(user=self.user).count(), 2)
        self.assertEqual(token_purchase_order.token_charge, self.token_charge)
        self.assertEqual(token_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(token_purchase_order.service_charge, self.service_charge)
        self.assertEqual(token_purchase_order.email_address, last_token_purchase_order.email_address)

    def test_elicit_num_tokens(self):
        data = {
            'user': self.user.id
        }
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'num_tokens')

    def test_elicit_email_address(self):
        data = {
            'user': self.user.id,
            'num_tokens': self.data.get('num_tokens')
        }
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'email_address')

    def test_elicit_do_confirm(self):
        data = {
            'user': self.user.id,
            'num_tokens': self.data.get('num_tokens'),
            'email_address': self.data.get('email_address')
        }
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_elicit_do_use_last_email_address(self):
        TokenPurchaseOrderFactory(user=self.user)
        data = {
            'user': self.user.id,
            'num_tokens': self.data.get('num_tokens'),
        }
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_use_last_email_address')

    def test_do_not_elicit_email_address(self):
        TokenPurchaseOrderFactory(user=self.user)
        data = {
            'user': self.user.id,
            'num_tokens': self.data.get('num_tokens'),
            'do_use_last_email_address': 'yes'
        }
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'do_confirm')

    def test_elicit_email_address_when_do_use_last_email_address_is_false(self):
        TokenPurchaseOrderFactory(user=self.user)
        data = {
            'user': self.user.id,
            'num_tokens': self.data.get('num_tokens'),
            'do_use_last_email_address': 'no'
        }
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'email_address')

    def test_fulfillment(self):
        data = self.data.copy()
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())

    def test_failure(self):
        data = self.data.copy()
        data['do_confirm'] = 'No'
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())

    def test_num_wei_equals_zero(self):
        data = self.data.copy()
        data['num_wei'] = 0
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        token_purchase_order = form.save()
        self.assertEqual(TokenPurchaseOrder.objects.filter(user=self.user).count(), 1)
        self.assertEqual(token_purchase_order.token_charge, self.token_charge)
        self.assertEqual(token_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(token_purchase_order.service_charge, self.service_charge)

    def test_wei_estimate_equals_zero(self):
        data = self.data.copy()
        data['wei_estimate'] = 0
        form = CreateTokenPurchaseOrderForm(data=data)
        self.assertTrue(form.is_valid())
        token_purchase_order = form.save()
        self.assertEqual(TokenPurchaseOrder.objects.filter(user=self.user).count(), 1)
        self.assertEqual(token_purchase_order.token_charge, self.token_charge)
        self.assertEqual(token_purchase_order.wei_charge, self.wei_charge)
        self.assertEqual(token_purchase_order.service_charge, self.service_charge)


class WeiCheckoutFormTestCase(TestCase):
    def setUp(self):
        self.wei_purchase_order = WeiPurchaseOrderFactory(braintree_id=None)
        self.user = self.wei_purchase_order.user
        self.data = {
            'wei_purchase_order': self.wei_purchase_order.token,
            'nonce': 'fake-valid-nonce',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = WeiCheckoutForm()
        self.assertIsNotNone(form)

    def test_checkout(self):
        data = self.data.copy()
        form = WeiCheckoutForm(data=data)
        self.assertTrue(form.is_valid())
        wei_purchase_order, transaction = form.checkout()
        # self.assertIsNotNone(wei_purchase_order.transaction_hash)
        self.assertIsNotNone(wei_purchase_order.braintree_id)
        # self.assertNotEqual(wei_purchase_order.transaction_hash, '')
        self.assertNotEqual(wei_purchase_order.braintree_id, '')
        self.assertIsNotNone(wei_purchase_order.user.braintree_id)
        self.assertFalse(wei_purchase_order.user.braintree_id == '')

    def test_processor_declined(self):
        data = self.data.copy()
        wei_purchase_order = WeiPurchaseOrderFactory(wei_charge=Money(2500.00, 'USD'), braintree_id=None)
        self.assertGreaterEqual(wei_purchase_order.total.amount, 2000.00)
        self.assertLessEqual(wei_purchase_order.total.amount, 3000.00)
        data['wei_purchase_order'] = wei_purchase_order.token
        form = WeiCheckoutForm(data=data)
        self.assertTrue(form.is_valid())
        with self.assertRaises(AssertionError):
            form.checkout()
        # self.assertEqual(wei_purchase_order.transaction_hash, '')
        self.assertIsNone(wei_purchase_order.braintree_id)

    def test_settled_wei_purchase_order(self):
        data = self.data.copy()
        data['wei_purchase_order'] = WeiPurchaseOrderFactory(braintree_id='1234', user=self.user).token
        form = WeiCheckoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('wei_purchase_order' in form.errors)

    def test_expired_wei_purchase_order(self):
        data = self.data.copy()
        form = WeiCheckoutForm(data=data)
        with freeze_time(datetime.utcnow() + timedelta(seconds=settings.WEI_PURCHASE_ORDER_TIMEOUT)):
            self.assertFalse(form.is_valid())
            self.assertTrue('wei_purchase_order' in form.errors)


class TokenCheckoutFormTestCase(TestCase):
    def setUp(self):
        self.token_purchase_order = TokenPurchaseOrderFactory(braintree_id=None)
        self.user = self.token_purchase_order.user
        self.data = {
            'token_purchase_order': self.token_purchase_order.token,
            'nonce': 'fake-valid-nonce',
        }

    def tearDown(self):
        User.objects.all().delete()

    def test_init(self):
        form = TokenCheckoutForm()
        self.assertIsNotNone(form)

    def test_checkout(self):
        data = self.data.copy()
        form = TokenCheckoutForm(data=data)
        self.assertTrue(form.is_valid())
        token_purchase_order, transaction = form.checkout()
        # self.assertIsNotNone(token_purchase_order.token_transaction_hash)
        # self.assertIsNotNone(token_purchase_order.wei_transaction_hash)
        self.assertIsNotNone(token_purchase_order.braintree_id)
        # self.assertNotEqual(token_purchase_order.token_transaction_hash, '')
        # self.assertNotEqual(token_purchase_order.wei_transaction_hash, '')
        self.assertNotEqual(token_purchase_order.braintree_id, '')
        self.assertIsNotNone(token_purchase_order.user.braintree_id)
        self.assertFalse(token_purchase_order.user.braintree_id == '')

    def test_processor_declined(self):
        data = self.data.copy()
        token_purchase_order = TokenPurchaseOrderFactory(token_charge=Money(2500.00, 'USD'), braintree_id=None)
        self.assertGreaterEqual(token_purchase_order.total.amount, 2000.00)
        self.assertLessEqual(token_purchase_order.total.amount, 3000.00)
        data['token_purchase_order'] = token_purchase_order.token
        form = TokenCheckoutForm(data=data)
        self.assertTrue(form.is_valid())
        with self.assertRaises(AssertionError):
            form.checkout()
        self.assertIsNone(token_purchase_order.braintree_id)

    def test_settled_token_purchase_order(self):
        data = self.data.copy()
        data['token_purchase_order'] = TokenPurchaseOrderFactory(user=self.user).token
        form = TokenCheckoutForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('token_purchase_order' in form.errors)

    def test_expired_token_purchase_order(self):
        data = self.data.copy()
        form = TokenCheckoutForm(data=data)
        with freeze_time(datetime.utcnow() + timedelta(seconds=settings.TOKEN_PURCHASE_ORDER_TIMEOUT)):
            self.assertFalse(form.is_valid())
            self.assertTrue('token_purchase_order' in form.errors)


class TransferTokensFormTestCase(TestCase):
    def setUp(self):
        def mock_memoize(*args, **kwargs):
            def inner(*args, **kwargs):
                class User:
                    id = uuid.uuid4().hex
                    screen_name = uuid.uuid4().hex

                return User()

            return inner

        self.patchers = [
            mock.patch('wyre.twitter.utils.Api'),
            mock.patch('wyre.ethereum.forms.TwitterApi'),
            mock.patch('wyre.ethereum.forms.memoize', mock.MagicMock(return_value=mock_memoize)),
            mock.patch('wyre.ethereum.forms.get_token_balance', mock.MagicMock(return_value=9000))
        ]
        [p.start() for p in self.patchers]
        self.account = AccountFactory(
            private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
            passphrase=settings.ERC20_TOKEN_AGENT_PASSPHRASE,
            address=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
        )
        self.user = self.account.user
        self.data = {
            'sender': self.user.id,
            'num_tokens': 30,
            'recipient': '@charmandar'
        }

    def tearDown(self):
        [p.stop() for p in self.patchers]
        User.objects.all().delete()

    def test_init(self):
        form = TransferTokensForm(platform=Platform.TWITTER)
        self.assertIsNotNone(form)

    def test_create_token_transfer(self):
        data = self.data.copy()
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        form.transfer()
        self.assertEqual(TokenTransfer.objects.filter(
            sender=data.get('sender'),
            num_tokens=data.get('num_tokens')
        ).count(), 1)

    def test_transfer_tokens(self):
        data = self.data.copy()
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        form.transfer()
        self.assertEqual(TokenTransfer.objects.filter(
            sender=data.get('sender')
        ).count(), 1)

    def test_elicit_recipient(self):
        data = {
            'sender': self.user.id
        }
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'recipient')

    def test_elicit_num_tokens(self):
        data = {
            'sender': self.user.id,
            'recipient': '@charmandarchar'
        }
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.get_field_to_elicit(), 'num_tokens')

    def test_elicit_nothing(self):
        data = self.data.copy()
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.get_field_to_elicit())

    def test_invalid_num_tokens(self):
        data = self.data.copy()
        data['num_tokens'] = 0
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('num_tokens' in form.errors)

    def test_fulfillment(self):
        data = self.data.copy()
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.is_fulfilled())

    def test_failure(self):  # No gas lol
        patcher = mock.patch('wyre.ethereum.forms.get_estimated_num_transactions', mock.MagicMock(return_value=3))
        patcher.start()
        data = self.data.copy()
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        self.assertFalse(form.is_fulfilled())
        patcher.stop()

    def test_invalid_twitter_recipient(self):
        data = self.data.copy()
        data['recipient'] = 'Eric Rice'
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('recipient' in form.errors)

    def test_complex_twitter_recipient_0(self):
        data = self.data.copy()
        data['recipient'] = '@earlyadapter he is looking kinda shifty'
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual('earlyadapter', form.cleaned_data.get('recipient'))

    def test_complex_twitter_recipient_1(self):
        data = self.data.copy()
        data['recipient'] = 'he is looking @earlyadapter @anotherscreenname kinda shifty'
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual('earlyadapter', form.cleaned_data.get('recipient'))

    def test_invalid_token_balance(self):
        patcher = mock.patch('wyre.ethereum.forms.get_token_balance', mock.MagicMock(return_value=0))
        patcher.start()

        data = self.data.copy()
        form = TransferTokensForm(platform=Platform.TWITTER, data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('num_tokens' in form.errors)

        patcher.stop()
