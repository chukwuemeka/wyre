import logging
import uuid
from decimal import Decimal

import braintree
import re
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from djmoney.money import Money
from eth_utils import to_checksum_address
from web3 import Web3

from .models import TokenPurchaseOrder, WeiPurchaseOrder, TokenTransfer
from .models import TokenWithdrawal
from .models import WeiWithdrawal
from .utils import get_ether_balance
from .utils import get_or_create_account, from_natural_value
from .utils import get_token_balance, get_estimated_num_transactions
from ..decorators import memoize
from ..enums import Platform
from ..forms import IntentFormMixin, YesNoField
from ..forms import ModelTokenField
from ..forms import MoneyField
from ..profiles.models import Profile
from ..twitter.utils import Api as TwitterApi

logger = logging.getLogger(__name__)

User = get_user_model()

braintree.Configuration.configure(
    braintree.Environment.Sandbox if settings.BRAINTREE_ENVIRONMENT.lower() == 'sandbox' else braintree.Environment.Production,
    merchant_id=settings.BRAINTREE_MERCHANT_ID,
    public_key=settings.BRAINTREE_PUBLIC_KEY,
    private_key=settings.BRAINTREE_PRIVATE_KEY
)


class EthereumAccountAddressField(forms.CharField):
    def to_python(self, value):
        ret = super().to_python(value)
        if not ret.startswith('0x'):
            ret = '0x{0}'.format(ret)
        return ret

    def clean(self, value):
        return to_checksum_address(super().clean(value))

    def validate(self, value):
        if not Web3.isAddress(value):
            raise forms.ValidationError('Invalid address')


class CreateWeiPurchaseOrderForm(IntentFormMixin, forms.Form):
    email_address = forms.EmailField(required=False)
    service_charge = MoneyField()
    wei_charge = MoneyField()
    num_wei = forms.DecimalField(max_digits=40, decimal_places=0)
    wei_estimate = forms.IntegerField(min_value=0)
    user = forms.ModelChoiceField(queryset=User.objects.all())
    do_use_last_email_address = YesNoField(required=False)
    do_confirm = YesNoField()
    WEI_CHARGE_CHOICES = [
        Money(Decimal(5.00), 'USD'),
        Money(Decimal(10.00), 'USD'),
        Money(Decimal(25.00), 'USD'),
        Money(Decimal(50.00), 'USD')
    ]

    def clean_wei_charge(self):
        wei_charge = self.cleaned_data.get('wei_charge')
        if wei_charge not in CreateWeiPurchaseOrderForm.WEI_CHARGE_CHOICES:
            raise forms.ValidationError('Invalid choice')
        return wei_charge

    def clean(self):
        cleaned_data = super().clean()
        email_address = cleaned_data.get('email_address')
        do_use_last_email_address = cleaned_data.get('do_use_last_email_address')
        user = cleaned_data.get('user')
        if do_use_last_email_address and user and not WeiPurchaseOrder.objects.filter(user=user).exists():
            raise forms.ValidationError('User does not have an existing token purchase order')
        if not do_use_last_email_address and not email_address:
            raise forms.ValidationError({
                'email_address': 'This field is required.'
            })
        return cleaned_data

    def is_fulfilled(self):
        return self.is_valid() and self.cleaned_data.get('do_confirm', False)

    def get_field_to_elicit(self):
        self.is_valid()
        fields = [
            'wei_charge',
            'do_use_last_email_address',
            'email_address',
            'do_confirm',
        ]
        user = self.cleaned_data.get('user')
        do_use_last_email_address = self.cleaned_data.get('do_use_last_email_address')
        if do_use_last_email_address == '' and WeiPurchaseOrder.objects.filter(
                user=user).exists() and 'wei_charge' not in self.errors:
            return 'do_use_last_email_address'
        for field in fields:
            if field in self.errors:
                return field
        return None

    def save(self):
        user = self.cleaned_data.get('user')
        if self.cleaned_data.get('do_use_last_email_address'):
            email_address = WeiPurchaseOrder.objects.filter(user=user).order_by('-date_created').first().email_address
        else:
            email_address = self.cleaned_data.get('email_address')
        assert email_address
        assert email_address != ''
        wei_purchase_order = WeiPurchaseOrder.objects.create(
            user=user,
            email_address=email_address,
            wei_charge=self.cleaned_data.get('wei_charge'),
            service_charge=self.cleaned_data.get('service_charge'),
            num_wei=self.cleaned_data.get('num_wei'),
            wei_estimate=self.cleaned_data.get('wei_estimate')
        )
        return wei_purchase_order


class CreateTokenPurchaseOrderForm(IntentFormMixin, forms.Form):
    email_address = forms.EmailField(required=False)
    token_charge = MoneyField()
    service_charge = MoneyField()
    wei_charge = MoneyField()
    num_tokens = forms.DecimalField(max_digits=40, decimal_places=settings.ERC20_TOKEN_NUM_DECIMAL_PLACES,
                                    min_value=from_natural_value(1))
    num_wei = forms.DecimalField(max_digits=40, decimal_places=0, min_value=0)
    wei_estimate = forms.IntegerField(min_value=0)
    user = forms.ModelChoiceField(queryset=User.objects.all())
    do_use_last_email_address = YesNoField(required=False)
    do_confirm = YesNoField()

    def clean(self):
        cleaned_data = super().clean()
        email_address = cleaned_data.get('email_address')
        do_use_last_email_address = cleaned_data.get('do_use_last_email_address')
        user = cleaned_data.get('user')
        if do_use_last_email_address and user and not TokenPurchaseOrder.objects.filter(user=user).exists():
            raise forms.ValidationError('User does not have an existing token purchase order')
        if not do_use_last_email_address and not email_address:
            raise forms.ValidationError({
                'email_address': 'This field is required.'
            })
        return cleaned_data

    def is_fulfilled(self):
        return self.is_valid() and self.cleaned_data.get('do_confirm', False)

    def get_field_to_elicit(self):
        self.is_valid()
        fields = [
            'num_tokens',
            'do_use_last_email_address',
            'email_address',
            'do_confirm',
        ]
        user = self.cleaned_data.get('user')
        do_use_last_email_address = self.cleaned_data.get('do_use_last_email_address')
        if do_use_last_email_address == '' and TokenPurchaseOrder.objects.filter(
                user=user).exists() and 'num_tokens' not in self.errors:
            return 'do_use_last_email_address'
        for field in fields:
            if field in self.errors:
                return field
        return None

    def save(self):
        user = self.cleaned_data.get('user')
        if self.cleaned_data.get('do_use_last_email_address'):
            email_address = TokenPurchaseOrder.objects.filter(user=user).order_by('-date_created').first().email_address
        else:
            email_address = self.cleaned_data.get('email_address')
        assert email_address
        assert email_address != ''
        token_purchase_order = TokenPurchaseOrder.objects.create(
            user=user,
            email_address=email_address,
            token_charge=self.cleaned_data.get('token_charge'),
            wei_charge=self.cleaned_data.get('wei_charge'),
            service_charge=self.cleaned_data.get('service_charge'),
            num_tokens=self.cleaned_data.get('num_tokens'),
            num_wei=self.cleaned_data.get('num_wei'),
            wei_estimate=self.cleaned_data.get('wei_estimate')
        )
        return token_purchase_order


class TokenCheckoutForm(forms.Form):
    token_purchase_order = ModelTokenField(queryset=TokenPurchaseOrder.objects.filter(braintree_id__isnull=True),
                                           max_age=settings.TOKEN_PURCHASE_ORDER_TIMEOUT)
    nonce = forms.CharField(widget=forms.HiddenInput())

    def checkout(self):
        assert self.is_valid()
        token_purchase_order = self.cleaned_data.get('token_purchase_order')

        result = braintree.Transaction.sale({
            'amount': token_purchase_order.total.amount,
            'order_id': 'wyre.ethereum.models.TokenPurchaseOrder:{0}'.format(token_purchase_order.id),
            'payment_method_nonce': self.cleaned_data.get('nonce'),
            'options': {
                'submit_for_settlement': True,
                'store_in_vault_on_success': True
            }
        })
        assert result.is_success, result.message
        token_purchase_order.braintree_id = result.transaction.id
        token_purchase_order.save()
        token_purchase_order.user.profile.braintree_id = result.transaction.customer_details.id
        token_purchase_order.user.profile.save()

        # # Initiate token transaction on blockchain
        # num_tokens = token_purchase_order.num_tokens
        # try:
        #     account = get_or_create_account(token_purchase_order.user)
        # except:
        #     logger.critical('Failed to get or create Ethereum account for {0}'.format(token_purchase_order.user),
        #                     exc_info=True)
        #     return token_purchase_order, result.transaction
        # try:
        #     token_purchase_order.token_transaction_hash = purchase_tokens(recipient_account=account,
        #                                                                   num_tokens=num_tokens)
        # except:
        #     logger.critical('Failed to purchase {0} tokens for {1}'.format(num_tokens, token_purchase_order.user),
        #                     exc_info=True)
        #     return token_purchase_order, result.transaction
        #
        # # Initiate wei transaction on blockchain
        # num_wei = token_purchase_order.num_wei
        # if num_wei > 0:
        #     try:
        #         account = get_or_create_account(token_purchase_order.user)
        #     except:
        #         logger.critical('Failed to get or create Ethereum account for {0}'.format(token_purchase_order.user),
        #                         exc_info=True)
        #         return token_purchase_order, result.transaction
        #     try:
        #         token_purchase_order.wei_transaction_hash = purchase_wei(recipient_account=account, num_wei=num_wei)
        #     except:
        #         logger.critical('Failed to purchase {0} wei for {1}'.format(num_wei, token_purchase_order.user),
        #                         exc_info=True)
        #         return token_purchase_order, result.transaction
        #
        # token_purchase_order.save()
        return token_purchase_order, result.transaction


class WeiCheckoutForm(forms.Form):
    wei_purchase_order = ModelTokenField(queryset=WeiPurchaseOrder.objects.filter(braintree_id__isnull=True),
                                         max_age=settings.WEI_PURCHASE_ORDER_TIMEOUT)
    nonce = forms.CharField(widget=forms.HiddenInput())

    def checkout(self):
        assert self.is_valid()
        wei_purchase_order = self.cleaned_data.get('wei_purchase_order')

        result = braintree.Transaction.sale({
            'amount': wei_purchase_order.total.amount,
            'order_id': 'wyre.ethereum.models.WeiPurchaseOrder:{0}'.format(wei_purchase_order.id),
            'payment_method_nonce': self.cleaned_data.get('nonce'),
            'options': {
                'submit_for_settlement': True,
                'store_in_vault_on_success': True
            }
        })
        assert result.is_success, result.message
        wei_purchase_order.braintree_id = result.transaction.id
        wei_purchase_order.save()
        wei_purchase_order.user.profile.braintree_id = result.transaction.customer_details.id
        wei_purchase_order.user.profile.save()

        # # Initiate wei transaction on blockchain
        # num_wei = wei_purchase_order.num_wei
        # if num_wei > 0:
        #     try:
        #         account = get_or_create_account(wei_purchase_order.user)
        #     except:
        #         logger.critical('Failed to get or create Ethereum account for {0}'.format(wei_purchase_order.user),
        #                         exc_info=True)
        #         return wei_purchase_order, result.transaction
        #     try:
        #         wei_purchase_order.transaction_hash = purchase_wei(recipient_account=account, num_wei=num_wei)
        #     except:
        #         logger.critical('Failed to purchase {0} wei for {1}'.format(num_wei, wei_purchase_order.user),
        #                         exc_info=True)
        #         return wei_purchase_order, result.transaction
        #
        # wei_purchase_order.save()
        return wei_purchase_order, result.transaction


class TransferTokensForm(IntentFormMixin, forms.Form):
    num_tokens = forms.DecimalField(max_digits=40, decimal_places=settings.ERC20_TOKEN_NUM_DECIMAL_PLACES,
                                    min_value=from_natural_value(1))
    recipient = forms.CharField()
    sender = forms.ModelChoiceField(queryset=User.objects.all())

    def __init__(self, platform=None, **kwargs):
        super().__init__(**kwargs)
        self.platform = platform
        assert self.platform is not None

    def clean(self):
        cleaned_data = super().clean()
        sender = cleaned_data.get('sender')
        num_tokens = cleaned_data.get('num_tokens')
        if sender and num_tokens:
            if num_tokens > get_token_balance(account=sender.account):
                self.add_error('num_tokens', forms.ValidationError('Invalid number of tokens'))
        return cleaned_data

    def clean_recipient(self):
        recipient = self.cleaned_data.get('recipient')
        if self.platform == Platform.TWITTER:
            match_object = re.search(r'@(\w{1,15})', recipient)
            if not match_object:
                raise forms.ValidationError('Please enter a valid Twitter screen name')
            recipient = match_object.groups()[0]
            api = TwitterApi(
                consumer_key=settings.TWITTER_PUBLIC_CONSUMER_KEY,
                consumer_secret=settings.TWITTER_PRIVATE_CONSUMER_KEY,
                access_token_key=settings.TWITTER_PUBLIC_ACCESS_KEY,
                access_token_secret=settings.TWITTER_PRIVATE_ACCESS_KEY
            )
            try:
                user = memoize('twitter_api', timeout=3600)(api.GetUser)(screen_name=recipient)
                assert user
                assert user.id
            except Exception:
                logger.exception('Failed to find user')
                raise forms.ValidationError('Please enter a valid Twitter screen name')

        return recipient

    def transfer(self):
        assert self.is_valid()
        if self.platform == Platform.TWITTER:
            api = TwitterApi(
                consumer_key=settings.TWITTER_PUBLIC_CONSUMER_KEY,
                consumer_secret=settings.TWITTER_PRIVATE_CONSUMER_KEY,
                access_token_key=settings.TWITTER_PUBLIC_ACCESS_KEY,
                access_token_secret=settings.TWITTER_PRIVATE_ACCESS_KEY
            )
            twitter_user = memoize('twitter_api', timeout=3600)(api.GetUser)(
                screen_name=self.cleaned_data.get('recipient')
            )
            queryset = Profile.objects.filter(twitter_id=twitter_user.id)
            if not queryset.exists():
                recipient = User.objects.create_user(
                    email_address='success+{0}@simulator.amazonses.com'.format(uuid.uuid4().hex)
                )
                profile = Profile.objects.create(
                    twitter_id=twitter_user.id,
                    twitter_screen_name=twitter_user.screen_name,
                    user=recipient
                )
                logger.info('Created user with twitter id {0}'.format(profile.twitter_id))
            else:
                recipient = queryset.first().user
            recipient.profile.twitter_screen_name = twitter_user.screen_name
            recipient.profile.save()

            logger.info('Transferring {0} tokens from @{1} to @{2}'.format(
                self.cleaned_data.get('num_tokens'),
                self.cleaned_data.get('sender').profile.twitter_screen_name,
                recipient.profile.twitter_screen_name
            ))

            token_transfer = TokenTransfer.objects.create(
                sender=self.cleaned_data.get('sender'),
                recipient=recipient,
                num_tokens=self.cleaned_data.get('num_tokens')
            )
            # try:
            #    token_transfer.submit_transaction()
            # except:
            #    logger.exception('Failed to transfer tokens')
            #    pass

            return token_transfer.transaction_hash

    def is_fulfilled(self):
        is_valid = self.is_valid()
        estimated_num_transactions = get_estimated_num_transactions(
            account=get_or_create_account(user=self.cleaned_data.get('sender'))
        )
        return is_valid and estimated_num_transactions > settings.MIN_ESTIMATED_NUM_TRANSACTIONS

    def get_field_to_elicit(self):
        fields = [
            'recipient',
            'num_tokens'
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None


class WithdrawTokensForm(IntentFormMixin, forms.Form):
    num_tokens = forms.DecimalField(max_digits=40, decimal_places=settings.ERC20_TOKEN_NUM_DECIMAL_PLACES,
                                    min_value=from_natural_value(1))
    to_address = EthereumAccountAddressField()
    user = forms.ModelChoiceField(queryset=User.objects.all())
    do_confirm = YesNoField()

    def clean(self):
        cleaned_data = super().clean()
        user = cleaned_data.get('user')
        num_tokens = cleaned_data.get('num_tokens')
        if user and num_tokens:
            if num_tokens > get_token_balance(account=user.account):
                self.add_error('num_tokens', forms.ValidationError('Invalid number of tokens'))
        return cleaned_data

    def withdraw(self):
        assert self.is_valid()
        logger.info('Transferring {0} tokens from @{1} to {2}'.format(
            self.cleaned_data.get('num_tokens'),
            self.cleaned_data.get('user').profile.twitter_screen_name,
            self.cleaned_data.get('to_address')
        ))

        TokenWithdrawal.objects.create(
            user=self.cleaned_data.get('user'),
            to_address=self.cleaned_data.get('to_address'),
            num_tokens=self.cleaned_data.get('num_tokens')
        )
        # try:
        #    token_withdrawal.submit_transaction()
        # except:
        #    logger.exception('Failed to withdraw tokens')
        #    pass

        # return token_withdrawal.transaction_hash

    def is_fulfilled(self):
        is_valid = self.is_valid()
        estimated_num_transactions = get_estimated_num_transactions(
            account=get_or_create_account(user=self.cleaned_data.get('user'))
        )
        return is_valid and self.cleaned_data.get('do_confirm',
                                                  False) and estimated_num_transactions > settings.MIN_ESTIMATED_NUM_TRANSACTIONS

    def get_field_to_elicit(self):
        fields = [
            'num_tokens',
            'to_address',
            'do_confirm',
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None


class WithdrawWeiForm(IntentFormMixin, forms.Form):
    num_ether = forms.DecimalField(max_digits=40, decimal_places=18, min_value=Web3.fromWei(1, 'ether'))
    to_address = EthereumAccountAddressField()
    user = forms.ModelChoiceField(queryset=User.objects.all())
    do_confirm = YesNoField()

    def clean(self):
        cleaned_data = super().clean()
        user = cleaned_data.get('user')
        num_ether = cleaned_data.get('num_ether')
        if user and num_ether:
            if num_ether > get_ether_balance(account=user.account):
                self.add_error('num_ether', forms.ValidationError('Invalid number of ether'))
        return cleaned_data

    def withdraw(self):
        assert self.is_valid()
        num_wei = Web3.toWei(self.cleaned_data.get('num_ether'), 'ether')
        logger.info('Transferring {0} wei from @{1} to {2}'.format(
            num_wei,
            self.cleaned_data.get('user').profile.twitter_screen_name,
            self.cleaned_data.get('to_address')
        ))

        WeiWithdrawal.objects.create(
            user=self.cleaned_data.get('user'),
            to_address=self.cleaned_data.get('to_address'),
            num_wei=num_wei
        )
        # try:
        #    wei_withdrawal.submit_transaction()
        # except:
        #    logger.exception('Failed to withdraw wei')
        #    pass
        # return wei_withdrawal.transaction_hash

    def is_fulfilled(self):
        is_valid = self.is_valid()
        estimated_num_transactions = get_estimated_num_transactions(
            account=get_or_create_account(user=self.cleaned_data.get('user'))
        )
        return is_valid and self.cleaned_data.get('do_confirm',
                                                  False) and estimated_num_transactions > settings.MIN_ESTIMATED_NUM_TRANSACTIONS

    def get_field_to_elicit(self):
        fields = [
            'num_ether',
            'to_address',
            'do_confirm',
        ]
        for field in fields:
            if field in self.errors:
                return field
        return None
