from __future__ import absolute_import

import logging
import uuid

import base64
from django.conf import settings
from django.core.management.base import BaseCommand
from web3 import Web3, HTTPProvider

from ...utils import generate_key_pair
from ...utils import get_public_key

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-p', '--passphrase')
        parser.add_argument('-k', '--private_key')

    def handle(self, *args, **options):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))

        passphrase = options.get('passphrase')
        if not passphrase:
            passphrase = uuid.uuid4().hex
            public_key, private_key = generate_key_pair()
            address = web3.personal.importRawKey(private_key, passphrase)
        else:
            private_key = base64.b64decode(options.get('private_key'))
            public_key = get_public_key(private_key=private_key)
            address = web3.personal.importRawKey(private_key, passphrase)

        self.stdout.write('public_key = {0}'.format(base64.b64encode(public_key).decode('utf-8')))
        self.stdout.write('private_key = {0}'.format(base64.b64encode(private_key).decode('utf-8')))
        self.stdout.write('passphrase = {0}'.format(passphrase))
        self.stdout.write('address = {0}'.format(address))
