from __future__ import absolute_import

import json
import logging
from decimal import Decimal
from time import sleep

from django.conf import settings
from django.core.management.base import BaseCommand
from web3 import Web3, HTTPProvider

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o', '--owner', required=True)
        parser.add_argument('-p', '--passphrase', required=True)
        parser.add_argument('-s', '--spender', required=True)
        parser.add_argument('-n', '--num_tokens', required=True, type=int)

    def handle(self, *args, **options):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))

        web3.personal.unlockAccount(options.get('owner'), options.get('passphrase'))
        contract = web3.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
        transaction = {
            'from': options.get('owner')
        }
        transaction_hash = contract.transact(transaction).approve(
            options.get('spender'),
            int(Decimal(options.get('num_tokens') * 10**settings.ERC20_TOKEN_NUM_DECIMAL_PLACES))
        )
        transaction_hashes = {
            'default': transaction_hash
        }

        # Wait for transactions...?
        logger.info('transaction_hashes = {0}'.format(json.dumps(transaction_hashes, indent=2)))
        receipts = {}
        while len(receipts.keys()) != len(transaction_hashes.keys()):
            for key, transaction_hash in transaction_hashes.items():
                if key in receipts:
                    continue
                receipt = web3.eth.getTransactionReceipt(transaction_hash)
                if receipt:
                    # logger.info('receipt[{0}] = {1}'.format(key, receipt))
                    receipts[key] = receipt
            logger.info('Waiting for {0} receipts'.format(len(transaction_hashes.keys()) - len(receipts.keys())))
            sleep(5)

        logger.info('receipts = {0}'.format(receipts))
