from __future__ import absolute_import

import csv
import logging
import math
from decimal import Decimal

import re
import requests
from django.core.management.base import BaseCommand
from io import StringIO
from moneyed import Money
from web3 import Web3

from ...models import PresaleTokenPurchaseOrder

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-u', '--url', required=True)
        parser.add_argument('--token-price', type=float, default=0.001)

    def handle(self, *args, **options):
        response = requests.get(options.get('url'))
        field_names = [
            'timestamp',
            'full_name',
            'alias',
            'email_address',
            'phone_number',
            'payment_method',
            'payment_source_transaction_number',
            'token_charge',
            'num_ether',
            'ethereum_address',
            'referral',
            'comments',
            'did_agree_to_terms_of_service'
        ]
        csv_reader = csv.DictReader(StringIO(response.text), field_names)

        rows = list()
        for i, row in enumerate(csv_reader):
            if i <= 0:
                continue
            if not row.get('token_charge'):
                continue
            try:
                if not Web3.isAddress(row.get('ethereum_address')):
                    continue
            except Exception:
                continue
            cleaned_row = dict(row)

            # Clean charge
            cleaned_row['token_charge'] = Money(amount=re.sub(r'[^\d\.]', '', row['token_charge']), currency='USD')

            # Clean ethereum_address
            cleaned_row['ethereum_address'] = Web3.toChecksumAddress(row['ethereum_address'])

            # Calculate num_tokens
            cleaned_row['num_tokens'] = math.ceil(cleaned_row['token_charge'].amount / Decimal(options.get('token_price')))

            rows.append(cleaned_row)

        for row in rows:
            PresaleTokenPurchaseOrder.objects.create(
                full_name=row['full_name'],
                email_address=row['email_address'],
                address=row['ethereum_address'],
                num_tokens=row['num_tokens'],
                token_charge=row['token_charge']
            )
