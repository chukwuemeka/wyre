from __future__ import absolute_import

import json
import logging
import time

import base64
from django.conf import settings
from django.core.management.base import BaseCommand
from web3 import Web3, HTTPProvider

# 0x956da1ea7d8eb66b532fc1b7173db6c0c2499932
# zR+cZdcv6+KHY+Gl8i/tDcYaCkf5Qxs7LhPK0FI6W80=
# b6017bf9b29141ef8a97d73781a37ed1

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o', '--owner', required=True)
        parser.add_argument('-k', '--private_key')
        parser.add_argument('-p', '--passphrase', required=True)
        parser.add_argument('-f', '--json_file_name', required=True)
        parser.add_argument('-t', '--token_name', required=True)
        parser.add_argument('-s', '--token_symbol', required=True)
        parser.add_argument('-d', '--decimal_units', type=int, default=18)
        parser.add_argument('-i', '--initial_supply', default=10000000000, type=int)

    def handle(self, *args, **options):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))

        # Owner
        owner = options.get('owner')
        passphrase = options.get('passphrase')
        private_key = options.get('private_key')
        if private_key:
            try:
                owner = web3.personal.importRawKey(base64.b64decode(private_key), passphrase)
            except ValueError as e:
                if e.args[0].get('message') != 'account already exists':
                    raise e

        web3.personal.unlockAccount(owner, passphrase)
        logger.info('owner = {0}'.format(owner))
        logger.info('passphrase = {0}'.format(passphrase))
        logger.info('balance = {0}'.format(web3.eth.getBalance(owner)))

        with open(options.get('json_file_name')) as json_file:
            data = json.load(json_file)
        transaction_hashes = {}
        for key, value in data.get('contracts').items():
            logger.info('Processing {0}'.format(key))
            contract = web3.eth.contract(
                abi=json.loads(value.get('abi')),
                # asm=value.get('asm'),
                bytecode='0x{0}'.format(value.get('bin')),
                # bytecode_runtime='0x{0}'.format(value.get('bin-runtime')),
                # clone_bin='0x{0}'.format(value.get('clone-bin')),
                # dev_doc=value.get('devdoc'),
                # metadata=value.get('metadata'),
                # opcodes=value.get('opcodes'),
                # src_map=value.get('srcmap'),
                # src_map_runtime=value.get('srcmap-runtime'),
                # user_doc=value.get('userdoc')
            )
            logger.info('contract = {0}'.format(contract))
            initial_supply = Web3.toWei(options.get('initial_supply'), 'ether')
            logger.info('initial_supply = {0}'.format(initial_supply))
            if options.get('token_name').lower() in key.lower():
                args = [
                    initial_supply,
                    options.get('token_name'),
                    options.get('decimal_units'),
                    options.get('token_symbol')
                ]
                logger.info('args = {0}'.format(args))
                transaction_hash = contract.deploy({
                    'from': owner
                }, args=args)
                transaction_hashes[key] = transaction_hash
                logger.info('Deployed {0} @ {1}'.format(key, transaction_hash))
        web3.personal.lockAccount(owner)

        # Wait for transactions...?
        logger.info('transaction_hashes = {0}'.format(json.dumps(transaction_hashes, indent=2)))
        receipts = {}
        while len(receipts.keys()) != len(transaction_hashes.keys()):
            for key, transaction_hash in transaction_hashes.items():
                if key in receipts:
                    continue
                receipt = web3.eth.getTransactionReceipt(transaction_hash)
                if receipt:
                    # logger.info('receipt[{0}] = {1}'.format(key, receipt))
                    receipts[key] = receipt
            logger.info('Waiting for {0} receipts'.format(len(transaction_hashes.keys()) - len(receipts.keys())))
            time.sleep(5)

        logger.info('{0}'.format(receipts))
