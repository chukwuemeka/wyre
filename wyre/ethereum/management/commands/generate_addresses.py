from __future__ import absolute_import

import logging

import yaml
from django.core.management.base import BaseCommand

from ...utils import generate_key_pair, get_address

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        addresses = []
        for i in range(10000):
            addresses.append('{0}'.format(get_address(public_key=generate_key_pair()[0])))
            if i % 100 == 0:
                logger.debug('Generated {0} addresses...'.format(i))
        logger.info(yaml.dump({
            'slot_types': {
                'ethereum_address': {
                    'name': 'EthereumAddress',
                    'enumeration_values': addresses
                }
            }
        }, default_flow_style=False))
