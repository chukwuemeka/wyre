from __future__ import absolute_import

import logging
import time

from django.conf import settings
from django.core.management.base import BaseCommand
from web3 import Web3, HTTPProvider

# 0x956da1ea7d8eb66b532fc1b7173db6c0c2499932
# zR+cZdcv6+KHY+Gl8i/tDcYaCkf5Qxs7LhPK0FI6W80=
# b6017bf9b29141ef8a97d73781a37ed1

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('transaction_hash')

    def handle(self, *args, **options):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))

        while True:
            transaction_receipt = web3.eth.getTransactionReceipt(options.get('transaction_hash'))
            if transaction_receipt:
                # logger.info('receipt[{0}] = {1}'.format(key, receipt))
                break
            logger.info('Waiting for {0}'.format(options.get('transaction_hash')))
            time.sleep(5)

        logger.info('transaction_receipt = {0}'.format(transaction_receipt))
