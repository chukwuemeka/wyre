from __future__ import absolute_import

import logging
from time import sleep

from aws_xray_sdk.core import xray_recorder
from django.core.management.base import BaseCommand
from web3.utils.filters import BlockFilter as DefaultBlockFilter

from ...utils import client
from ....ethereum import tasks
from ....xray.decorators import trace

logger = logging.getLogger(__name__)


class BlockFilter(DefaultBlockFilter):
    def __init__(self, web3):
        filter_id = web3.manager.request_blocking(
            'eth_newBlockFilter', [],
        )
        super().__init__(web3, filter_id)

    def format_entry(self, entry):
        if not isinstance(entry, bytes):
            return entry
        return self.web3.toHex(entry)


@trace('block_listener')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_block'))
def process_block(block_hash, trace_id=None):
    logger.info('Received block_hash {0}'.format(block_hash))
    tasks.process_block.apply_async(
        args=(block_hash,),
        kwargs=dict(trace_id=trace_id),
        MessageGroupId='default',
        MessageDeduplicationId=block_hash
    )


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--max-num-iterations', type=int, dest='max_num_iterations', default=None)

    def handle(self, *args, **options):
        block_filter = BlockFilter(client)
        logger.debug('block_filter = {0}'.format(block_filter))
        max_iterations = options.get('max_num_iterations')
        num_iterations = 0
        while not max_iterations or num_iterations < max_iterations:
            logger.info('node_info = {0}'.format(client.admin.nodeInfo))
            for log_entry in block_filter.get_new_entries():
                logger.info('log_entry = {}'.format(log_entry))
                process_block(log_entry)
            sleep(5)
            num_iterations += 1
