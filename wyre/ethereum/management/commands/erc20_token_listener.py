from __future__ import absolute_import

import logging
from time import sleep

from aws_xray_sdk.core import xray_recorder
from django.conf import settings
from django.core.management.base import BaseCommand

from ...utils import client
from ...utils import signatures, Event
from ....ethereum import tasks
from ....xray.decorators import trace

logger = logging.getLogger(__name__)


@trace('erc20_token_listener')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_log'))
def process_log(log, trace_id=None):
    logger.info('Received log {0}'.format(log))
    group_id = 'default'
    topics = log.get('topics')
    logger.debug('topics = {0}'.format(topics))
    if topics and len(topics) == 3:
        signature = signatures.get(topics[0])
        logger.debug('signature = {0}'.format(signature))
        if signature == Event.TRANSFER:
            group_id = client.toHex(client.toInt(log.get('topics')[2]))
    logger.debug('MessageGroupId = {0}'.format(group_id))
    logger.debug('MessageDeduplicationId = {0}'.format(log.get('transactionHash')))
    tasks.process_log.apply_async(
        args=(log,),
        kwargs=dict(trace_id=trace_id),
        MessageGroupId=group_id,
        MessageDeduplicationId=client.toHex(log.get('transactionHash'))
    )


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-c', '--contract_address', default=settings.ERC20_TOKEN_ADDRESS)
        parser.add_argument('-m', '--max-num-iterations', type=int, dest='max_num_iterations', default=None)

    def handle(self, *args, **options):
        max_num_iterations = options.get('max_num_iterations')
        num_iterations = 0
        log_filter = client.eth.filter({
            'fromBlock': 0,
            'address': options.get('contract_address')
        })
        logger.debug('log_filter = {0}'.format(log_filter))

        while not max_num_iterations or num_iterations < max_num_iterations:
            logger.info('node_info = {0}'.format(client.admin.nodeInfo))
            for log_entry in log_filter.get_new_entries():
                process_log(log_entry)
            sleep(5)
            num_iterations += 1
