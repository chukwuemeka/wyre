from __future__ import absolute_import

import logging

from django.conf import settings
from django.core.management.base import BaseCommand
from web3 import Web3, HTTPProvider

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o', '--owner', required=True)
        parser.add_argument('-s', '--spender', required=True)

    def handle(self, *args, **options):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))

        contract = web3.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
        allowance = contract.call().allowance(
            options.get('owner'),
            options.get('spender')
        )

        logger.info('allowance = {0}'.format(allowance))
