from __future__ import absolute_import

import logging
import uuid

from django.conf import settings
from django.core.management.base import BaseCommand
from web3 import Web3, HTTPProvider

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-k', '--private_key', required=True)

    def handle(self, *args, **options):
        web3 = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))

        passphrase = uuid.uuid4().hex
        address = web3.personal.importRawKey(options.get('private_key'), passphrase)

        self.stdout.write('private_key = {0}'.format(options.get('private_key')))
        self.stdout.write('passphrase = {0}'.format(passphrase))
        self.stdout.write('address = {0}'.format(address))
