from web3.gas_strategies.rpc import rpc_gas_price_strategy
from web3.gas_strategies.time_based import fast_gas_price_strategy
from web3.gas_strategies.time_based import medium_gas_price_strategy


def fail_safe_gas_price_strategy(web3, transaction_params=None, **kwargs):
    try:
        return fast_gas_price_strategy(web3, transaction_params=transaction_params)
    except Exception:
        pass
    try:
        return medium_gas_price_strategy(web3, transaction_params=transaction_params)
    except Exception:
        pass
    return rpc_gas_price_strategy(web3, transaction_params=transaction_params)
