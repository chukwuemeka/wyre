import json
import logging
import uuid
from datetime import timedelta
from decimal import Decimal

import base64
import functools
import requests
import sha3
from aws_xray_sdk.core import xray_recorder
from coinbase.wallet.client import Client
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import caches
from django.db.models import Avg
from django.utils import timezone
from djmoney.money import Money
from ecdsa import SigningKey, SECP256k1
from enum import Enum, auto
from eth_utils import to_checksum_address
from web3 import Web3, HTTPProvider
from web3.middleware.pythonic import (
    pythonic_middleware,
    to_hexbytes,
)
from web3.utils.transactions import get_buffered_gas_estimate

from .gas_strategies import fail_safe_gas_price_strategy
from .middleware.cache import simple_django_cache_middleware, latest_block_based_django_cache_middleware, \
    time_based_django_cache_middleware
from .models import Account, UsdToWeiExchangeRate
from ..decorators import lock
from ..decorators import memoize

User = get_user_model()
client = Web3(HTTPProvider('http://{0}:{1}'.format(settings.GETH_HOST, settings.GETH_PORT)))
size_extra_data_for_poa = 200
pythonic_middleware.__closure__[2].cell_contents['eth_getBlockByNumber'].args[1].args[0]['extraData'] = to_hexbytes(
    size_extra_data_for_poa, variable_length=True)
pythonic_middleware.__closure__[2].cell_contents['eth_getBlockByHash'].args[1].args[0]['extraData'] = to_hexbytes(
    size_extra_data_for_poa, variable_length=True)
client.eth.setGasPriceStrategy(fail_safe_gas_price_strategy)
client.middleware_stack.add(time_based_django_cache_middleware)
client.middleware_stack.add(latest_block_based_django_cache_middleware)
client.middleware_stack.add(simple_django_cache_middleware)

logger = logging.getLogger(__name__)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'is_base64'))
def is_base64(s):
    try:
        if base64.b64encode(base64.b64decode(s)) == s:
            return True
    except Exception:
        pass
    return False


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_public_key'))
def get_public_key(private_key):
    private_key = SigningKey.from_string(private_key, curve=SECP256k1)
    return private_key.get_verifying_key().to_string()


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_address'))
def get_address(*, private_key=None, public_key=None):
    if private_key:
        if isinstance(private_key, str) and private_key.startswith('0x'):
            private_key = client.toBytes(hexstr=private_key)
        public_key = get_public_key(private_key)
    assert public_key
    keccak = sha3.keccak_256()
    keccak.update(public_key)
    address = keccak.hexdigest()[24:]
    return to_checksum_address('0x{0}'.format(address))


@xray_recorder.capture('{0}.{1}'.format(__name__, 'generate_key_pair'))
def generate_key_pair():
    private_key = SigningKey.generate(curve=SECP256k1)
    public_key = private_key.get_verifying_key().to_string()
    # logger.debug('private_key = {0}'.format(private_key))
    # logger.debug('public_key = {0}'.format(public_key))
    return public_key, private_key.to_string()


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_or_create_account'))
def get_or_create_account(user):
    queryset = Account.objects.filter(user=user)
    if queryset.exists():
        account = queryset.first()
    else:
        passphrase = uuid.uuid4().hex
        public_key, private_key = generate_key_pair()
        account = Account.objects.create(
            user=user,
            private_key=client.toHex(private_key),
            passphrase=passphrase,
            address=get_address(public_key=public_key)
        )
    return account


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_allowance'))
def get_allowance(*, owner=None, spender=None):
    assert owner
    assert spender
    contract = client.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
    allowance = contract.functions.allowance(owner, spender).call()
    return allowance


@xray_recorder.capture('{0}.{1}'.format(__name__, 'purchase_tokens'))
@lock('{0}.{1}.{2}'.format(
    __name__,
    'web3_transaction',
    get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
), retry_count=100, retry_delay=0.5)
def purchase_tokens(*, recipient_address=None, recipient_account=None, num_tokens=None, do_commit=True):
    if recipient_account:
        recipient_address = recipient_account.address
    agent_address = get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
    assert recipient_address
    assert num_tokens
    allowance = get_allowance(owner=settings.ERC20_TOKEN_OWNER_ADDRESS, spender=agent_address)
    natural_value = int(to_natural_value(num_tokens))
    assert allowance >= natural_value, 'Allowance ({0}) is too small to transfer {1}'.format(allowance, natural_value)
    contract = client.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
    nonce = get_next_nonce(agent_address)
    gas_price = get_gas_price()
    transaction = contract.functions.transferFrom(
        settings.ERC20_TOKEN_OWNER_ADDRESS,
        recipient_address,
        natural_value
    ).buildTransaction({
        'from': agent_address,
        'nonce': nonce,
        'gasPrice': gas_price
    })
    signed_transaction = client.eth.account.signTransaction(transaction, settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)

    if not do_commit:
        logger.info('purchase_tokens({0}, {1}) = n/a'.format(recipient_address, num_tokens))
        return

    try:
        transaction_hash = client.toHex(client.eth.sendRawTransaction(signed_transaction.rawTransaction))
    except Exception:
        transaction_pool = client.txpool.inspect
        logger.exception('Failed to send raw transaction'.format(transaction_pool))
        raise
    logger.info('purchase_tokens({0}, {1}) = {2}'.format(recipient_address, num_tokens, transaction_hash))
    set_last_nonce(agent_address, nonce)
    return transaction_hash


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_last_nonce'))
def get_last_nonce(address):
    key = '{}.{}.{}.last_nonce'.format(__name__, 'get_last_nonce', address)
    return caches['web3'].get(key)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'set_last_nonce'))
def set_last_nonce(address, nonce):
    key = '{}.{}.{}.last_nonce'.format(__name__, 'get_last_nonce', address)
    return caches['web3'].set(key, nonce, timeout=5)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_next_nonce'))
def get_next_nonce(address):
    transaction_pool = client.txpool.inspect
    max_pending_nonce = functools.reduce(
        max,
        [int(k) for k in transaction_pool.get('pending', {}).get(to_checksum_address(address), {}).keys()],
        -1
    )
    max_queued_nonce = functools.reduce(
        max,
        [int(k) for k in transaction_pool.get('queued', {}).get(to_checksum_address(address), {}).keys()],
        -1
    )
    last_nonce = get_last_nonce(address) or -9000
    nonce = max(max(0, client.eth.getTransactionCount(address)) - 1, max_pending_nonce, max_queued_nonce,
                last_nonce) + 1
    return nonce


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_estimated_num_transactions'))
def get_estimated_num_transactions(*, account=None, address=None):
    if account:
        address = account.address
    assert address
    gas_estimate = get_token_transfer_gas_estimate()
    gas_price = get_gas_price()
    logger.info('gas_price = {0}'.format(gas_price))
    return round(get_wei_balance(address=address) / (gas_price * gas_estimate))


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_wei_balance'))
def get_wei_balance(*, account=None, address=None):
    if account:
        address = account.address
    assert address
    return client.eth.getBalance(address)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_ether_balance'))
def get_ether_balance(*, account=None, address=None):
    return Web3.fromWei(get_wei_balance(account=account, address=address), 'ether')


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_token_balance'))
def get_token_balance(*, account=None, address=None):
    if account:
        address = account.address
    assert address
    contract = client.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
    logger.info('balance of {0} = {1}'.format(address, client.eth.getBalance(address)))
    balance = contract.functions.balanceOf(address).call()
    logger.debug('balance_of({0}) = {1}'.format(address, balance))
    return from_natural_value(balance)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'transfer_wei'))
@lock('{0}.{1}.{2}'.format(
    __name__,
    'web3_transaction',
    get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
), retry_count=100, retry_delay=0.5)
def transfer_wei(*, from_account=None, from_address=None, from_private_key=None, to_address=None, num_wei=None,
                 nonce=None, gas_price=None):
    if from_account:
        from_address = from_account.address
        from_private_key = from_account.private_key
    assert from_address
    assert from_private_key
    assert isinstance(int(num_wei), int)
    assert to_address
    nonce = nonce or get_next_nonce(from_address)
    gas_price = gas_price or get_gas_price()
    chain_id = get_chain_id()
    transaction = {
        'from': from_address,
        'to': to_address,
        'value': int(num_wei),
        'nonce': nonce,
        'gasPrice': gas_price,
        'chainId': chain_id
    }
    transaction['gas'] = get_buffered_gas_estimate(client, transaction)
    signed_transaction = client.eth.account.signTransaction(transaction, from_private_key)
    try:
        transaction_hash = client.toHex(client.eth.sendRawTransaction(signed_transaction.rawTransaction))
    except Exception:
        transaction_pool = client.txpool.inspect
        logger.exception('Failed to send raw transaction'.format(transaction_pool))
        raise
    logger.info('transfer_wei({0}, {1}, {2}) = {3}'.format(from_address, to_address, num_wei, transaction_hash))
    set_last_nonce(from_address, nonce)
    return transaction_hash


@xray_recorder.capture('{0}.{1}'.format(__name__, 'transfer_tokens'))
def transfer_tokens(*args, **kwargs):
    assert kwargs.get('from_account')
    assert kwargs.get('from_account').address

    @lock(
        '{0}.{1}.{2}'.format(__name__, 'web3_transaction', kwargs.get('from_account').address),
        retry_count=100,
        retry_delay=0.5
    )
    def _transfer_tokens(*, from_account=None, to_account=None, to_address=None, num_tokens=None):
        assert from_account
        if to_account:
            to_address = to_account.address
        assert num_tokens
        assert to_address
        natural_value = to_natural_value(num_tokens)
        contract = client.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
        nonce = get_next_nonce(from_account.address)
        transaction = contract.functions.transfer(
            to_address,
            natural_value
        ).buildTransaction({
            'from': from_account.address,
            'nonce': nonce,
            'gasPrice': get_gas_price()
        })
        signed_transaction = client.eth.account.signTransaction(transaction, from_account.private_key)
        transaction_hash = client.toHex(client.eth.sendRawTransaction(signed_transaction.rawTransaction))
        logger.info(
            'transfer({0}, {1}, {2}) = {3}'.format(from_account.address, to_address, num_tokens, transaction_hash))
        set_last_nonce(from_account.address, nonce)
        return transaction_hash

    return _transfer_tokens(*args, **kwargs)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'to_natural_value'))
def to_natural_value(num_tokens):
    return int(Decimal(num_tokens * (10 ** settings.ERC20_TOKEN_NUM_DECIMAL_PLACES)))


@xray_recorder.capture('{0}.{1}'.format(__name__, 'from_natural_value'))
def from_natural_value(natural_value):
    return Decimal(natural_value / (10 ** settings.ERC20_TOKEN_NUM_DECIMAL_PLACES))


@xray_recorder.capture('{0}.{1}'.format(__name__, 'purchase_wei'))
def purchase_wei(*, recipient_address=None, recipient_account=None, num_wei=None):
    if recipient_account:
        recipient_address = recipient_account.address
    agent_address = get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
    assert recipient_address
    assert num_wei
    assert int(num_wei)
    return transfer_wei(
        from_address=agent_address,
        from_private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY,
        to_address=recipient_address,
        num_wei=num_wei
    )


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_etherscan_url'))
def get_etherscan_url(*, private_key=None, address=None):
    if private_key:
        address = get_address(private_key=private_key)
    if address:
        return '{0}/address/{1}'.format(settings.ETHERSCAN_DOMAIN, address)
    raise Exception


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_usd_to_wei_exchange_rate_from_gemini'))
def get_usd_to_wei_exchange_rate_from_gemini():
    base_url = 'https://api.gemini.com/v1'
    url = '{0}/pubticker/ethusd'.format(base_url)
    response = memoize('gemini_exchange_rate', timeout=1)(requests.get)(url, timeout=2)
    assert response.ok, 'Failed to fetch exchange rate via Gemini'
    exchange_rate = Web3.toWei(1, 'ether') / Decimal(response.json().get('last'))
    logger.info('gemini_exchange_rate = {0}'.format(exchange_rate))
    return exchange_rate


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_usd_to_wei_exchange_rate_from_coinbase'))
def get_usd_to_wei_exchange_rate_from_coinbase(**kwargs):
    client = Client(settings.COINBASE_PUBLIC_KEY, settings.COINBASE_PRIVATE_KEY)
    try:
        response = client.get_exchange_rates(currency='USD')
    except json.JSONDecodeError:
        return None
    exchange_rate = Decimal(Web3.toWei(Decimal(response.get('rates').get('ETH')), 'ether'))
    logger.info('coinbase_exchange_rate = {0}'.format(exchange_rate))
    if xray_recorder.current_segment():
        xray_recorder.current_segment().put_annotation('coinbase_exchange_rate', float(exchange_rate))
    elif xray_recorder.current_subsegment():
        xray_recorder.current_subsegment().put_annotation('coinbase_exchange_rate', float(exchange_rate))
    UsdToWeiExchangeRate.objects.create(
        source='coinbase',
        value=exchange_rate
    )
    return exchange_rate


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_usd_to_wei_exchange_rate'))
def get_usd_to_wei_exchange_rate():
    queryset = UsdToWeiExchangeRate.objects.filter(date_created__gte=timezone.now() - timedelta(minutes=15))
    if not queryset.exists():
        get_usd_to_wei_exchange_rate_from_coinbase()
    return min(Decimal(queryset.aggregate(average_value=Avg('value'))['average_value']),
               Web3.toWei(1.0 / 1000.0, 'ether'))
    # # Try to use Gemini
    # try:
    #     exchange_rate = get_usd_to_wei_exchange_rate_from_gemini()
    #     return exchange_rate
    # except:
    #     logger.exception('Failed to fetch exchange rate via Gemini')
    #
    # # Try to use Coinbase
    # try:
    #     exchange_rate = get_usd_to_wei_exchange_rate_from_coinbase()
    #     return exchange_rate
    # except:
    #     logger.exception('Failed to fetch exchange rate via Coinbase')
    #     raise


@xray_recorder.capture('{0}.{1}'.format(__name__, 'to_wei'))
def to_wei(*, money=None):
    assert money
    assert isinstance(money, Money)
    return money.amount * get_usd_to_wei_exchange_rate()


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_gas_price'))
def get_gas_price(**kwargs):
    return client.toWei(4, 'gwei')
    # return get_gas_price()


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_block'))
def get_block(block_identifier):
    return client.eth.getBlock(block_identifier)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_transaction'))
def get_transaction(transaction_hash):
    return client.eth.getTransaction(transaction_hash)


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_token_transfer_gas_estimate'))
@memoize('{0}.{1}'.format(__name__, 'get_token_transfer_gas_estimate'), timeout=timedelta(minutes=15).total_seconds())
def get_token_transfer_gas_estimate(**kwargs):
    contract = client.eth.contract(address=settings.ERC20_TOKEN_ADDRESS, abi=settings.ERC20_TOKEN_ABI)
    from_address = get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
    to_address = get_address(private_key=generate_key_pair()[1])
    transaction = {
        'from': from_address
    }
    return contract.estimateGas(transaction).transferFrom(
        settings.ERC20_TOKEN_OWNER_ADDRESS,
        to_address,
        50
    )


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_wei_transfer_gas_estimate'))
@memoize('{0}.{1}'.format(__name__, 'get_wei_transfer_gas_estimate'), timeout=timedelta(minutes=15).total_seconds())
def get_wei_transfer_gas_estimate(**kwargs):
    from_address = get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
    to_address = get_address(private_key=generate_key_pair()[1])
    return client.eth.estimateGas({
        'from': from_address,
        'to': to_address,
        'value': 1
    })


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_mean_gas_price'))
def get_mean_gas_price(*, num_blocks):
    sum_ = 0
    block_number = None
    num_transactions = 0
    for i in range(num_blocks):
        if not block_number:
            block = client.eth.getBlock('latest')
            block_number = block.number - 1
        else:
            block = get_block(block_number)
            block_number -= 1
        for transaction_hash in block.get('transactions'):
            transaction = get_transaction(transaction_hash)
            sum_ += transaction.get('gasPrice')
            num_transactions += 1
    return int(max(1, sum_) / max(1, num_transactions))


@xray_recorder.capture('{0}.{1}'.format(__name__, 'get_chain_id'))
def get_chain_id():
    return client.admin.nodeInfo['protocols']['eth']['network']


@xray_recorder.capture('{0}.{1}'.format(__name__, 'override_pending_transaction'))
def override_pending_transaction(*, address, private_key):
    assert address
    return transfer_wei(
        from_address=address,
        from_private_key=private_key,
        to_address=address,
        num_wei=0,
        nonce=client.eth.getTransactionCount(address) + 1,
        gas_price=client.toWei(20, 'gwei')
    )


class Event(Enum):
    TRANSFER = auto()


signatures = {
    Web3.sha3(hexstr=Web3.toHex(text='Transfer(address, address, uint256)'.replace(' ', ''))): Event.TRANSFER
}
