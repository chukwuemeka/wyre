from decimal import Decimal
from decimal import ROUND_HALF_UP, ROUND_CEILING
from math import ceil

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import signing
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template import loader
from django.utils import timezone
from django.utils.functional import cached_property
from djmoney.models.fields import MoneyField
from moneyed import Money
from moneyed.localization import format_money
from web3 import Web3

from ..context_processors import constants

User = get_user_model()


class AbstractModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Account(AbstractModel):
    address = models.CharField(max_length=100, unique=True)
    private_key = models.CharField(max_length=100, unique=True)
    passphrase = models.CharField(max_length=500)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        db_index=True
    )

    # @cached_property
    # def private_key(self):
    #     return base64.b64decode(self.base64_private_key)

    @cached_property
    def public_key(self):
        from .utils import get_public_key
        return get_public_key(self.private_key)


class WeiPurchaseOrderManager(models.Manager):
    def is_token_valid(self, token):
        return bool(self.from_token(token))

    def from_token(self, token):
        try:
            data = signing.loads(token, max_age=settings.WEI_PURCHASE_ORDER_TIMEOUT)
        except signing.BadSignature:
            return None
        try:
            wei_purchase_order = self.get(id=data.get('id'))
        except ObjectDoesNotExist:
            return None
        return wei_purchase_order


class WeiPurchaseOrder(AbstractModel):
    email_address = models.EmailField(max_length=500)
    num_wei = models.DecimalField(max_digits=40, decimal_places=0)
    wei_estimate = models.DecimalField(max_digits=40, decimal_places=0)
    transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    braintree_id = models.CharField(max_length=100, null=True, blank=True, unique=True)
    service_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')
    wei_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')
    did_send_notification = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    objects = WeiPurchaseOrderManager()

    def submit_transaction(self):
        from .utils import purchase_wei
        if not self.transaction_hash:
            self.transaction_hash = purchase_wei(recipient_account=self.user.account, num_wei=self.num_wei)
            self.save()

    @staticmethod
    def get_charges(*, user=None, wei_charge=None):
        from .utils import get_usd_to_wei_exchange_rate
        from .utils import get_wei_transfer_gas_estimate

        assert user
        assert user.account
        assert wei_charge

        # Token charges and wei charges
        exchange_rate = get_usd_to_wei_exchange_rate()
        scaled_exchange_rate = exchange_rate / settings.USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR
        wei_charge = Money(Decimal(wei_charge.amount).quantize(Decimal('.01'), rounding=ROUND_HALF_UP), 'USD')
        num_wei = int(ceil(wei_charge.amount * scaled_exchange_rate))

        # Gas estimate
        wei_estimate = get_wei_transfer_gas_estimate()

        # https://support.stripe.com/questions/can-i-charge-my-stripe-fees-to-my-customers
        goal = wei_charge + Money((wei_estimate / exchange_rate),
                                  'USD')  # How much money do we want deposited to our account?
        service_charge_amount = ((goal.amount + Decimal(settings.BRAINTREE_TRANSACTION_FEE)) / Decimal(
            1 - settings.BRAINTREE_TRANSACTION_RATE)) - goal.amount

        service_charge = Money(service_charge_amount.quantize(Decimal('.01'), rounding=ROUND_CEILING), 'USD')
        return service_charge, num_wei, wei_estimate

    @property
    def num_ether(self):
        return Web3.fromWei(self.num_wei, 'ether')

    @property
    def total(self):
        return self.wei_charge + self.service_charge

    @property
    def token(self):
        data = {
            'id': self.pk
        }
        return signing.dumps(data)


class TokenPurchaseOrderManager(models.Manager):
    def is_token_valid(self, token):
        return bool(self.from_token(token))

    def from_token(self, token):
        try:
            data = signing.loads(token, max_age=settings.TOKEN_PURCHASE_ORDER_TIMEOUT)
        except signing.BadSignature:
            return None
        try:
            token_purchase_order = self.get(id=data.get('id'))
        except ObjectDoesNotExist:
            return None
        return token_purchase_order


class TokenPurchaseOrder(AbstractModel):
    email_address = models.EmailField(max_length=500)
    num_tokens = models.DecimalField(max_digits=40, decimal_places=18)
    num_wei = models.DecimalField(max_digits=40, decimal_places=0)
    wei_estimate = models.DecimalField(max_digits=40, decimal_places=0)
    token_transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    wei_transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    braintree_id = models.CharField(max_length=100, null=True, blank=True, unique=True)
    token_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')
    service_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')
    wei_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')
    did_send_notification = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    objects = TokenPurchaseOrderManager()

    def submit_transactions(self):
        from .utils import purchase_tokens, purchase_wei
        if not self.token_transaction_hash:
            self.token_transaction_hash = purchase_tokens(recipient_account=self.user.account,
                                                          num_tokens=self.num_tokens)
            self.save()
        if not self.wei_transaction_hash and self.num_wei > Decimal(0):
            self.wei_transaction_hash = purchase_wei(recipient_account=self.user.account, num_wei=self.num_wei)
            self.save()

    @property
    def num_ether(self):
        return Web3.fromWei(self.num_wei, 'ether')

    @property
    def ether_estimate(self):
        return Web3.fromWei(self.wei_estimate, 'ether')

    @property
    def formatted_num_tokens(self):
        from ..utils import format_decimal
        return format_decimal(self.num_tokens)

    @staticmethod
    def get_charges(*, user=None, num_tokens=None):
        from .utils import get_usd_to_wei_exchange_rate
        from .utils import get_token_transfer_gas_estimate
        from .utils import get_wei_transfer_gas_estimate

        assert user
        assert user.account
        assert num_tokens

        # Token charges and wei charges
        token_charge = Money(
            Decimal(num_tokens * Decimal(settings.TOKEN_PRICE)).quantize(Decimal('.01'), rounding=ROUND_HALF_UP), 'USD')
        exchange_rate = get_usd_to_wei_exchange_rate()
        scaled_exchange_rate = exchange_rate / settings.USD_TO_WEI_EXCHANGE_RATE_SCALE_FACTOR
        if TokenPurchaseOrder.objects.filter(user=user).exists():
            wei_charge = Money(0, 'USD')
            num_wei = 0
        else:
            wei_charge = Money(
                Decimal(settings.DEFAULT_WEI_CHARGE.amount).quantize(Decimal('.01'), rounding=ROUND_HALF_UP), 'USD')
            num_wei = int(ceil(wei_charge.amount * scaled_exchange_rate))

        # Gas estimate
        wei_estimate = get_token_transfer_gas_estimate()
        if num_wei > 0:
            wei_estimate += get_wei_transfer_gas_estimate()

        # https://support.stripe.com/questions/can-i-charge-my-stripe-fees-to-my-customers
        goal = token_charge + wei_charge + Money((wei_estimate / exchange_rate),
                                                 'USD')  # How much money do we want deposited to our account?
        service_charge_amount = ((goal.amount + Decimal(settings.BRAINTREE_TRANSACTION_FEE)) / Decimal(
            1 - settings.BRAINTREE_TRANSACTION_RATE)) - goal.amount

        service_charge = Money(service_charge_amount.quantize(Decimal('.01'), rounding=ROUND_CEILING), 'USD')
        return token_charge, wei_charge, service_charge, num_wei, wei_estimate

    @property
    def total(self):
        return self.token_charge + self.wei_charge + self.service_charge

    @property
    def token(self):
        data = {
            'id': self.pk
        }
        return signing.dumps(data)


class TokenTransfer(AbstractModel):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_token_transfers')
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_token_transfers')
    num_tokens = models.DecimalField(max_digits=40, decimal_places=18)
    transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    did_send_notification = models.BooleanField(default=False)

    def submit_transaction(self):
        if not self.transaction_hash:
            from .utils import transfer_tokens, get_or_create_account
            self.transaction_hash = transfer_tokens(
                from_account=get_or_create_account(self.sender),
                to_account=get_or_create_account(self.recipient),
                num_tokens=self.num_tokens
            )
            self.save()


class TokenWithdrawal(AbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='token_withdrawals')
    to_address = models.CharField(max_length=100)
    num_tokens = models.DecimalField(max_digits=40, decimal_places=18)
    transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    did_send_notification = models.BooleanField(default=False)

    def submit_transaction(self):
        if not self.transaction_hash:
            from .utils import transfer_tokens
            self.transaction_hash = transfer_tokens(
                from_account=self.user.account,
                to_address=self.to_address,
                num_tokens=self.num_tokens
            )
            self.save()


class WeiWithdrawal(AbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='wei_withdrawals')
    to_address = models.CharField(max_length=100)
    num_wei = models.DecimalField(max_digits=40, decimal_places=0)
    transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)

    def submit_transaction(self):
        if not self.transaction_hash:
            from .utils import transfer_wei
            self.transaction_hash = transfer_wei(
                from_account=self.user.account,
                to_address=self.to_address,
                num_wei=self.num_wei
            )
            self.save()


class UsdToWeiExchangeRate(AbstractModel):
    source = models.CharField(max_length=100)
    value = models.DecimalField(max_digits=40, decimal_places=18)


class PresaleWeiPurchaseOrder(AbstractModel):
    full_name = models.CharField(max_length=100)
    email_address = models.EmailField(max_length=100)
    address = models.CharField(max_length=100)
    num_wei = models.DecimalField(max_digits=40, decimal_places=0)
    transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    wei_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')

    def send_receipt(self):
        assert self.transaction_hash
        context = {
            'erc20_token_symbol': settings.ERC20_TOKEN_SYMBOL,
            'ethereum_address': self.address,
            'num_ether': self.formatted_num_ether,
            'wei_charge': format_money(self.wei_charge, locale='en_US'),
            'date_created': timezone.now(),
            'transaction_url': '{0}/tx/{1}'.format(settings.ETHERSCAN_DOMAIN, self.transaction_hash)
        }
        context.update(constants(None))
        subject = ''.join(
            loader.render_to_string('ethereum/emails/presale_wei_receipt_subject.txt', context).splitlines())
        body = loader.render_to_string('ethereum/emails/presale_wei_receipt_body.txt', context)
        email_message = EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[self.email_address],
            bcc=[settings.SERVER_EMAIL, settings.SUPPORT_EMAIL]
        )
        email_message.send()

    def submit_transaction(self):
        from .utils import purchase_wei
        if not self.transaction_hash:
            self.transaction_hash = purchase_wei(recipient_address=self.address, num_wei=self.num_wei)
            self.save()

    @property
    def formatted_num_ether(self):
        from ..utils import format_decimal
        return format_decimal(self.num_ether) if self.num_ether else None

    @property
    def num_ether(self):
        return Web3.fromWei(self.num_wei, 'ether') if self.num_wei else None

    @property
    def total(self):
        return self.wei_charge


class PresaleTokenPurchaseOrder(AbstractModel):
    full_name = models.CharField(max_length=100)
    email_address = models.EmailField(max_length=100)
    address = models.CharField(max_length=100)
    num_tokens = models.DecimalField(max_digits=40, decimal_places=18)
    transaction_hash = models.CharField(max_length=200, null=True, blank=True, unique=True)
    token_charge = MoneyField(max_digits=20, decimal_places=2, default_currency='USD')

    def send_receipt(self):
        context = {
            'erc20_token_symbol': settings.ERC20_TOKEN_SYMBOL,
            'ethereum_address': self.address,
            'num_tokens': self.formatted_num_tokens,
            'token_charge': format_money(self.token_charge, locale='en_US'),
            'date_created': timezone.now()
        }
        context.update(constants(None))
        subject = ''.join(
            loader.render_to_string('ethereum/emails/presale_token_receipt_subject.txt', context).splitlines())
        body = loader.render_to_string('ethereum/emails/presale_token_receipt_body.txt', context)
        email_message = EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[self.email_address],
            bcc=[settings.SERVER_EMAIL, settings.SUPPORT_EMAIL]
        )
        email_message.send()

    def submit_transaction(self):
        from .utils import purchase_tokens
        if not self.transaction_hash:
            self.transaction_hash = purchase_tokens(recipient_address=self.address, num_tokens=self.num_tokens)
            self.save()

    @property
    def formatted_num_tokens(self):
        from ..utils import format_decimal
        return format_decimal(self.num_tokens)

    @property
    def total(self):
        return self.token_charge
