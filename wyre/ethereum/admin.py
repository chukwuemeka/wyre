from django.conf import settings
from django.contrib import admin

from .models import PresaleTokenPurchaseOrder
from .models import PresaleWeiPurchaseOrder


@admin.register(PresaleWeiPurchaseOrder)
class PresaleWeiPurchaseOrderAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'email_address', 'formatted_num_ether', 'transaction_hash', 'date_created')
    ordering = ('-date_created',)
    actions = ['submit_transaction']
    readonly_fields = [
        'formatted_num_ether',
        'num_ether'
    ]

    def submit_transaction(self, request, queryset):
        from .tasks import submit_presale_wei_purchase_order_transaction
        from .utils import get_address
        for presale_wei_purchase_order in queryset:
            submit_presale_wei_purchase_order_transaction.apply_async(
                (presale_wei_purchase_order.id,),
                MessageGroupId=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY),
                MessageDeduplicationId='PresaleWeiPurchaseOrder:{}'.format(presale_wei_purchase_order.id)
            )
        self.message_user(
            request,
            '{} presale wei purchase orders were successfully submitted.'.format(queryset.count())
        )

    submit_transaction.short_description = 'Submit transaction to Ethereum (async)'


@admin.register(PresaleTokenPurchaseOrder)
class PresaleTokenPurchaseOrderAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'email_address', 'formatted_num_tokens', 'transaction_hash', 'date_created')
    ordering = ('-date_created',)
    actions = ['submit_transaction']
    readonly_fields = [
        'formatted_num_tokens'
    ]

    def submit_transaction(self, request, queryset):
        from .tasks import submit_presale_token_purchase_order_transaction
        from .utils import get_address
        for presale_token_purchase_order in queryset:
            submit_presale_token_purchase_order_transaction.apply_async(
                (presale_token_purchase_order.id,),
                MessageGroupId=get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY),
                MessageDeduplicationId='PresaleTokenPurchaseOrder:{}'.format(presale_token_purchase_order.id)
            )
        self.message_user(
            request,
            '{} presale token purchase orders were successfully submitted.'.format(queryset.count())
        )

    submit_transaction.short_description = 'Submit transaction to Ethereum (async)'
