from importlib import import_module

import random
from aws_xray_sdk.core import xray_recorder
from celery import shared_task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from web3 import Web3

from .models import TokenTransfer
from .models import TokenWithdrawal
from .utils import get_etherscan_url, get_gas_price, get_usd_to_wei_exchange_rate_from_coinbase, \
    get_wei_transfer_gas_estimate, get_token_transfer_gas_estimate, client
from ..ethereum.models import PresaleTokenPurchaseOrder
from ..ethereum.models import PresaleWeiPurchaseOrder
from ..ethereum.models import TokenPurchaseOrder
from ..ethereum.models import WeiPurchaseOrder
from ..ethereum.utils import get_address, get_block
from ..lex.responses import Text, Status, UrlButton
from ..twitter.tasks import send_direct_messages
from ..twitter.tasks import send_statuses
from ..twitter.utils import get_default_welcome_message_id
from ..urlresolvers import get_absolute_static_url
from ..utils import format_decimal, pluralize
from ..xray.decorators import trace

ETHEREUM_TRANSACTIONS_RATE_LIMIT = '20/m'

logger = get_task_logger(__name__)

ERC20_TOKEN_AGENT_ADDRESS = get_address(private_key=settings.ERC20_TOKEN_AGENT_PRIVATE_KEY)
TWITTER_LOCUST_SCREEN_NAME_IDENTIFIER = '!!locust!!'

User = get_user_model()
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
Session = SessionStore().model


# @xray_recorder.capture('{0}.{1}'.format(__name__, 'send_messages'))
# def send_messages(user, messages):
#     profile = user.profile
#     if profile.twitter_id:
#         eta = timezone.now()
#         for message in messages:
#             pickled_message = to_pickle_string(message)
#             eta += timedelta(seconds=abs(random.gauss(1.5, 0.5)))
#             send_direct_message(
#                 pickled_message,
#                 profile.twitter_id,
#                 settings.TWITTER_PUBLIC_CONSUMER_KEY,
#                 settings.TWITTER_PRIVATE_CONSUMER_KEY,
#                 settings.TWITTER_PUBLIC_ACCESS_KEY,
#                 settings.TWITTER_PRIVATE_ACCESS_KEY
#             )
#     else:
#         logger.error('Failed to respond to user {0}'.format(user))


@shared_task(queue='Wyre{0}EthereumBlocks.fifo'.format(settings.ENVIRONMENT.capitalize()))
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_block'))
def process_block(block_hash, trace_id=None):
    def send_gas_messages(user, num_wei):
        num_ether = Web3.fromWei(num_wei, 'ether')
        messages = [
            Text('Hey there 👋'),
            Text('Just letting you know that {0} ETH (aka gas ⛽) has hit your account'.format(num_ether)),
            Text(
                'If you want to know how much gas you have right now, say something like "How much gas do I have?"'),
        ]
        send_direct_messages(messages, user)

    logger.info('Received block hash {0}'.format(block_hash))
    block = get_block(block_hash)
    logger.debug('block.number = {0}'.format(block.get('number')))
    logger.debug('block.transactions = {0}'.format(block.get('transactions')))
    for transaction_hash in block.get('transactions'):
        transaction_hash = client.toHex(transaction_hash)
        logger.debug('Processing {0}'.format(transaction_hash))
        if WeiPurchaseOrder.objects.exclude(email_address__iendswith='simulator.amazonses.com').filter(
                transaction_hash__iexact=transaction_hash).exists():
            wei_purchase_order = WeiPurchaseOrder.objects.exclude(
                email_address__icontains='simulator.amazonses.com').filter(
                transaction_hash__iexact=transaction_hash, did_send_notification=False).first()
            xray_recorder.get_trace_entity().set_user('User:{0}'.format(wei_purchase_order.user.id))
            send_gas_messages(wei_purchase_order.user, wei_purchase_order.num_wei)
        elif TokenPurchaseOrder.objects.exclude(email_address__iendswith='simulator.amazonses.com').filter(
                wei_transaction_hash__iexact=transaction_hash, did_send_notification=False).exists():
            token_purchase_order = TokenPurchaseOrder.objects.exclude(
                email_address__icontains='simulator.amazonses.com').filter(
                wei_transaction_hash__iexact=transaction_hash).first()
            xray_recorder.get_trace_entity().set_user('User:{0}'.format(token_purchase_order.user.id))
            send_gas_messages(token_purchase_order.user, token_purchase_order.num_wei)


@shared_task(queue='Wyre{0}EthereumLogs.fifo'.format(settings.ENVIRONMENT.capitalize()))
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'process_log'))
def process_log(log, trace_id=None):
    logger.info('Processing log {0}'.format(log))
    transaction_hash = client.toHex(log.get('transactionHash'))
    token_purchase_orders = TokenPurchaseOrder.objects.filter(token_transaction_hash__iexact=transaction_hash,
                                                              did_send_notification=False).exclude(
        email_address__iendswith='simulator.amazonses.com')
    token_transfers = TokenTransfer.objects.filter(transaction_hash__iexact=transaction_hash,
                                                   did_send_notification=False).exclude(
        sender__profile__twitter_screen_name__icontains=TWITTER_LOCUST_SCREEN_NAME_IDENTIFIER)
    token_withdrawals = TokenWithdrawal.objects.filter(transaction_hash__iexact=transaction_hash,
                                                       did_send_notification=False).exclude(
        user__profile__twitter_screen_name__icontains=TWITTER_LOCUST_SCREEN_NAME_IDENTIFIER)
    if token_purchase_orders.exists():
        token_purchase_order = token_purchase_orders.first()
        num_tokens = token_purchase_order.num_tokens
        user = token_purchase_order.user
        xray_recorder.get_trace_entity().set_user('User:{0}'.format(user.id))
        messages = [
            Text('Hey there 👋'),
            Text('Just letting you know that {0} {1} tokens have hit your account 💰'.format(format_decimal(num_tokens), settings.ERC20_TOKEN_SYMBOL)),
            Text('If you want to know how many {0} tokens you own, say something like "How many {0} tokens do I have?"'.format(settings.ERC20_TOKEN_SYMBOL)),
            Text('Feel free geek out 🤓 on the state of your account within Ethereum', buttons=[UrlButton(label='View My Account', url=get_etherscan_url(address=user.account.address))]),
            # Text(
            #     'Feel free geek out 🤓 on the state of your account within Ethereum by going here: {0}'.format(
            #         get_etherscan_url(address=user.account.address))),
        ]
        send_direct_messages(messages, user)
        token_purchase_order.did_send_notification = True
        token_purchase_order.save()
    elif token_transfers.exists():
        token_transfer = token_transfers.first()
        num_tokens = token_transfer.num_tokens
        recipient = token_transfer.recipient
        sender = token_transfer.sender

        assert sender.profile.twitter_screen_name, 'Failed to find twitter screen name for {0}'.format(sender)
        assert recipient.profile.twitter_screen_name, 'Failed to find twitter screen name for {0}'.format(recipient)

        # Media
        gif_filename = random.choice(['transfer_tokens_%d.gif' % i for i in range(10)])
        media_url = get_absolute_static_url('gif/%s' % gif_filename)

        # Notify sender
        messages = [
            Text('Hey there 👋'),
            Text('@{2} just received the {0} {1} {3} you sent 💸'.format(
                format_decimal(num_tokens),
                settings.ERC20_TOKEN_SYMBOL,
                recipient.profile.twitter_screen_name,
                pluralize('token', num_tokens)
            )),
        ]
        send_direct_messages(messages, sender)

        # Notify recipient
        text = """
Congrats 🙌 You just received some BAE 😍 tokens!

BTW I'm Bae 😍

My job is to help you out with purchasing and transferring BAE tokens

To get started

Try saying something like:
"I want to buy some BAE"
"I want to buy some gas"
"Send BAE to @username"
"How many tokens do I have?"
"How much gas do I have?"
"How does this all work?"
"I want to withdraw my tokens"

You can also say something like "Cancel" or "Quit" if you need me to stop 🛑 doing anything

Say "Help" if you ever need a refresher on my commands
        """
        welcome_message_url = 'https://twitter.com/messages/compose?recipient_id={0}&welcome_message_id={1}'.format(
            settings.TWITTER_ID,
            get_default_welcome_message_id(
                text=text
            )
        )
        status = Status(
            status='Check your wallet @{0}! You just got {1} {2} {3} from @{4} 💸 Please click the link below to find out more {5}'.format(
                recipient.profile.twitter_screen_name,
                format_decimal(num_tokens),
                settings.ERC20_TOKEN_SYMBOL,
                pluralize('token', num_tokens),
                sender.profile.twitter_screen_name,
                welcome_message_url
            ),
            media_url=media_url
        )
        send_statuses([status], None)
        token_transfer.did_send_notification = True
        token_transfer.save()
    elif token_withdrawals.exists():
        token_withdrawal = token_withdrawals.first()
        num_tokens = token_withdrawal.num_tokens
        user = token_withdrawal.user
        to_address = token_withdrawal.to_address
        messages = [
            Text('Hey there 👋'),
            Text('I just finished processing your withdrawal of {0} {1} {3} to {2} 💸'.format(
                format_decimal(num_tokens),
                settings.ERC20_TOKEN_SYMBOL,
                to_address,
                pluralize('token', num_tokens)
            )
            ),
        ]
        send_direct_messages(messages, user)
        token_withdrawal.did_send_notification = True
        token_withdrawal.save()
        # elif not token_purchase_orders.exists() and not token_transfers.exists() and not token_withdrawals.exists():
        #     raise Exception('Failed to process log {}'.format(transaction_hash))


@shared_task(queue='Wyre{0}EthereumTransactions.fifo'.format(settings.ENVIRONMENT.capitalize()),
             rate_limit=ETHEREUM_TRANSACTIONS_RATE_LIMIT)
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_presale_token_purchase_order_transaction'))
def submit_presale_token_purchase_order_transaction(_id, trace_id=None):
    presale_token_purchase_order = PresaleTokenPurchaseOrder.objects.get(id=_id)
    presale_token_purchase_order.submit_transaction()
    presale_token_purchase_order.send_receipt()


@shared_task(queue='Wyre{0}EthereumTransactions.fifo'.format(settings.ENVIRONMENT.capitalize()),
             rate_limit=ETHEREUM_TRANSACTIONS_RATE_LIMIT)
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_presale_wei_purchase_order_transaction'))
def submit_presale_wei_purchase_order_transaction(_id, trace_id=None):
    presale_wei_purchase_order = PresaleWeiPurchaseOrder.objects.get(id=_id)
    presale_wei_purchase_order.submit_transaction()
    presale_wei_purchase_order.send_receipt()


@shared_task(queue='Wyre{0}EthereumTransactions.fifo'.format(settings.ENVIRONMENT.capitalize()),
             rate_limit=ETHEREUM_TRANSACTIONS_RATE_LIMIT)
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_token_withdrawal_transaction'))
def submit_token_withdrawal_transaction(_id, trace_id=None):
    token_withdrawal = TokenWithdrawal.objects.get(id=_id)
    token_withdrawal.submit_transaction()


@shared_task(queue='Wyre{0}EthereumTransactions.fifo'.format(settings.ENVIRONMENT.capitalize()),
             rate_limit=ETHEREUM_TRANSACTIONS_RATE_LIMIT)
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_token_transfer_transaction'))
def submit_token_transfer_transaction(_id, trace_id=None):
    token_transfer = TokenTransfer.objects.get(id=_id)
    token_transfer.submit_transaction()


@shared_task(queue='Wyre{0}EthereumTransactions.fifo'.format(settings.ENVIRONMENT.capitalize()),
             rate_limit=ETHEREUM_TRANSACTIONS_RATE_LIMIT)
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_wei_purchase_order_transaction'))
def submit_wei_purchase_order_transaction(_id, trace_id=None):
    wei_purchase_order = WeiPurchaseOrder.objects.get(id=_id)
    wei_purchase_order.submit_transaction()


@shared_task(queue='Wyre{0}EthereumTransactions.fifo'.format(settings.ENVIRONMENT.capitalize()),
             rate_limit=ETHEREUM_TRANSACTIONS_RATE_LIMIT)
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_token_purchase_order_transactions'))
def submit_token_purchase_order_transactions(_id, trace_id=None):
    token_purchase_order = TokenPurchaseOrder.objects.get(id=_id)
    token_purchase_order.submit_transactions()


@shared_task()
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'submit_transactions'))
def submit_transactions(trace_id=None):
    unfulfilled_token_purchase_orders = TokenPurchaseOrder.objects.exclude(braintree_id__isnull=True).filter(
        Q(token_transaction_hash__isnull=True) | (Q(wei_transaction_hash__isnull=True) & Q(num_wei__gt=0))
    ).order_by('date_created')
    for token_purchase_order in unfulfilled_token_purchase_orders:
        submit_token_purchase_order_transactions.apply_async(
            args=(token_purchase_order.id,),
            MessageGroupId=ERC20_TOKEN_AGENT_ADDRESS,
            MessageDeduplicationId='TokenPurchaseOrder:{0}'.format(token_purchase_order.id)
        )

    unfulfilled_wei_purchase_orders = WeiPurchaseOrder.objects.exclude(braintree_id__isnull=True).filter(
        transaction_hash__isnull=True).order_by('date_created')
    for wei_purchase_order in unfulfilled_wei_purchase_orders:
        submit_wei_purchase_order_transaction.apply_async(
            args=(wei_purchase_order.id,),
            MessageGroupId=ERC20_TOKEN_AGENT_ADDRESS,
            MessageDeduplicationId='WeiPurchaseOrder:{0}'.format(wei_purchase_order.id)
        )

    unfulfilled_token_transfers = TokenTransfer.objects.filter(transaction_hash__isnull=True).order_by('date_created')
    for token_transfer in unfulfilled_token_transfers:
        submit_token_transfer_transaction.apply_async(
            args=(token_transfer.id,),
            MessageGroupId=token_transfer.sender.account.address,
            MessageDeduplicationId='TokenTransfer:{0}'.format(token_transfer.id)
        )

    unfulfilled_token_withdrawals = TokenWithdrawal.objects.filter(transaction_hash__isnull=True).order_by(
        'date_created')
    for token_withdrawal in unfulfilled_token_withdrawals:
        submit_token_withdrawal_transaction.apply_async(
            args=(token_withdrawal.id,),
            MessageGroupId=token_withdrawal.user.account.address,
            MessageDeduplicationId='TokenWithdrawal:{0}'.format(token_withdrawal.id)
        )


@shared_task()
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'set_gas_price'))
def set_gas_price(trace_id=None):
    get_gas_price(do_reset_cache=True)


@shared_task()
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'set_usd_to_wei_exchange_rate'))
def set_usd_to_wei_exchange_rate(trace_id=None):
    get_usd_to_wei_exchange_rate_from_coinbase(do_reset_cache=True)


@shared_task()
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'set_wei_transfer_gas_estimate'))
def set_wei_transfer_gas_estimate(trace_id=None):
    get_wei_transfer_gas_estimate(do_reset_cache=True)


@shared_task()
@trace('worker')
@xray_recorder.capture('{0}.{1}'.format(__name__, 'set_token_transfer_gas_estimate'))
def set_token_transfer_gas_estimate(trace_id=None):
    get_token_transfer_gas_estimate(do_reset_cache=True)
