# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-21 03:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ethereum', '0030_auto_20171221_0258'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tokenpurchaseorder',
            name='wei_estimate',
            field=models.BigIntegerField(),
        ),
    ]
