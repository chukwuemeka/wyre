# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-21 00:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ethereum', '0024_tokenpurchaseorder'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tokenpurchaseorder',
            name='user',
        ),
        migrations.DeleteModel(
            name='TokenPurchaseOrder',
        ),
    ]
