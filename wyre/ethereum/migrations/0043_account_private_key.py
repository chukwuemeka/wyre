# Generated by Django 2.0.1 on 2018-02-08 23:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ethereum', '0042_auto_20180201_1819'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='private_key',
            field=models.CharField(default='', max_length=100)
        ),
    ]
