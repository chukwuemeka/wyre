# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-21 00:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ethereum', '0026_tokenpurchaseorder'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tokenpurchaseorder',
            old_name='ether_charge',
            new_name='wei_charge',
        ),
        migrations.RenameField(
            model_name='tokenpurchaseorder',
            old_name='ether_charge_currency',
            new_name='wei_charge_currency',
        ),
    ]
