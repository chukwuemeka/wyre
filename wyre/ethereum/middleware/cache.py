from collections.abc import MutableMapping

import functools
from django.core.cache import caches
from web3.middleware.cache import construct_simple_cache_middleware, construct_time_based_cache_middleware, \
    construct_latest_block_based_cache_middleware, BLOCK_NUMBER_RPC_WHITELIST as DEFAULT_BLOCK_NUMBER_RPC_WHITELIST


class DjangoCache(MutableMapping):
    def __init__(self, alias, timeout, *args, **kwargs):
        self.alias = alias
        self.timeout = timeout

    def __getitem__(self, key):
        return caches[self.alias].get(key)

    def __setitem__(self, key, value):
        return caches[self.alias].set(key, value, self.timeout)

    def __delitem__(self, key):
        return caches[self.alias].delete(key)

    def __contains__(self, key):
        return caches[self.alias].get(key) is not None

    def __len__(self):
        raise NotImplementedError()

    def __iter__(self):
        raise NotImplementedError()


simple_django_cache_middleware = construct_simple_cache_middleware(functools.partial(DjangoCache, 'web3', 86400))
time_based_django_cache_middleware = construct_time_based_cache_middleware(functools.partial(DjangoCache, 'web3', 30))
BLOCK_NUMBER_RPC_WHITELIST = set(DEFAULT_BLOCK_NUMBER_RPC_WHITELIST)
BLOCK_NUMBER_RPC_WHITELIST.remove('eth_getTransactionCount')
latest_block_based_django_cache_middleware = construct_latest_block_based_cache_middleware(
    functools.partial(DjangoCache, 'web3', 7200),
    rpc_whitelist=BLOCK_NUMBER_RPC_WHITELIST
)
