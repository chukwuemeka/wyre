import os

import django
from django.apps import apps
from django.conf import settings

# Configure Django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wyre.settings")
if not apps.ready and not settings.configured:
    django.setup()

from wyre.locust.locusts import *  # noqa
